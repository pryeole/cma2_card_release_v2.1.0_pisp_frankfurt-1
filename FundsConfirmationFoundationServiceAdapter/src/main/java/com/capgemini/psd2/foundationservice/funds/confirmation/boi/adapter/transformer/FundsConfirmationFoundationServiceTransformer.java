/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.transformer;

import java.time.OffsetDateTime;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.cisp.domain.OBActiveOrHistoricCurrencyAndAmount;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmation1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationDataResponse1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationResponse1;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.constants.FundsConfirmationFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.raml.domain.AvailableFunds;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.raml.domain.Balance;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.raml.domain.BalanceTypeEnum;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.raml.domain.Balanceresponse;
import com.capgemini.psd2.validator.PSD2Validator;

/**
 * The Class FundsConfirmationFoundationServiceTransformer.
 */
@Component
public class FundsConfirmationFoundationServiceTransformer {

	/** The psd2 validator. */
	@Autowired
	private PSD2Validator psd2Validator;

	public <T> OBFundsConfirmationResponse1 transformAccountBalance(T inputBalanceObj,
			OBFundsConfirmation1 oBFundsConfirmation1, Map<String, String> params) {
		OBFundsConfirmationResponse1 oBFundsConfirmationResponse1 = new OBFundsConfirmationResponse1();
		OBFundsConfirmationDataResponse1 data = new OBFundsConfirmationDataResponse1();

		UUID uuid = UUID.randomUUID();

		if (params.get(FundsConfirmationFoundationServiceConstants.ACCOUNT_SUBTYPE)
				.equals(FundsConfirmationFoundationServiceConstants.CURRENT_ACCOUNT)
				|| params.get(FundsConfirmationFoundationServiceConstants.ACCOUNT_SUBTYPE)
						.equals(FundsConfirmationFoundationServiceConstants.SAVINGS)) {

			AvailableFunds availableFunds = (AvailableFunds) inputBalanceObj;

			if (!availableFunds.getAccountCurrency().getIsoAlphaCode()
					.equalsIgnoreCase(oBFundsConfirmation1.getData().getInstructedAmount().getCurrency())) {
				throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.CURRENCY_MISMATCH);
			}
			Double difference = availableFunds.getAvailableFundsForWithdrawalAmount().getTransactionCurrency()
					- Double.valueOf(oBFundsConfirmation1.getData().getInstructedAmount().getAmount());
			if (difference >= 0) {
				data.setFundsAvailable(true);
			} else {
				data.setFundsAvailable(false);
			}
			data.setFundsConfirmationId(uuid.toString());

			data.setConsentId(oBFundsConfirmation1.getData().getConsentId());
			data.setReference(oBFundsConfirmation1.getData().getReference());

			OBActiveOrHistoricCurrencyAndAmount instructedAmount = oBFundsConfirmation1.getData().getInstructedAmount();
			instructedAmount.setAmount(oBFundsConfirmation1.getData().getInstructedAmount().getAmount());
			instructedAmount.setCurrency(oBFundsConfirmation1.getData().getInstructedAmount().getCurrency());
			data.setInstructedAmount(instructedAmount);

			data.setCreationDateTime(OffsetDateTime.now().toString());
		}

		else if (params.get(FundsConfirmationFoundationServiceConstants.ACCOUNT_SUBTYPE)
				.equals(FundsConfirmationFoundationServiceConstants.CREDIT_CARD)) {
			Balanceresponse creditCardAccount = (Balanceresponse) inputBalanceObj;
			for (Balance listOfBalancesItem : creditCardAccount.getBalancesList()) {
				if ((listOfBalancesItem.getBalanceType().toString())
						.equalsIgnoreCase(BalanceTypeEnum.AVAILABLE_LIMIT.toString())) {

					if (!listOfBalancesItem.getBalanceCurrency().getIsoAlphaCode()
							.equalsIgnoreCase(oBFundsConfirmation1.getData().getInstructedAmount().getCurrency())) {
						throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.CURRENCY_MISMATCH);
					}
					Double difference = listOfBalancesItem.getBalanceAmount().getLocalReportingCurrency()
							- Double.valueOf(oBFundsConfirmation1.getData().getInstructedAmount().getAmount());
					if (difference >= 0) {
						data.setFundsAvailable(true);
					} else {
						data.setFundsAvailable(false);
					}
					data.setFundsConfirmationId(uuid.toString());
					data.setConsentId(oBFundsConfirmation1.getData().getConsentId());
					data.setReference(oBFundsConfirmation1.getData().getReference());
					OBActiveOrHistoricCurrencyAndAmount instructedAmount = oBFundsConfirmation1.getData()
							.getInstructedAmount();
					instructedAmount.setAmount(oBFundsConfirmation1.getData().getInstructedAmount().getAmount());
					instructedAmount.setCurrency(oBFundsConfirmation1.getData().getInstructedAmount().getCurrency());
					data.setInstructedAmount(instructedAmount);
					data.setCreationDateTime(OffsetDateTime.now().toString());
				}
			}

		}
		psd2Validator.validate(data);
		oBFundsConfirmationResponse1.setData(data);
		return oBFundsConfirmationResponse1;
	}
}
