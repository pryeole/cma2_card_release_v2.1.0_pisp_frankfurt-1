package com.capgemini.psd2.response.test.validator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.capgemini.psd2.aisp.domain.OBBCAData1;
import com.capgemini.psd2.aisp.domain.OBExternalProductType1Code;
import com.capgemini.psd2.aisp.domain.OBOtherProductType1;
import com.capgemini.psd2.aisp.domain.OBPCAData1;
import com.capgemini.psd2.aisp.domain.OBProduct2;
import com.capgemini.psd2.aisp.domain.OBReadProduct2;
import com.capgemini.psd2.aisp.domain.OBReadProduct2Data;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.response.validator.AccountProductResponseValidator;

public class AccountProductResponseValidatorTest {

	AccountProductResponseValidator obj=new AccountProductResponseValidator();
	
	@Before
	public void setUp() throws Exception {
		Map<String, String> map=new HashMap<>();
		map.put("SIGNATURE", "Something wrong with x-jws-signature request header");
		Map<String, String> specificErrorMessageMap=new HashMap<>();
		specificErrorMessageMap.put("signature_invalid_content", "Invalid content prsent in joseheader of x-jws-signature header");
		specificErrorMessageMap.put("signature_missing", "signature header missing in request");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessageMap);
	}
	
	@Test(expected=PSD2Exception.class)
	public void testPlatformValidations() {
		OBReadProduct2 obReadProduct2=new OBReadProduct2();
		OBReadProduct2Data data=new OBReadProduct2Data();
		List<OBProduct2> productList=new ArrayList<>();
		OBProduct2 product=new OBProduct2();
		OBBCAData1 obBCAData1=new OBBCAData1();
		product.setBCA(obBCAData1);
		product.setProductType(OBExternalProductType1Code.COMMERCIALCREDITCARD);
		productList.add(product);
		data.setProduct(productList);
		obReadProduct2.setData(data);
		
		obj.performPlatformValidation(obReadProduct2);
	}
	
	@Test(expected=PSD2Exception.class)
	public void testPlatformValidations1() {
		OBReadProduct2 obReadProduct2=new OBReadProduct2();
		OBReadProduct2Data data=new OBReadProduct2Data();
		List<OBProduct2> productList=new ArrayList<>();
		OBProduct2 product=new OBProduct2();
		OBPCAData1 obBCAData1=new OBPCAData1();
		product.setPCA(obBCAData1);
		product.setProductType(OBExternalProductType1Code.COMMERCIALCREDITCARD);
		productList.add(product);
		data.setProduct(productList);
		obReadProduct2.setData(data);
		
		obj.performPlatformValidation(obReadProduct2);
	}
	
	@Test(expected=PSD2Exception.class)
	public void testPlatformValidations2() {
		OBReadProduct2 obReadProduct2=new OBReadProduct2();
		OBReadProduct2Data data=new OBReadProduct2Data();
		List<OBProduct2> productList=new ArrayList<>();
		OBProduct2 product=new OBProduct2();
        OBOtherProductType1 otherProductType=new OBOtherProductType1();
		product.setOtherProductType(otherProductType);
		product.setProductType(OBExternalProductType1Code.COMMERCIALCREDITCARD);
		productList.add(product);
		data.setProduct(productList);
		obReadProduct2.setData(data);	
		obj.performPlatformValidation(obReadProduct2);
	}
	
	@Test(expected=PSD2Exception.class)
	public void testPlatformValidations3() {
		OBReadProduct2 obReadProduct2=new OBReadProduct2();
		OBReadProduct2Data data=new OBReadProduct2Data();
		List<OBProduct2> productList=new ArrayList<>();
		OBProduct2 product=new OBProduct2();
		product.setOtherProductType(null);
		product.setProductType(OBExternalProductType1Code.OTHER);
		productList.add(product);
		data.setProduct(productList);
		obReadProduct2.setData(data);	
		obj.performPlatformValidation(obReadProduct2);
	}
	
	@Test
	public void testPlatformValidations4() {
		OBReadProduct2 obReadProduct2=new OBReadProduct2();
		OBReadProduct2Data data=new OBReadProduct2Data();
		List<OBProduct2> productList=new ArrayList<>();
		data.setProduct(productList);
		obReadProduct2.setData(data);	
		obj.performPlatformValidation(obReadProduct2);
	}
}
