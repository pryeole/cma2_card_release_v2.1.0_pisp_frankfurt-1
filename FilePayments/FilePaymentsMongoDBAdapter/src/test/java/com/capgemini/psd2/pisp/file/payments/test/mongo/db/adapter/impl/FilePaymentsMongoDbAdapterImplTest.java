package com.capgemini.psd2.pisp.file.payments.test.mongo.db.adapter.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.FPaymentsFoundationResource;
import com.capgemini.psd2.pisp.domain.OBExternalStatus1Code;
import com.capgemini.psd2.pisp.domain.OBExternalStatus2Code;
import com.capgemini.psd2.pisp.domain.OBFile1;
import com.capgemini.psd2.pisp.domain.OBMultiAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBWriteDataFile1;
import com.capgemini.psd2.pisp.domain.OBWriteDataFileResponse1;
import com.capgemini.psd2.pisp.enums.ProcessExecutionStatusEnum;
import com.capgemini.psd2.pisp.file.payments.mongo.db.adapter.config.PaymentAwsObject;
import com.capgemini.psd2.pisp.file.payments.mongo.db.adapter.impl.FilePaymentsMongoDbAdapterImpl;
import com.capgemini.psd2.pisp.file.payments.mongo.db.adapter.repository.FPaymentsDataRepository;
import com.capgemini.psd2.pisp.file.payments.mongo.db.adapter.repository.FilePaymentsFoundationRepository;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.utilities.SandboxValidationConfig;
import com.capgemini.psd2.utilities.SandboxValidationUtility;

@RunWith(SpringJUnit4ClassRunner.class)
public class FilePaymentsMongoDbAdapterImplTest {

	private MockMvc mockMvc;
	
	@Mock
	private FPaymentsDataRepository fPaymentsDataRepository;

	@Mock
	private FilePaymentsFoundationRepository fPaymentsFoundationRepository;

	@Mock
	private BasicAWSCredentials awsCredentials;

	@Mock
	private ClientConfiguration clientConfig;

	@Mock
	private SandboxValidationUtility utility;
	
	@Mock
	private PaymentAwsObject s3objectmock;
	
	@Mock
	private RequestHeaderAttributes reqAttributes;
	
	@Mock
    SandboxValidationConfig sandboxConfig;

	
	@InjectMocks
	private FilePaymentsMongoDbAdapterImpl adapter;
	
	@Value("${app.awsRegion}")
	private String awsRegion;

	@Value("${app.awsS3BucketName}")
	private String awsS3BucketName;
	
	@Value("${app.awsReportfileName}")
	private String fileName;
	
	@Mock
	public AmazonS3 s3client;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(adapter).dispatchOptions(true).build();
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testFindFilePaymentsResourceStatus() throws Exception{
	
		CustomPaymentStageIdentifiers stageIdentifiers=new CustomPaymentStageIdentifiers();
		CustomFilePaymentsPOSTResponse reponse=new CustomFilePaymentsPOSTResponse();
		when(fPaymentsDataRepository.findOneByDataFilePaymentId(anyObject())).thenReturn(reponse);
		adapter.findFilePaymentsResourceStatus(stageIdentifiers);
	}
	
	@Test(expected=PSD2Exception.class)
	public void testFindFilePaymentsResourceStatusExc() throws Exception{
	
		CustomPaymentStageIdentifiers stageIdentifiers=new CustomPaymentStageIdentifiers();
		when(fPaymentsDataRepository.findOneByDataFilePaymentId(anyObject())).thenThrow(new DataAccessResourceFailureException("Test"));
		adapter.findFilePaymentsResourceStatus(stageIdentifiers);
	}
	
	
	private CustomFilePaymentsPOSTRequest createCustomRequest(BigDecimal controlSum) {
		ReflectionTestUtils.setField(adapter, "borneByCreditorRegex", "^(111([0-9]{0,10}\\.\\d{0,5}))$");
		ReflectionTestUtils.setField(adapter, "borneByDebtorRegex", "^(222([0-9]{0,10}\\.\\d{0,5}))$");
		ReflectionTestUtils.setField(adapter, "followServiceLevelRegex", "^(333([0-9]{0,10}\\.\\d{0,5}))$");
		ReflectionTestUtils.setField(adapter, "sharedRegex", "^(444([0-9]{0,10}\\.\\d{0,5}))$");
		ReflectionTestUtils.setField(adapter, "chargeAmount", "10.55");
		CustomFilePaymentsPOSTRequest customRequest=new CustomFilePaymentsPOSTRequest();
		OBFile1 initiation=new OBFile1();
		OBWriteDataFile1 data=new OBWriteDataFile1();
		
		initiation.setControlSum(controlSum);
		customRequest.setData(data);
		customRequest.getData().setInitiation(initiation);
		return customRequest;
	}
	@Test
	public void testProcessPayments() throws Exception{
		Map<String, String> params=new HashMap<>();
		BigDecimal controlSum=new BigDecimal("1112.65");
		CustomFilePaymentsPOSTRequest customRequest=createCustomRequest(controlSum);
		Map<String, OBExternalStatus1Code> paymentStatusMap=new HashMap<>();
		FPaymentsFoundationResource resource=new FPaymentsFoundationResource();
		OBMultiAuthorisation1 authorisation=new OBMultiAuthorisation1();
		ProcessExecutionStatusEnum value=ProcessExecutionStatusEnum.PASS;
		
		authorisation.setStatus(OBExternalStatus2Code.AUTHORISED);
		when(sandboxConfig.getChargeAmountLimit()).thenReturn("5");
		when(utility.getMockedProcessExecStatus(anyString(),anyString())).thenReturn(value);
		when(utility.getMockedMultiAuthBlock(anyString(), anyString())).thenReturn(authorisation);
		when(fPaymentsFoundationRepository.save(any(FPaymentsFoundationResource.class))).thenReturn(resource);
		
		adapter.processPayments(customRequest, paymentStatusMap, params);
	}
	
	@Test
	public void testProcessPaymentsDebtor() throws Exception{
	
		BigDecimal controlSum=new BigDecimal("2222.65");
		CustomFilePaymentsPOSTRequest customRequest=createCustomRequest(controlSum);
		Map<String, OBExternalStatus1Code> paymentStatusMap=new HashMap<>();
		FPaymentsFoundationResource resource=new FPaymentsFoundationResource();
		Map<String, String> params=new HashMap<>();
		OBMultiAuthorisation1 authorisation=new OBMultiAuthorisation1();
		ProcessExecutionStatusEnum value=ProcessExecutionStatusEnum.PASS;
		
		authorisation.setStatus(OBExternalStatus2Code.AWAITINGFURTHERAUTHORISATION);
		when(sandboxConfig.getChargeAmountLimit()).thenReturn("5");
		when(utility.getMockedProcessExecStatus(anyString(),anyString())).thenReturn(value);
		when(utility.getMockedMultiAuthBlock(anyString(), anyString())).thenReturn(authorisation);
		when(fPaymentsFoundationRepository.save(any(FPaymentsFoundationResource.class))).thenReturn(resource);
		
		adapter.processPayments(customRequest, paymentStatusMap, params);
	}
	
	@Test
	public void testProcessPaymentsShared() throws Exception{
	
		BigDecimal controlSum=new BigDecimal("4442.65");
		CustomFilePaymentsPOSTRequest customRequest=createCustomRequest(controlSum);
		Map<String, OBExternalStatus1Code> paymentStatusMap=new HashMap<>();
		FPaymentsFoundationResource resource=new FPaymentsFoundationResource();
		Map<String, String> params=new HashMap<>();
		OBMultiAuthorisation1 authorisation=new OBMultiAuthorisation1();
		ProcessExecutionStatusEnum value=ProcessExecutionStatusEnum.PASS;
		
		authorisation.setStatus(OBExternalStatus2Code.REJECTED);
		when(sandboxConfig.getChargeAmountLimit()).thenReturn("5");
		when(utility.getMockedProcessExecStatus(anyString(),anyString())).thenReturn(value);
		when(utility.getMockedMultiAuthBlock(anyString(), anyString())).thenReturn(authorisation);
		when(fPaymentsFoundationRepository.save(any(FPaymentsFoundationResource.class))).thenReturn(resource);
		
		adapter.processPayments(customRequest, paymentStatusMap, params);
	}
	
	@Test
	public void testProcessPaymentsServiceLevel() throws Exception{
		
		BigDecimal controlSum=new BigDecimal("3332.65");
		CustomFilePaymentsPOSTRequest customRequest=createCustomRequest(controlSum);
		Map<String, OBExternalStatus1Code> paymentStatusMap=new HashMap<>();
		FPaymentsFoundationResource resource=new FPaymentsFoundationResource();
		Map<String, String> params=new HashMap<>();
		OBMultiAuthorisation1 authorisation=new OBMultiAuthorisation1();
		ProcessExecutionStatusEnum value=ProcessExecutionStatusEnum.FAIL;
		
		authorisation.setStatus(OBExternalStatus2Code.REJECTED);
		when(sandboxConfig.getChargeAmountLimit()).thenReturn("5");
		when(utility.getMockedProcessExecStatus(anyString(),anyString())).thenReturn(value);
		when(utility.getMockedMultiAuthBlock(anyString(), anyString())).thenReturn(authorisation);
		when(fPaymentsFoundationRepository.save(any(FPaymentsFoundationResource.class))).thenReturn(resource);
		
		adapter.processPayments(customRequest, paymentStatusMap, params);
	}
	
	@Test(expected=PSD2Exception.class)
	public void testProcessPaymentsServiceLevelExc() throws Exception{
	
		BigDecimal controlSum=new BigDecimal("2222.65");
		CustomFilePaymentsPOSTRequest customRequest=createCustomRequest(controlSum);
		Map<String, OBExternalStatus1Code> paymentStatusMap=new HashMap<>();
		Map<String, String> params=new HashMap<>();
		OBMultiAuthorisation1 authorisation=new OBMultiAuthorisation1();
		ProcessExecutionStatusEnum value=ProcessExecutionStatusEnum.FAIL;
		
		authorisation.setStatus(OBExternalStatus2Code.REJECTED);
		when(sandboxConfig.getChargeAmountLimit()).thenReturn("5");
		when(utility.getMockedProcessExecStatus(anyString(),anyString())).thenReturn(value);
		when(utility.getMockedMultiAuthBlock(anyString(), anyString())).thenReturn(authorisation);
		when(fPaymentsFoundationRepository.save(any(FPaymentsFoundationResource.class))).thenThrow(new DataAccessResourceFailureException("Test"));
		
		adapter.processPayments(customRequest, paymentStatusMap, params);
	}
	
	@Test(expected=PSD2Exception.class)
	public void testProcessPaymentsServiceLevelSandboxExc() throws Exception{
	
		BigDecimal controlSum=new BigDecimal("2222.65");
		CustomFilePaymentsPOSTRequest customRequest=createCustomRequest(controlSum);
		Map<String, OBExternalStatus1Code> paymentStatusMap=new HashMap<>();
		Map<String, String> params=new HashMap<>();
		OBMultiAuthorisation1 authorisation=new OBMultiAuthorisation1();
		ProcessExecutionStatusEnum value=ProcessExecutionStatusEnum.FAIL;
		FPaymentsFoundationResource resource=new FPaymentsFoundationResource();
		authorisation.setStatus(OBExternalStatus2Code.REJECTED);
		
		when(utility.getMockedProcessExecStatus(anyString(),anyString())).thenReturn(value);
		when(utility.getMockedMultiAuthBlock(anyString(), anyString())).thenReturn(authorisation);
		when(fPaymentsFoundationRepository.save(any(FPaymentsFoundationResource.class))).thenReturn(resource);
		when(utility.isValidAmount(anyString(), anyString())).thenReturn(true);
		adapter.processPayments(customRequest, paymentStatusMap, params);
	}
	
	@Test
	public void testRetrieveStagedFilePayments() throws Exception{
		
		CustomPaymentStageIdentifiers customPaymentStageIdentifiers=new CustomPaymentStageIdentifiers();
		Map<String, String> params=new HashMap<>();
		FPaymentsFoundationResource resource=new FPaymentsFoundationResource();
		
		when(fPaymentsFoundationRepository
				.findOneByDataConsentId(anyString())).thenReturn(resource);
		when(reqAttributes.getMethodType()).thenReturn("POST");
		adapter.retrieveStagedFilePayments(customPaymentStageIdentifiers, params);
	}
	
	@Test(expected=PSD2Exception.class)
	public void testRetrieveStagedFilePaymentsSandboxExc() throws Exception{
		
		CustomPaymentStageIdentifiers customPaymentStageIdentifiers=new CustomPaymentStageIdentifiers();
		Map<String, String> params=new HashMap<>();
		FPaymentsFoundationResource resource=new FPaymentsFoundationResource();
		OBWriteDataFileResponse1 data=new OBWriteDataFileResponse1();
		OBFile1 initiation=new OBFile1();
		
		BigDecimal controlSum=new BigDecimal("123.435");
		initiation.setControlSum(controlSum);
		data.setInitiation(initiation);
		resource.setData(data);
		
		when(fPaymentsFoundationRepository
				.findOneByDataConsentId(anyString())).thenReturn(resource);
		when(reqAttributes.getMethodType()).thenReturn("GET");
		when(utility.isValidAmount(anyString(), anyString())).thenReturn(true);
		
		adapter.retrieveStagedFilePayments(customPaymentStageIdentifiers, params);
	}
	
	@Test
	public void testDownloadFilePaymentsReport() throws Exception{
		ReflectionTestUtils.setField(adapter, "awsRegion", "eu-west-1");
		ReflectionTestUtils.setField(awsCredentials, "accessKey", "AKIAJHAQ2CSZAEIG4ZCQ");
		ReflectionTestUtils.setField(awsCredentials, "secretKey", "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");
		ReflectionTestUtils.setField(adapter, "awsS3BucketName", "file-payment-consents-bucket");
		
		Resource resource = new ClassPathResource("/file-report.pdf");
		File file = resource.getFile();
		InputStream is = new FileInputStream(file);
		ObjectMetadata objMeta = mock(ObjectMetadata.class);		
		
		S3Object fullObject = new S3Object();
		fullObject.setKey("local.pdf");
		fullObject.setBucketName("file-payment-consents-bucket");
		fullObject.setObjectContent(is);
		fullObject.setObjectMetadata(objMeta);
		
		when(s3objectmock.getPaymentAwsClient(any(BasicAWSCredentials.class), anyObject(),anyString())).thenReturn(s3client);

		when(s3objectmock.getPaymentAwsObjectS3Object(any(AmazonS3Client.class), anyString(), anyString())).thenReturn(fullObject);
	
		adapter.downloadFilePaymentsReport("e7d11b90-c069-42a1-b42a-aa28831fc52f");
		
	}
	
	@Test(expected=PSD2Exception.class)
	public void testDownloadFilePaymentsReportExc() throws Exception{
		ReflectionTestUtils.setField(adapter, "awsRegion", "eu-west-1");
		ReflectionTestUtils.setField(awsCredentials, "accessKey", "AKIAJHAQ2CSZAEIG4ZCQ");
		ReflectionTestUtils.setField(awsCredentials, "secretKey", "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");
		ReflectionTestUtils.setField(adapter, "awsS3BucketName", "file-payment-consents-bucket");
		
		Resource resource = new ClassPathResource("/file-report.pdf");
		File file = resource.getFile();
		
		when(s3objectmock.getPaymentAwsClient(any(BasicAWSCredentials.class), anyObject(),anyString())).thenReturn(s3client);

		when(s3objectmock.getPaymentAwsObjectS3Object(any(AmazonS3Client.class), anyString(), anyString())).thenThrow(Exception.class);
	
		adapter.downloadFilePaymentsReport("e7d11b90-c069-42a1-b42a-aa28831fc52f");
		
	}
}
