package com.capgemini.psd2.pisp.file.payments.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.cma2.aisp.domain.NoPayloadLogging;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.FPaymentsRetrieveGetRequest;
import com.capgemini.psd2.pisp.domain.PaymentFileSubmitPOST201Response;
import com.capgemini.psd2.pisp.domain.PlatformFilePaymentsFileResponse;
import com.capgemini.psd2.pisp.file.payments.service.FilePaymentsService;
import com.capgemini.psd2.pisp.validation.adapter.FilePaymentValidator;

@RestController
public class FilePaymentsController {

	@Autowired
	private FilePaymentsService filePaymentsService;

	@Autowired
	private FilePaymentValidator filePaymentValidator;

	@Value("${app.responseErrorMessageEnabled}")
	private String responseErrorMessageEnabled;

	@NoPayloadLogging
	@RequestMapping(value = "/file-payments/{paymentId}/report-file", method = RequestMethod.GET)
	public ResponseEntity<?> downloadFilePaymentReport(FPaymentsRetrieveGetRequest paymentsRetrieveGetRequest) {

		filePaymentValidator.validateFPaymentsDownloadRequest(paymentsRetrieveGetRequest);

		PlatformFilePaymentsFileResponse fileResponse = filePaymentsService
				.downloadTransactionFileByConsentId(paymentsRetrieveGetRequest.getPaymentId());

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "inline; filename=" + fileResponse.getFileName());
		headers.setContentType(MediaType.APPLICATION_PDF);

		return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_PDF)
				.body(new ByteArrayResource(fileResponse.getFileByteArray()));

	}

	@RequestMapping(value = "/file-payments", method = RequestMethod.POST)
	public ResponseEntity<?> submitFilePaymentResource(
			@RequestBody CustomFilePaymentsPOSTRequest customFilePaymentsPOSTRequest) {

		boolean isPaymentValidated = true;
		PaymentFileSubmitPOST201Response filePaymentResponse = filePaymentsService
				.createFilePaymentsResource(customFilePaymentsPOSTRequest, isPaymentValidated);

		return new ResponseEntity<>(filePaymentResponse, HttpStatus.CREATED);

	}

	@RequestMapping(value = "/file-payments/{filePaymentId}", method = RequestMethod.GET)
	public ResponseEntity<PaymentFileSubmitPOST201Response> findFilePaymentResourceStatus(
			@PathVariable("filePaymentId") String filePaymentId) {

		PaymentFileSubmitPOST201Response filePaymentResponse = filePaymentsService
				.findStatusFilePaymentsResource(filePaymentId);
		return new ResponseEntity<>(filePaymentResponse, HttpStatus.OK);

	}
}
