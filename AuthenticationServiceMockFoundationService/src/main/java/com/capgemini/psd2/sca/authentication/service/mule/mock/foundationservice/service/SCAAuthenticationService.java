package com.capgemini.psd2.sca.authentication.service.mule.mock.foundationservice.service;

import com.capgemini.psd2.sca.authentication.service.mule.mock.foundationservice.domain.Login;
import com.capgemini.psd2.sca.authentication.service.mule.mock.foundationservice.domain.LoginResponse;

public interface SCAAuthenticationService {

	
	public LoginResponse  createAuthenticationDetails(Login login);

}
