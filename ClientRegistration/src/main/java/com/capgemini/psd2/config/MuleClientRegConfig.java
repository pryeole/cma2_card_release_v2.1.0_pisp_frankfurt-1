package com.capgemini.psd2.config;

import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import com.capgemini.tpp.dtos.ApiDetail;

@Configuration
@ConfigurationProperties(prefix = "muleclientReg")
public class MuleClientRegConfig {

	private String prefix;
	private String user;
	private String password;
	private String loginUrl;
	private String apiUrlPrefix;
	private String applicationUrl;
	private String applicationRollBackUrl;
	private String apiUrl;
	private String tierUrl;
	private String apiContractUrl;
	private String showSecretUrl;
	private String resetSecretUrl;
	private Map<String,ApiDetail> muleContractApi;
	
	public Map<String, ApiDetail> getMuleContractApi() {
		return muleContractApi;
	}

	public void setMuleContractApi(Map<String, ApiDetail> muleContractApi) {
		this.muleContractApi = muleContractApi;
	}

	public String getUser() {
		return user;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getTierUrl() {
		return getAbsoluteUrl(prefix, apiUrlPrefix, tierUrl);
	}

	public String getPassword() {
		return password;
	}

	public String getApplicationUrl() {
		return getAbsoluteUrl(prefix, apiUrlPrefix, applicationUrl);
	}

	public String getApplicationRollBackUrl() {
		return getAbsoluteUrl(prefix, apiUrlPrefix, applicationRollBackUrl);
	}

	public String getApiUrl() {
		return getAbsoluteUrl(prefix, apiUrlPrefix, apiUrl);
	}

	public String getApiContractUrl() {
		return getAbsoluteUrl(prefix, apiUrlPrefix, apiContractUrl);
	}

	public void setUser(String user) {
		this.user = user;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getLoginUrl() {
		return getAbsoluteUrl(prefix, loginUrl, "");
	}

	public void setLoginUrl(String loginUrl) {
		this.loginUrl = loginUrl;
	}

	public String getApiUrlPrefix() {
		return apiUrlPrefix;
	}

	public void setApiUrlPrefix(String apiUrlPrefix) {
		this.apiUrlPrefix = apiUrlPrefix;
	}

	public void setApplicationUrl(String applicationUrl) {
		this.applicationUrl = applicationUrl;
	}

	public void setApplicationRollBackUrl(String applicationRollBackUrl) {
		this.applicationRollBackUrl = applicationRollBackUrl;
	}

	public void setApiUrl(String apiUrl) {
		this.apiUrl = apiUrl;
	}

	public void setApiContractUrl(String apiContractUrl) {
		this.apiContractUrl = apiContractUrl;
	}

	public void setTierUrl(String tierUrl) {
		this.tierUrl = tierUrl;
	}

	public String getShowSecretUrl() {
		return getAbsoluteUrl(prefix, apiUrlPrefix, showSecretUrl);
	}

	public void setShowSecretUrl(String showSecretUrl) {
		this.showSecretUrl = showSecretUrl;
	}

	public String getResetSecretUrl() {
		return getAbsoluteUrl(prefix, apiUrlPrefix, resetSecretUrl);
	}

	public void setResetSecretUrl(String resetSecretUrl) {
		this.resetSecretUrl = resetSecretUrl;
	}

	public String getAbsoluteUrl(String prefix, String apiUrlPrefix, String appUrl) {
		return prefix + apiUrlPrefix + appUrl;
	}
}
