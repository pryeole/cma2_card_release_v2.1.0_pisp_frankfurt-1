(function (angular) {
    angular.module("fraudAnalyzer", ["oc.lazyLoad"]).provider("$fraudAnalyze",[function () {
        var jsCollectors = [];
        var filesLoaded;
        this.setJsCollectors = function(jsFiles){
            jsCollectors = jsFiles;
        };
        this.$get = function($ocLazyLoad) {
            if(jsCollectors.length){
                filesLoaded = $ocLazyLoad.load(jsCollectors);
            }
            return {
                loaded: filesLoaded,
                init: function () {
                    // 
                },
                capture: function(){
                    // 
                }
            }
        }
    }]);
})(angular);