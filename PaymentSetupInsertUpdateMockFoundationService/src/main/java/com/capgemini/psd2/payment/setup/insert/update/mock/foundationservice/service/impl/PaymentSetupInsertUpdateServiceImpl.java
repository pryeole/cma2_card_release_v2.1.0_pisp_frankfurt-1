package com.capgemini.psd2.payment.setup.insert.update.mock.foundationservice.service.impl;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceException;
import com.capgemini.psd2.foundationservice.utilities.NullCheckUtils;
import com.capgemini.psd2.foundationservice.validator.SuccesCodeEnum;
import com.capgemini.psd2.foundationservice.validator.ValidationPassed;
import com.capgemini.psd2.foundationservice.validator.ValidationUtility;
import com.capgemini.psd2.payment.setup.insert.update.mock.foundationservice.domain.Payment;
import com.capgemini.psd2.payment.setup.insert.update.mock.foundationservice.domain.PaymentInstruction;
import com.capgemini.psd2.payment.setup.insert.update.mock.foundationservice.repository.PaymentSetupInsertUpdateRepository;
import com.capgemini.psd2.payment.setup.insert.update.mock.foundationservice.service.PaymentSetupInsertUpdateService;

@Service
public class PaymentSetupInsertUpdateServiceImpl implements PaymentSetupInsertUpdateService {
	@Autowired
	PaymentSetupInsertUpdateRepository paymentSetupCreateRepository;
	@Autowired
	ValidationUtility validationUtil;
	
	@Override
	public ValidationPassed stagePaymentInsertion(PaymentInstruction paymentInstruction) {
		
		String eIdentification = paymentInstruction.getEndToEndIdentification();
		String requestType = paymentInstruction.getPayment().getRequestType();
		String successCode = null;
		String successMsg = null;
		if ("I".equalsIgnoreCase(requestType)) {
			successCode = SuccesCodeEnum.SUCCESS_STAGE_PAYMENT_INSERT.getSuccessCode();
			successMsg = SuccesCodeEnum.SUCCESS_STAGE_PAYMENT_INSERT.getSuccessMessage();
		} else if ("U".equalsIgnoreCase(requestType)) {
			successCode = SuccesCodeEnum.SUCCESS_STAGE_PAYMENT_UPDATE.getSuccessCode();
			successMsg = SuccesCodeEnum.SUCCESS_STAGE_PAYMENT_UPDATE.getSuccessMessage();
		}
		ValidationPassed validationPassed = validationUtil.validateMockBusinessValidations(eIdentification, successCode, successMsg);
		
		String paymentID = null;
		String submissionID = null;
		Payment payment = paymentInstruction.getPayment();
		if ("I".equalsIgnoreCase(requestType)) {
			paymentID = UUID.randomUUID().toString().replace("-", "");
			submissionID = UUID.randomUUID().toString().replace("-", "");
			payment.setPaymentId(paymentID);
			payment.getPaymentInformation().setSubmissionID(submissionID);
			paymentSetupCreateRepository.save(paymentInstruction);
		}else if("U".equalsIgnoreCase(requestType)){
			PaymentInstruction paymentToUpdate = paymentSetupCreateRepository.findByPaymentPaymentId(paymentInstruction.getPayment().getPaymentId());
			if (NullCheckUtils.isNullOrEmpty(paymentToUpdate)) {
				throw MockFoundationServiceException.populateMockFoundationServiceException(ErrorCodeEnum.NO_TXN_DETAILS_FOUND_PMU);
			}			
			if("AcceptedSettlementInProcess".equalsIgnoreCase(payment.getPaymentInformation().getStatus())){
				payment.getPaymentInformation().setInitiatingPartyName(paymentToUpdate.getPayment().getPaymentInformation().getInitiatingPartyName());
				payment.getPaymentInformation().setCreatedDateTime(paymentToUpdate.getPayment().getPaymentInformation().getCreatedDateTime());
			}
			payment.getPaymentInformation().setSubmissionID(paymentToUpdate.getPayment().getPaymentInformation().getSubmissionID());
			paymentToUpdate.getPayment().setPaymentInformation(payment.getPaymentInformation());
			paymentToUpdate.getPayment().setPayeeInformation(payment.getPayeeInformation());
			paymentToUpdate.getPayment().setPayerInformation(payment.getPayerInformation());
			paymentToUpdate.getPayment().setRequestType(payment.getRequestType());
			paymentToUpdate.getPayment().setChannelUserID(payment.getChannelUserID());
			paymentSetupCreateRepository.save(paymentToUpdate);
		}	
		paymentID = payment.getPaymentId();

		validationPassed.setSuccessMessage(paymentID);
		return validationPassed;

	}
}
