package com.capgemini.psd2.aisp.validation.adapter.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.domain.OBReadTransaction3;
import com.capgemini.psd2.aisp.domain.OBTransaction3;
import com.capgemini.psd2.aisp.validation.adapter.AISPCustomValidator;
import com.capgemini.psd2.aisp.validation.adapter.utilities.CommonAccountValidations;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.validator.PSD2Validator;

@Component("accountTransactionsValidator")
@ConfigurationProperties("app")
public class AccountTransactionsValidatorImpl implements AISPCustomValidator<OBReadTransaction3, OBReadTransaction3> {

	@Autowired
	private PSD2Validator psd2Validator;

	@Value("${app.swaggerValidations.response:#{true}}")
	private Boolean resValidationEnabled;

	@Autowired
	private CommonAccountValidations commonAccountValidations;

	@Override
	public OBReadTransaction3 validateRequestParams(OBReadTransaction3 obReadTransaction3) {
		return obReadTransaction3;
	}

	@Override
	public boolean validateResponseParams(OBReadTransaction3 obReadTransaction3) {
		if (resValidationEnabled)
			executeAccountTransactionSwaggerValidations(obReadTransaction3);
		executeAccountTransactionCustomValidations(obReadTransaction3);
		return true;
	}

	@Override
	public void validateUniqueId(String consentId) {
		if (NullCheckUtils.isNullOrEmpty(consentId)) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_EXPECTED, ErrorMapKeys.NO_ACCOUNT_ID_FOUND));
		}
		commonAccountValidations.validateUniqueUUID(consentId);
	}

	private void executeAccountTransactionSwaggerValidations(OBReadTransaction3 obReadTransaction3) {
		psd2Validator.validate(obReadTransaction3);

	}

	private void executeAccountTransactionCustomValidations(OBReadTransaction3 obReadTransaction3) {
		if (!NullCheckUtils.isNullOrEmpty(obReadTransaction3)) {
			if (!NullCheckUtils.isNullOrEmpty(obReadTransaction3.getData())) {
				for (OBTransaction3 obTransaction : obReadTransaction3.getData().getTransaction()) {
					if (!NullCheckUtils.isNullOrEmpty(obTransaction)) {
						if (!NullCheckUtils.isNullOrEmpty(obTransaction.getBookingDateTime())) {
							commonAccountValidations
									.validateAndParseDateTimeFormatForResponse(obTransaction.getBookingDateTime());
						}
						if (!NullCheckUtils.isNullOrEmpty(obTransaction.getValueDateTime())) {
							commonAccountValidations
									.validateAndParseDateTimeFormatForResponse(obTransaction.getValueDateTime());
						}
						if (!NullCheckUtils.isNullOrEmpty(obTransaction.getValueDateTime())) {
							commonAccountValidations
									.validateAndParseDateTimeFormatForResponse(obTransaction.getValueDateTime());
						}
						if (!NullCheckUtils.isNullOrEmpty(obTransaction.getAmount())) {
							if (!NullCheckUtils.isNullOrEmpty(obTransaction.getAmount().getAmount())) {
								commonAccountValidations.validateAmount(obTransaction.getAmount().getAmount());
							}
							if (!NullCheckUtils.isNullOrEmpty(obTransaction.getAmount().getCurrency())) {
								commonAccountValidations.isValidCurrency(obTransaction.getAmount().getCurrency());
							}
						}
	
						if (!NullCheckUtils.isNullOrEmpty(obTransaction.getChargeAmount())) {
							if (!NullCheckUtils.isNullOrEmpty(obTransaction.getChargeAmount().getAmount())) {
								commonAccountValidations.validateAmount(obTransaction.getAmount().getAmount());
							}
							if (!NullCheckUtils.isNullOrEmpty(obTransaction.getChargeAmount().getCurrency())) {
								commonAccountValidations.isValidCurrency(obTransaction.getAmount().getCurrency());
							}
	
						}
						if (!NullCheckUtils.isNullOrEmpty(obTransaction.getCurrencyExchange())) {
							if (!NullCheckUtils.isNullOrEmpty(obTransaction.getCurrencyExchange().getInstructedAmount())) {
								if (!NullCheckUtils.isNullOrEmpty(
										obTransaction.getCurrencyExchange().getInstructedAmount().getAmount())) {
									commonAccountValidations.validateAmount(obTransaction.getAmount().getAmount());
								}
	
								if (!NullCheckUtils.isNullOrEmpty(
										obTransaction.getCurrencyExchange().getInstructedAmount().getCurrency())) {
									commonAccountValidations.isValidCurrency(obTransaction.getAmount().getCurrency());
								}
							}
	
							if (!NullCheckUtils.isNullOrEmpty(obTransaction.getCurrencyExchange().getQuotationDate())) {
								commonAccountValidations.validateAndParseDateTimeFormatForResponse(
										obTransaction.getCurrencyExchange().getQuotationDate());
							}
						}
						if (!NullCheckUtils.isNullOrEmpty(obTransaction.getBalance())) {
							if (!NullCheckUtils.isNullOrEmpty(obTransaction.getBalance().getAmount())) {
								if (!NullCheckUtils.isNullOrEmpty(obTransaction.getBalance().getAmount().getAmount())) {
									commonAccountValidations.validateAmount(obTransaction.getAmount().getAmount());
								}
								if (!NullCheckUtils.isNullOrEmpty(obTransaction.getBalance().getAmount().getCurrency())) {
									commonAccountValidations.isValidCurrency(obTransaction.getAmount().getCurrency());
								}
							}
	
						}
	
						if (!NullCheckUtils.isNullOrEmpty(obTransaction.getCreditorAgent())) {
							commonAccountValidations.validateCreditorOrDebtorAgent(obTransaction.getCreditorAgent());
	
						}
	
						if (!NullCheckUtils.isNullOrEmpty(obTransaction.getDebtorAgent())) {
							commonAccountValidations.validateCreditorOrDebtorAgent(obTransaction.getDebtorAgent());
	
						}
	
						if (!NullCheckUtils.isNullOrEmpty(obTransaction.getDebtorAccount())) {
							if (!NullCheckUtils.isNullOrEmpty(obTransaction.getDebtorAccount().getSchemeName())
									&& !NullCheckUtils
											.isNullOrEmpty(obTransaction.getDebtorAccount().getIdentification())) {
								commonAccountValidations.validateSchemeNameWithIdentification(
										obTransaction.getDebtorAccount().getSchemeName(),
										obTransaction.getDebtorAccount().getIdentification());
							}
							if (!NullCheckUtils.isNullOrEmpty(obTransaction.getDebtorAccount().getSchemeName())
									&& !NullCheckUtils
											.isNullOrEmpty(obTransaction.getDebtorAccount().getSecondaryIdentification())) {
								commonAccountValidations.validateSchemeNameWithSecondaryIdentification(
										obTransaction.getDebtorAccount().getSchemeName(),
										obTransaction.getDebtorAccount().getSecondaryIdentification());
							}
						}
	
						if (!NullCheckUtils.isNullOrEmpty(obTransaction.getCreditorAccount())) {
							if (!NullCheckUtils.isNullOrEmpty(obTransaction.getCreditorAccount().getSchemeName())
									&& !NullCheckUtils
											.isNullOrEmpty(obTransaction.getCreditorAccount().getIdentification())) {
								commonAccountValidations.validateSchemeNameWithIdentification(
										obTransaction.getCreditorAccount().getSchemeName(),
										obTransaction.getCreditorAccount().getIdentification());
							}
							if (!NullCheckUtils.isNullOrEmpty(obTransaction.getCreditorAccount().getSchemeName())
									&& !NullCheckUtils
											.isNullOrEmpty(obTransaction.getCreditorAccount().getSecondaryIdentification())) {
								commonAccountValidations.validateSchemeNameWithSecondaryIdentification(
										obTransaction.getCreditorAccount().getSchemeName(),
										obTransaction.getCreditorAccount().getSecondaryIdentification());
							}
						}
	
					}
				}
			}
			if (!NullCheckUtils.isNullOrEmpty(obReadTransaction3.getMeta())
					&& !NullCheckUtils.isNullOrEmpty(obReadTransaction3.getMeta().getFirstAvailableDateTime())) {
				commonAccountValidations.validateAndParseDateTimeFormatForResponse(
						obReadTransaction3.getMeta().getFirstAvailableDateTime());
			}

			if (!NullCheckUtils.isNullOrEmpty(obReadTransaction3.getMeta())
					&& !NullCheckUtils.isNullOrEmpty(obReadTransaction3.getMeta().getLastAvailableDateTime())) {
				commonAccountValidations.validateAndParseDateTimeFormatForResponse(
						obReadTransaction3.getMeta().getLastAvailableDateTime());
			}

		}
	}
}