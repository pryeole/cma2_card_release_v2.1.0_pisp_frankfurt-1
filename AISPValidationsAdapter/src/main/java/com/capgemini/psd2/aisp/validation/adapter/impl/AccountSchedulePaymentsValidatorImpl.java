package com.capgemini.psd2.aisp.validation.adapter.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.domain.OBExternalPermissions1Code;
import com.capgemini.psd2.aisp.domain.OBReadScheduledPayment1;
import com.capgemini.psd2.aisp.domain.OBScheduledPayment1;
import com.capgemini.psd2.aisp.validation.adapter.AISPCustomValidator;
import com.capgemini.psd2.aisp.validation.adapter.utilities.CommonAccountValidations;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.validator.PSD2Validator;

@Component("accountSchedulePaymentsValidator")
@ConfigurationProperties("app")
public class AccountSchedulePaymentsValidatorImpl
		implements AISPCustomValidator<OBReadScheduledPayment1, OBReadScheduledPayment1> {

	@Autowired
	private PSD2Validator psd2Validator;

	@Value("${app.swaggerValidations.response:#{true}}")
	private Boolean resValidationEnabled;

	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;
	
	@Autowired
	private CommonAccountValidations commonAccountValidations;

	@Override
	public OBReadScheduledPayment1 validateRequestParams(OBReadScheduledPayment1 obReadScheduledPayment1) {
		return obReadScheduledPayment1;
	}

	@Override
	public boolean validateResponseParams(OBReadScheduledPayment1 oBReadScheduledPayment1) {
		if (resValidationEnabled)
			executeAccountScheduledPaymentSwaggerValidations(oBReadScheduledPayment1);
		executeAccountScheduledPaymentCustomValidations(oBReadScheduledPayment1);
		return true;
	}
	
	//swagger validation for AccountScheduledPayment
	private void executeAccountScheduledPaymentSwaggerValidations(OBReadScheduledPayment1 oBReadScheduledPayment1) {
		psd2Validator.validate(oBReadScheduledPayment1);
	}
	
	//custom validation for AccountScheduledPayment
	private void executeAccountScheduledPaymentCustomValidations(OBReadScheduledPayment1 oBReadScheduledPayment1) {
		if((!NullCheckUtils.isNullOrEmpty(oBReadScheduledPayment1)) && (!NullCheckUtils.isNullOrEmpty(oBReadScheduledPayment1.getData()))
				&& (!NullCheckUtils.isNullOrEmpty(oBReadScheduledPayment1.getData().getScheduledPayment()))){
			//Validate Permission
			validatePermissionBasedResponse(oBReadScheduledPayment1.getData().getScheduledPayment());
			
			for(OBScheduledPayment1 obScheduledPayment1 : oBReadScheduledPayment1.getData().getScheduledPayment()){
				if(!NullCheckUtils.isNullOrEmpty(obScheduledPayment1)) {
					//validate CeditorAgent
					if(!NullCheckUtils.isNullOrEmpty(obScheduledPayment1.getCreditorAgent()))
						commonAccountValidations.validateCreditorAgent(obScheduledPayment1.getCreditorAgent());
					
					//validate date & time
					if(!NullCheckUtils.isNullOrEmpty(obScheduledPayment1.getScheduledPaymentDateTime()))
						commonAccountValidations.validateAndParseDateTimeFormatForResponse(obScheduledPayment1.getScheduledPaymentDateTime());
					
					//Check InstrucctedAmount is null or empty
					if(!NullCheckUtils.isNullOrEmpty(obScheduledPayment1.getInstructedAmount())){
						//validate amount
						if(!NullCheckUtils.isNullOrEmpty(obScheduledPayment1.getInstructedAmount().getAmount()))
							commonAccountValidations.validateAmount(obScheduledPayment1.getInstructedAmount().getAmount());
						
						//validate currency
						if(!NullCheckUtils.isNullOrEmpty(obScheduledPayment1.getInstructedAmount().getCurrency()))
							commonAccountValidations.isValidCurrency(obScheduledPayment1.getInstructedAmount().getCurrency());
					}
					
					//check CreditorAccount is null or empty
					if(!NullCheckUtils.isNullOrEmpty(obScheduledPayment1.getCreditorAccount())){
						//validate SchemeName with Identification for CreditorAccount
						if((!NullCheckUtils.isNullOrEmpty(obScheduledPayment1.getCreditorAccount().getSchemeName())) && 
								!NullCheckUtils.isNullOrEmpty(obScheduledPayment1.getCreditorAccount().getIdentification()))
							commonAccountValidations.validateSchemeNameWithIdentification(obScheduledPayment1.getCreditorAccount().getSchemeName(),
																					obScheduledPayment1.getCreditorAccount().getIdentification());
						//validate SchemeName with SecondryIdentification for CreditorAccount
						if((!NullCheckUtils.isNullOrEmpty(obScheduledPayment1.getCreditorAccount().getSchemeName())) &&
								!NullCheckUtils.isNullOrEmpty(obScheduledPayment1.getCreditorAccount().getSecondaryIdentification()))
							commonAccountValidations.validateSchemeNameWithSecondaryIdentification(obScheduledPayment1.getCreditorAccount().getSchemeName(),
																				obScheduledPayment1.getCreditorAccount().getSecondaryIdentification());
					}
				}
			}
		}
	}
	
	private void validatePermissionBasedResponse(List<OBScheduledPayment1> oBscheduledPayment1List){
		if(!NullCheckUtils.isNullOrEmpty(reqHeaderAtrributes.getClaims())){
			if((reqHeaderAtrributes.getClaims().toString().toUpperCase()).contains(OBExternalPermissions1Code.READSCHEDULEDPAYMENTSDETAIL.toString().toUpperCase())){
				for(OBScheduledPayment1 oBScheduledPayment1 : oBscheduledPayment1List){
					if(!NullCheckUtils.isNullOrEmpty(oBScheduledPayment1)){
						if(NullCheckUtils.isNullOrEmpty(oBScheduledPayment1.getCreditorAccount()))
							throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
								InternalServerErrorMessage.INSUFFICIENT_DATA_FOR_DETAIL_PERMISSION));
					}
				}
			 }
		}
	}
	
	@Override
	public void validateUniqueId(String consentId) {
		if (NullCheckUtils.isNullOrEmpty(consentId))
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID,
					ErrorMapKeys.VALIDATION_ERROR));
		commonAccountValidations.validateUniqueUUID(consentId);
	}


}