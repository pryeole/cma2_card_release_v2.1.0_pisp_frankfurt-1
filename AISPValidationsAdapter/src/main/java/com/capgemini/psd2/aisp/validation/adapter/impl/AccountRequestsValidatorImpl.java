package com.capgemini.psd2.aisp.validation.adapter.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.domain.OBReadConsent1;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1;
import com.capgemini.psd2.aisp.validation.adapter.AISPCustomValidator;
import com.capgemini.psd2.aisp.validation.adapter.utilities.CommonAccountValidations;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.validator.PSD2Validator;

@Component("accountRequestsValidator")
@ConfigurationProperties("app")
public class AccountRequestsValidatorImpl implements
AISPCustomValidator<OBReadConsent1, OBReadConsentResponse1> {

	@Autowired
	private PSD2Validator psd2Validator;
	
	@Value("${app.swaggerValidations.response:#{true}}")
	private Boolean resValidationEnabled;
	
	@Value("${app.swaggerValidations.request:#{true}}")
	private Boolean reqValidationEnabled;
	
	@Autowired
	private CommonAccountValidations commonAccountValidations;

	@Override
	public OBReadConsent1 validateRequestParams(OBReadConsent1 oBReadConsent1) {
		if (reqValidationEnabled)
			executeAccountRequestSwaggerValidations(oBReadConsent1);
		return executeAccountRequestCustomValidations(oBReadConsent1);
	}

	private void executeAccountRequestSwaggerValidations(OBReadConsent1 oBReadConsent1) {
		psd2Validator.validateWithError(oBReadConsent1,
				ErrorCodeEnum.INVALID_PAYLOAD_SRUCTURE.toString());
	}

	private OBReadConsent1 executeAccountRequestCustomValidations(OBReadConsent1 oBReadConsent1) {
		return commonAccountValidations.validateAccountRequestInputs(oBReadConsent1);
	}

	@Override
	public boolean validateResponseParams(OBReadConsentResponse1 oBReadConsentResponse1) {
		if (reqValidationEnabled)
			executeAccountResponseSwaggerValidations(oBReadConsentResponse1);
		executeAccountResponseCustomValidations(oBReadConsentResponse1);
		return true;
	}

	private void executeAccountResponseSwaggerValidations(OBReadConsentResponse1 oBReadConsentResponse1) {
		psd2Validator.validateWithError(oBReadConsentResponse1,
				ErrorCodeEnum.INVALID_PAYLOAD_SRUCTURE.toString());
		
	}

	private void executeAccountResponseCustomValidations(OBReadConsentResponse1 oBReadConsentResponse1) {
		
		// Mandatory Field - Creation Date Time
		if(!NullCheckUtils.isNullOrEmpty(oBReadConsentResponse1.getData().getCreationDateTime())) {
			commonAccountValidations.validateAndParseDateTimeFormatForResponse(oBReadConsentResponse1.getData().getCreationDateTime());
		}

		// Mandatory Field - Status Date Time
		if(!NullCheckUtils.isNullOrEmpty(oBReadConsentResponse1.getData().getStatusUpdateDateTime())) {
			commonAccountValidations.validateAndParseDateTimeFormatForResponse(oBReadConsentResponse1.getData().getStatusUpdateDateTime());
		}
		
		// Not Mandatory Field - Expiration Date Time
		if(!NullCheckUtils.isNullOrEmpty(oBReadConsentResponse1.getData().getExpirationDateTime())){
			commonAccountValidations.validateAndParseDateTimeFormatForResponse(oBReadConsentResponse1.getData().getExpirationDateTime());
		}
		
		// Not Mandatory Field - TransactionFromDateTime
		if(!NullCheckUtils.isNullOrEmpty(oBReadConsentResponse1.getData().getTransactionFromDateTime())){
			commonAccountValidations.validateAndParseDateTimeFormatForResponse(oBReadConsentResponse1.getData().getTransactionFromDateTime());
		}
		
		// Not Mandatory Field - TransactionToDateTime
		if(!NullCheckUtils.isNullOrEmpty(oBReadConsentResponse1.getData().getTransactionToDateTime())){
			commonAccountValidations.validateAndParseDateTimeFormatForResponse(oBReadConsentResponse1.getData().getTransactionToDateTime());
		}
	}

	@Override
	public void validateUniqueId(String consentId) {
		if (NullCheckUtils.isNullOrEmpty(consentId)) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID,
					ErrorMapKeys.VALIDATION_ERROR));
		}
		commonAccountValidations.validateUniqueUUID(consentId);		
	}

}