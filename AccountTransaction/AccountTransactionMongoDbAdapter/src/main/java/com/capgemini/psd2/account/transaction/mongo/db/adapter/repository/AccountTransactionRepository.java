package com.capgemini.psd2.account.transaction.mongo.db.adapter.repository;

import java.time.LocalDateTime;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import com.capgemini.psd2.aisp.mongo.db.adapter.domain.AccountTransactionCMA2;

/**//**
 * The Interface AccountTransactionRepository.
 *//*
*/
public interface AccountTransactionRepository
		extends PagingAndSortingRepository<AccountTransactionCMA2, String> {

	Page<AccountTransactionCMA2> findByAccountNumberAndAccountNSCAndBookingDateTimeCopyBetween(String accountNumber, String accountNsc,
			LocalDateTime fromBookingDateTime, LocalDateTime toBookingDateTime, Pageable pageable);

	Page<AccountTransactionCMA2> findByAccountNumberAndAccountNSCAndCreditDebitIndicatorAndBookingDateTimeCopyBetween(
			String accountNumber, String accountNsc, String indicator, LocalDateTime fromBookingDateTime,
			LocalDateTime toBookingDateTime, Pageable pageable);
}