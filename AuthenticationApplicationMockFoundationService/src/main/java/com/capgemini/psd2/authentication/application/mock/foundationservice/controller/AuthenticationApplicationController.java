package com.capgemini.psd2.authentication.application.mock.foundationservice.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.authentication.application.mock.foundationservice.domain.AuthenticationRequest;
import com.capgemini.psd2.authentication.application.mock.foundationservice.domain.DigitalUser;
import com.capgemini.psd2.authentication.application.mock.foundationservice.domain.DigitalUserRequest;
import com.capgemini.psd2.authentication.application.mock.foundationservice.service.AuthenticationApplicationService;
import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceAuthException;
import com.capgemini.psd2.foundationservice.utilities.NullCheckUtils;



@RestController
public class AuthenticationApplicationController {

	@Autowired
	private AuthenticationApplicationService authenticationApplicationService;

	@RequestMapping(value = "/fs-authentication-service/services/business/login", method = RequestMethod.POST,consumes= "application/json", produces = "application/json")
	@ResponseBody
	public ResponseEntity authenticateBOLUserLogin(@RequestBody DigitalUserRequest digitalUser,
			
			@RequestHeader(required = false, value = "X-API-SOURCE-USER") String boiUser,
			@RequestHeader(required = true, value = "X-API-SOURCE-SYSTEM") String boiChannel,
			@RequestHeader(required = false, value = "X-BOI-PLATFORM") String boiPlatform,
			@RequestHeader(required = false, value = "X-API-CORRELATION-ID") String correlationID,
			@RequestHeader(required = false, value = "X-API-TRANSANCTION-ID") String transanctionID) {

		if ( NullCheckUtils.isNullOrEmpty(boiChannel)) {
			throw MockFoundationServiceAuthException.populateMockFoundationServiceException(ErrorCodeEnum.AUTHENTICATION_FAILURE_HEADER_AUTH);
		}
/*
		String userName = authenticationRequest.getDigitalUser().getDigitalUserIdentifier();
		String password = authenticationRequest.getDigitalUser().getCredentialsSourceSystem();
		
		
		if (NullCheckUtils.isNullOrEmpty(userName) || NullCheckUtils.isNullOrEmpty(password)) {
			throw MockFoundationServiceAuthException.populateMockFoundationServiceException(ErrorCodeEnum.GENERAL_ERROR_AUTH);
		}
*/
		//String digitalId = digitalUser.getDigitalId();
		authenticationApplicationService.retrieveBOLUserCredentials(digitalUser);

		return new ResponseEntity<>(digitalUser,HttpStatus.CREATED);

	}
	
	
	@RequestMapping(value = "/fs-authentication-service/services/personal/login", method = RequestMethod.POST, consumes= {MediaType.APPLICATION_XML_VALUE}, produces = {MediaType.APPLICATION_XML_VALUE })
	@ResponseBody
	public ResponseEntity authenticateB365UserLogin(@RequestBody AuthenticationRequest authenticationRequest,
			
			@RequestHeader(required = false, value = "X-BOI-USER") String boiUser,
			@RequestHeader(required = false, value = "X-BOI-CHANNEL") String boiChannel,
			@RequestHeader(required = false, value = "X-BOI-PLATFORM") String boiPlatform,
			@RequestHeader(required = false, value = "X-CORRELATION-ID") String correlationID) {

		if (NullCheckUtils.isNullOrEmpty(boiUser) || NullCheckUtils.isNullOrEmpty(boiChannel)
				|| NullCheckUtils.isNullOrEmpty(boiPlatform) || NullCheckUtils.isNullOrEmpty(correlationID)) {
			throw MockFoundationServiceAuthException.populateMockFoundationServiceException(ErrorCodeEnum.AUTHENTICATION_FAILURE_HEADER_AUTH);
		}

		String userName = authenticationRequest.getUserName();
		String password = authenticationRequest.getPassword();
		
		if (NullCheckUtils.isNullOrEmpty(userName) || NullCheckUtils.isNullOrEmpty(password)) {
			throw MockFoundationServiceAuthException.populateMockFoundationServiceException(ErrorCodeEnum.GENERAL_ERROR_AUTH);
		}

		authenticationApplicationService.retrieveB365UserCredentials(userName, password);

		return new ResponseEntity(HttpStatus.NO_CONTENT);
	}


	@RequestMapping(value = "/authentication-service/1.0.0/it-boi/authentication/p/v1.0/digital-user/login", method = RequestMethod.POST,consumes=MediaType.APPLICATION_JSON_VALUE , produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity authenticateDigitalUserLogin(@RequestBody DigitalUserRequest digitalUserRequest,
			@RequestHeader(required = false, value = "X-API-SOURCE-USER") String boiUser,
			@RequestHeader(required = false, value = "X-API-SOURCE-SYSTEM") String boiChannel,
			@RequestHeader(required = false, value = "X-BOI-PLATFORM") String boiPlatform,
			@RequestHeader(required = false, value = "X-API-CORRELATION-ID") String correlationID,
			@RequestHeader(required = false, value = "X-API-TRANSANCTION-ID") String transanctionID) 
		 {

		if (NullCheckUtils.isNullOrEmpty(boiUser) || NullCheckUtils.isNullOrEmpty(boiChannel)
				|| NullCheckUtils.isNullOrEmpty(boiPlatform) || NullCheckUtils.isNullOrEmpty(correlationID)) {
			throw MockFoundationServiceAuthException.populateMockFoundationServiceException(ErrorCodeEnum.AUTHENTICATION_FAILURE_HEADER_AUTH);
		}

		 String digitalUserIdentifier =  digitalUserRequest.getDigitalUser().getDigitalUserIdentifier();
		 /*String credentialsSourceSystem = digitalUserRequest.getDigitalUser().getCredentialsSourceSystem();
		 String secureAccessKey = digitalUserRequest.getDigitalUser().getCredentials().toString();
		 String secureAccessKeyCode = digitalUserRequest.getDigitalUser().getCredentials().toString();
		 String additionalParameters = digitalUserRequest.getDigitalUser().getCredentials().toString();*/
		  
		/*if (NullCheckUtils.isNullOrEmpty(digitalUserIdentifier) || NullCheckUtils.isNullOrEmpty(credentialsSourceSystem) || NullCheckUtils.isNullOrEmpty(secureAccessKey) 
				|| NullCheckUtils.isNullOrEmpty(secureAccessKeyCode) || NullCheckUtils.isNullOrEmpty(additionalParameters)) {
			throw MockFoundationServiceAuthException.populateMockFoundationServiceException(ErrorCodeEnum.GENERAL_ERROR_AUTH);
		}
*/
		return new ResponseEntity(HttpStatus.NO_CONTENT);

	}
}
