package com.capgemini.psd2.pisp.payment.setup.test.mock.data;

import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTResponse;
import com.fasterxml.jackson.databind.ObjectMapper;

public class DPaymentConsentsPOSTRequestResponseMockData {

	public static CustomDPaymentConsentsPOSTResponse getPaymentConsentsPOSTRequest() {
		return new CustomDPaymentConsentsPOSTResponse();
	}

	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}