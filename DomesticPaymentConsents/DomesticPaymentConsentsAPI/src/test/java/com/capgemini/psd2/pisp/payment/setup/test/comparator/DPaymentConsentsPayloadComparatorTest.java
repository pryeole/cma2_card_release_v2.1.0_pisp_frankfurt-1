package com.capgemini.psd2.pisp.payment.setup.test.comparator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBDomestic1;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBExternalAuthorisation1Code;
import com.capgemini.psd2.pisp.domain.OBPostalAddress6;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticConsent1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticConsentResponse1;
import com.capgemini.psd2.pisp.domain.OBWriteDomesticConsentResponse1;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.payment.setup.comparator.DPaymentConsentsPayloadComparator;

@RunWith(SpringJUnit4ClassRunner.class)
public class DPaymentConsentsPayloadComparatorTest {

	@InjectMocks
	private DPaymentConsentsPayloadComparator comparator;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void compareTest() {
		OBWriteDomesticConsentResponse1 response = new OBWriteDomesticConsentResponse1();
		OBWriteDataDomesticConsentResponse1 data = new OBWriteDataDomesticConsentResponse1();
		response.setData(data);
		OBDomestic1 initiation = new OBDomestic1();
		initiation.setLocalInstrument("Localinstrumenttest");
		data.setInitiation(initiation);
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		initiation.setInstructedAmount(instructedAmount);
		String amnt = "123.12d";
		instructedAmount.setAmount(amnt);
		OBRisk1 risk = new OBRisk1();
		response.setRisk(risk);
		risk.setMerchantCustomerIdentification("1234");

		OBWriteDomesticConsentResponse1 adaptedPaymentConsentsResponse = new OBWriteDomesticConsentResponse1();

		OBWriteDataDomesticConsentResponse1 data1 = new OBWriteDataDomesticConsentResponse1();
		adaptedPaymentConsentsResponse.setData(data1);
		data1.setInitiation(initiation);
		initiation.setInstructedAmount(instructedAmount);
		initiation.setLocalInstrument("Localinstrumenttest");
		OBRisk1 risk1 = new OBRisk1();
		adaptedPaymentConsentsResponse.setRisk(risk1);
		risk1.setMerchantCustomerIdentification("1234");
		assertNotNull(response);
		assertNotNull(adaptedPaymentConsentsResponse);
		int x = comparator.compare(response, adaptedPaymentConsentsResponse);
		assertEquals(0, x);
	}

	@Test
	public void comparetest1() {

		CustomDPaymentConsentsPOSTRequest request = new CustomDPaymentConsentsPOSTRequest();
		OBRisk1 risk = new OBRisk1();
		OBWriteDataDomesticConsent1 data = new OBWriteDataDomesticConsent1();

		OBDomestic1 initiation = new OBDomestic1();
		data.setInitiation(initiation);

		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		initiation.setCreditorAccount(creditorAccount);

		creditorAccount.setIdentification("08080021325698");
		creditorAccount.setName("ACME Inc");
		creditorAccount.setSecondaryIdentification("0002");

		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setName("Bob Clements");
		debtorAccount.setSchemeName("SortCodeAccountNumber");
		debtorAccount.setIdentification("08080021325698");
		initiation.setDebtorAccount(debtorAccount);
		request.setData(data);
		request.setRisk(risk);

		PaymentConsentsPlatformResource paymentSetupPlatformResource = new PaymentConsentsPlatformResource();
		paymentSetupPlatformResource.setId("144");
		paymentSetupPlatformResource.setPaymentId("58923-001");
		paymentSetupPlatformResource.setPaymentConsentId("58923");
		paymentSetupPlatformResource.setSetupCmaVersion("3");
		String tppDebtorDetails = "false";
		paymentSetupPlatformResource.setTppDebtorDetails(tppDebtorDetails);
		String tppDebtorNameDetails = "TDebtorS";
		paymentSetupPlatformResource.setTppDebtorNameDetails(tppDebtorNameDetails);

		CustomDPaymentConsentsPOSTResponse response = new CustomDPaymentConsentsPOSTResponse();
		OBWriteDataDomesticConsentResponse1 data1 = new OBWriteDataDomesticConsentResponse1();
		response.setData(data1);
		OBDomestic1 initiation1 = new OBDomestic1();
		data1.setInitiation(initiation1);
		initiation1.setInstructionIdentification("9999");
		comparator.comparePaymentDetails(response, request, paymentSetupPlatformResource);
	}

	@Test
	public void comparetest2() {

		CustomDPaymentConsentsPOSTRequest request = new CustomDPaymentConsentsPOSTRequest();
		OBRisk1 risk = new OBRisk1();
		OBWriteDataDomesticConsent1 data = new OBWriteDataDomesticConsent1();

		OBDomestic1 initiation = new OBDomestic1();
		data.setInitiation(initiation);

		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		initiation.setCreditorAccount(creditorAccount);
		OBRemittanceInformation1 remittanceInformation = new OBRemittanceInformation1();
		remittanceInformation.setReference("788");
		remittanceInformation.setUnstructured("546");
		initiation.setRemittanceInformation(remittanceInformation);
		creditorAccount.setIdentification("08080021325698");
		creditorAccount.setName("ACME Inc");
		creditorAccount.setSecondaryIdentification("0002");

		OBCashAccountDebtor3 debtorAccount = null;

		initiation.setDebtorAccount(debtorAccount);
		request.setData(data);
		request.setRisk(risk);
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		data.setInitiation(initiation);
		initiation.setInstructedAmount(instructedAmount);
		instructedAmount.setAmount("20.0");
		OBPostalAddress6 creditorPostalAddress = new OBPostalAddress6();
		initiation.setCreditorPostalAddress(creditorPostalAddress);
		
		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);
		deliveryAddress.setBuildingNumber("572");
		deliveryAddress.setCountry("Ireland");
		List<String> addressLine = new ArrayList<>();
		deliveryAddress.setAddressLine(addressLine);
		addressLine.add("warner bros");
		addressLine.add("warner bros2");
		deliveryAddress.setCountrySubDivision("Dublin west");

		PaymentConsentsPlatformResource paymentSetupPlatformResource = new PaymentConsentsPlatformResource();
		paymentSetupPlatformResource.setId("144");
		paymentSetupPlatformResource.setPaymentId("58923-001");
		paymentSetupPlatformResource.setPaymentConsentId("58923");
		paymentSetupPlatformResource.setSetupCmaVersion("3");
		String tppDebtorDetails = "false";
		paymentSetupPlatformResource.setTppDebtorDetails(tppDebtorDetails);
		String tppDebtorNameDetails = "TDebtorS";
		paymentSetupPlatformResource.setTppDebtorNameDetails(tppDebtorNameDetails);

		CustomDPaymentConsentsPOSTResponse response = new CustomDPaymentConsentsPOSTResponse();
		OBWriteDataDomesticConsentResponse1 data1 = new OBWriteDataDomesticConsentResponse1();
		response.setData(data1);
		OBRisk1 risk1 = new OBRisk1();
		response.setRisk(risk1);
		OBDomestic1 initiation1 = new OBDomestic1();
		data1.setInitiation(initiation1);
		instructedAmount.setAmount("20.0");
		initiation1.setInstructedAmount(instructedAmount);
		initiation1.setInstructionIdentification("9999");
		initiation1.setCreditorPostalAddress(null);
		comparator.comparePaymentDetails(response, request, paymentSetupPlatformResource);
	}

	@Test
	public void comparetest_nullRemittance() {

		CustomDPaymentConsentsPOSTRequest request = new CustomDPaymentConsentsPOSTRequest();
		OBRisk1 risk = new OBRisk1();
		OBWriteDataDomesticConsent1 data = new OBWriteDataDomesticConsent1();

		OBDomestic1 initiation = new OBDomestic1();
		data.setInitiation(initiation);

		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		initiation.setCreditorAccount(creditorAccount);
		creditorAccount.setIdentification("08080021325698");
		creditorAccount.setName("ACME Inc");
		creditorAccount.setSecondaryIdentification("0002");

		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();

		debtorAccount.setName("Bob Clements");
		debtorAccount.setSchemeName("SortCodeAccountNumber");
		initiation.setDebtorAccount(debtorAccount);
		request.setData(data);
		request.setRisk(risk);

		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);
		deliveryAddress.setBuildingNumber("572");
		deliveryAddress.setCountry("Ireland");
		List<String> addressLine = new ArrayList<>();
		deliveryAddress.setAddressLine(addressLine);
		addressLine.add("warner bros");
		addressLine.add("warner bros2");
		String countrySubDivision = "Dublin west";
		deliveryAddress.setCountrySubDivision(countrySubDivision);

		PaymentConsentsPlatformResource paymentSetupPlatformResource = new PaymentConsentsPlatformResource();
		paymentSetupPlatformResource.setId("144");
		paymentSetupPlatformResource.setPaymentId("58923-001");
		paymentSetupPlatformResource.setPaymentConsentId("58923");
		paymentSetupPlatformResource.setSetupCmaVersion("3");
		String tppDebtorDetails = "True";
		paymentSetupPlatformResource.setTppDebtorDetails(tppDebtorDetails);
		String tppDebtorNameDetails = "TDebtorS";
		paymentSetupPlatformResource.setTppDebtorNameDetails(tppDebtorNameDetails);

		CustomDPaymentConsentsPOSTResponse response = new CustomDPaymentConsentsPOSTResponse();
		OBWriteDataDomesticConsentResponse1 data1 = new OBWriteDataDomesticConsentResponse1();
		response.setData(data1);
		OBRisk1 risk1 = new OBRisk1();
		response.setRisk(risk1);
		OBDomestic1 initiation1 = new OBDomestic1();
		initiation1.setDebtorAccount(debtorAccount);

		data1.setInitiation(initiation1);
		initiation1.setInstructionIdentification("9999");
		
		comparator.comparePaymentDetails(response, request, paymentSetupPlatformResource);
	}

	@Test
	public void comparetest_checkRemittanceInfo() {
		CustomDPaymentConsentsPOSTRequest request = new CustomDPaymentConsentsPOSTRequest();
		OBRisk1 risk = new OBRisk1();
		OBWriteDataDomesticConsent1 data = new OBWriteDataDomesticConsent1();

		OBDomestic1 initiation = new OBDomestic1();
		OBRemittanceInformation1 remittanceInformation = new OBRemittanceInformation1();
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("123.223");
		initiation.setInstructedAmount(instructedAmount);
		data.setInitiation(initiation);
		remittanceInformation.setReference(null);
		remittanceInformation.setUnstructured(null);
		initiation.setRemittanceInformation(remittanceInformation);
		
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		initiation.setCreditorAccount(creditorAccount);
		creditorAccount.setIdentification("08080021325698");
		creditorAccount.setName("ACME Inc");
		creditorAccount.setSecondaryIdentification("0002");
		
		request.setData(data);
		request.setRisk(risk);

		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);
		deliveryAddress.setBuildingNumber("572");
		deliveryAddress.setCountry("Ireland");
		List<String> addressLine = new ArrayList<>();
		deliveryAddress.setAddressLine(addressLine);
		addressLine.add("warner bros");
		addressLine.add("warner bros2");
		String countrySubDivision = "Dublin west";
		deliveryAddress.setCountrySubDivision(countrySubDivision);

		PaymentConsentsPlatformResource paymentSetupPlatformResource = new PaymentConsentsPlatformResource();
		paymentSetupPlatformResource.setId("144");
		paymentSetupPlatformResource.setPaymentId("58923-001");
		paymentSetupPlatformResource.setPaymentConsentId("58923");
		paymentSetupPlatformResource.setSetupCmaVersion("3");
		/*
		 * String tppDebtorDetails = "False";
		 * paymentSetupPlatformResource.setTppDebtorDetails(tppDebtorDetails); String
		 * tppDebtorNameDetails = "Bob Clements";
		 * paymentSetupPlatformResource.setTppDebtorNameDetails(tppDebtorNameDetails);
		 */

		CustomDPaymentConsentsPOSTResponse response = new CustomDPaymentConsentsPOSTResponse();
		OBWriteDataDomesticConsentResponse1 data1 = new OBWriteDataDomesticConsentResponse1();
		response.setData(data1);
		
		OBRisk1 risk1 = new OBRisk1();
		response.setRisk(risk1);
		OBDomestic1 initiation1 = new OBDomestic1();
		OBDomestic1InstructedAmount instructedAmount1 = new OBDomestic1InstructedAmount();
		instructedAmount1.setAmount("123.223");
		initiation1.setInstructedAmount(instructedAmount1);
		initiation1.setRemittanceInformation(null);

		initiation1.setInstructionIdentification("9999");
		data1.setInitiation(initiation1);
		
		comparator.comparePaymentDetails(response, request, paymentSetupPlatformResource);
	}
	
	@Test
	public void comparetest_nullAddressLine() {

		CustomDPaymentConsentsPOSTRequest request = new CustomDPaymentConsentsPOSTRequest();
		OBRisk1 risk = new OBRisk1();
		OBWriteDataDomesticConsent1 data = new OBWriteDataDomesticConsent1();

		OBDomestic1 initiation = new OBDomestic1();
		data.setInitiation(initiation);
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("20.0");
		initiation.setInstructedAmount(instructedAmount);

		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		initiation.setCreditorAccount(creditorAccount);
		creditorAccount.setIdentification("08080021325698");
		creditorAccount.setName("ACME Inc");
		creditorAccount.setSecondaryIdentification("0002");

		OBCashAccountDebtor3 debtorAccount = null;
		initiation.setDebtorAccount(debtorAccount);

		request.setData(data);
		request.setRisk(risk);
		data.setInitiation(initiation);
		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);
		deliveryAddress.setBuildingNumber("572");
		deliveryAddress.setCountry("Ireland");
		List<String> addressLine = new ArrayList<>();
		String e = "Ground Floor";
		addressLine.add(e);
		deliveryAddress.setAddressLine(addressLine);
		initiation.setCreditorPostalAddress(new OBPostalAddress6());
		initiation.getCreditorPostalAddress().setAddressLine(addressLine);
		String countrySubDivision = "Dublin west";
		deliveryAddress.setCountrySubDivision(countrySubDivision);

		PaymentConsentsPlatformResource paymentSetupPlatformResource = new PaymentConsentsPlatformResource();
		paymentSetupPlatformResource.setId("144");
		paymentSetupPlatformResource.setPaymentId("58923-001");
		paymentSetupPlatformResource.setPaymentConsentId("58923");
		paymentSetupPlatformResource.setSetupCmaVersion("3");
		String tppDebtorDetails = "false";
		paymentSetupPlatformResource.setTppDebtorDetails(tppDebtorDetails);
		String tppDebtorNameDetails = "TDebtorS";
		paymentSetupPlatformResource.setTppDebtorNameDetails(tppDebtorNameDetails);

		CustomDPaymentConsentsPOSTResponse response = new CustomDPaymentConsentsPOSTResponse();
		OBWriteDataDomesticConsentResponse1 data1 = new OBWriteDataDomesticConsentResponse1();
		response.setData(data1);
		OBRisk1 risk1 = new OBRisk1();
		response.setRisk(risk1);
		risk1.setDeliveryAddress(new OBRisk1DeliveryAddress());
		List<String> addressLineResponse = new ArrayList<>();
		String e1 = "Ground Floor";
		addressLine.add(e1);
		risk1.getDeliveryAddress().setAddressLine(addressLineResponse);
		OBDomestic1 initiation1 = new OBDomestic1();
		data1.setInitiation(initiation1);
		initiation1.setInstructedAmount(instructedAmount);
		instructedAmount.setAmount("20.0");
		initiation1.setInstructionIdentification("9999");
		comparator.comparePaymentDetails(response, request, paymentSetupPlatformResource);
	}

	@Test
	public void comparetest_nullAddressLine1() {

		CustomDPaymentConsentsPOSTRequest request = new CustomDPaymentConsentsPOSTRequest();
		OBRisk1 risk = new OBRisk1();
		OBWriteDataDomesticConsent1 data = new OBWriteDataDomesticConsent1();

		OBDomestic1 initiation = new OBDomestic1();
		data.setInitiation(initiation);
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("20.0");
		initiation.setInstructedAmount(instructedAmount);
		OBPostalAddress6 creditorPostalAddressD = new OBPostalAddress6();
		List<String> addressLineList = new ArrayList<String>();
		addressLineList.add("");
		creditorPostalAddressD.setAddressLine(addressLineList);
		initiation.setCreditorPostalAddress(creditorPostalAddressD);

		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		initiation.setCreditorAccount(creditorAccount);
		creditorAccount.setIdentification("08080021325698");
		creditorAccount.setName("ACME Inc");
		creditorAccount.setSecondaryIdentification("0002");

		request.setData(data);
		request.setRisk(risk);

		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);
		deliveryAddress.setBuildingNumber("572");
		deliveryAddress.setCountry("Ireland");
		List<String> addressLine = new ArrayList<>();
		addressLine.add("");
		deliveryAddress.setAddressLine(addressLine);
		String countrySubDivision = "Dublin west";
		deliveryAddress.setCountrySubDivision(countrySubDivision);

		PaymentConsentsPlatformResource paymentSetupPlatformResource = new PaymentConsentsPlatformResource();
		paymentSetupPlatformResource.setId("144");
		paymentSetupPlatformResource.setPaymentId("58923-001");
		paymentSetupPlatformResource.setPaymentConsentId("58923");
		paymentSetupPlatformResource.setSetupCmaVersion("3");

		CustomDPaymentConsentsPOSTResponse response = new CustomDPaymentConsentsPOSTResponse();
		OBWriteDataDomesticConsentResponse1 data1 = new OBWriteDataDomesticConsentResponse1();
		response.setData(data1);
		OBRisk1 risk1 = new OBRisk1();
		response.setRisk(risk1);
		OBRisk1DeliveryAddress deliveryAddress1 = new OBRisk1DeliveryAddress();
		List<String> addressLine1 = null;
		deliveryAddress1.setAddressLine(addressLine1);
		risk1.setDeliveryAddress(deliveryAddress1 );
	
		OBDomestic1 initiation1 = new OBDomestic1();
		data1.setInitiation(initiation1);
		initiation1.setCreditorPostalAddress(new OBPostalAddress6());
		initiation1.setInstructionIdentification("9999");
		OBDomestic1InstructedAmount instructedAmount1 = new OBDomestic1InstructedAmount();
		instructedAmount1.setAmount("20.0");
		initiation1.setInstructedAmount(instructedAmount1);
		OBCashAccountDebtor3 debtorAccount1 = new OBCashAccountDebtor3();
		initiation1.setDebtorAccount(debtorAccount1);
		debtorAccount1.setName("PSD2");

		comparator.comparePaymentDetails(response, request, paymentSetupPlatformResource);
	}

	@Test
	public void comparetest_nullrequestDebtor() {

		CustomDPaymentConsentsPOSTRequest request = new CustomDPaymentConsentsPOSTRequest();
		OBRisk1 risk = new OBRisk1();
		OBWriteDataDomesticConsent1 data = new OBWriteDataDomesticConsent1();

		OBDomestic1 initiation = new OBDomestic1();
		data.setInitiation(initiation);
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("20.0");
		initiation.setInstructedAmount(instructedAmount);
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		initiation.setCreditorAccount(creditorAccount);
		creditorAccount.setIdentification("08080021325698");
		creditorAccount.setName("ACME Inc");
		creditorAccount.setSecondaryIdentification("0002");

		OBCashAccountDebtor3 debtorAccount = null;
		initiation.setDebtorAccount(debtorAccount);
		request.setData(data);
		request.setRisk(risk);
		data.setInitiation(initiation);
		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);
		deliveryAddress.setBuildingNumber("572");
		deliveryAddress.setCountry("Ireland");
		List<String> addressLine = new ArrayList<>();
		String e = "Ground Floor";
		addressLine.add(e);
		deliveryAddress.setAddressLine(addressLine);
		initiation.setCreditorPostalAddress(new OBPostalAddress6());
		initiation.getCreditorPostalAddress().setAddressLine(addressLine);

		String countrySubDivision = "Dublin west";
		deliveryAddress.setCountrySubDivision(countrySubDivision);

		PaymentConsentsPlatformResource paymentSetupPlatformResource = new PaymentConsentsPlatformResource();
		paymentSetupPlatformResource.setId("144");
		paymentSetupPlatformResource.setPaymentId("58923-001");
		paymentSetupPlatformResource.setPaymentConsentId("58923");
		paymentSetupPlatformResource.setSetupCmaVersion("3");
		String tppDebtorDetails = "false";
		paymentSetupPlatformResource.setTppDebtorDetails(tppDebtorDetails);
		String tppDebtorNameDetails = "TDebtorS";
		paymentSetupPlatformResource.setTppDebtorNameDetails(tppDebtorNameDetails);

		CustomDPaymentConsentsPOSTResponse response = new CustomDPaymentConsentsPOSTResponse();
		OBWriteDataDomesticConsentResponse1 data1 = new OBWriteDataDomesticConsentResponse1();
		response.setData(data1);
		OBRisk1 risk1 = new OBRisk1();
		response.setRisk(risk1);
		risk1.setDeliveryAddress(new OBRisk1DeliveryAddress());
		List<String> addressLineResponse = new ArrayList<>();
		String e1 = "Ground Floor";
		addressLine.add(e1);
		risk1.getDeliveryAddress().setAddressLine(addressLineResponse);
		data1.setInitiation(initiation);
		initiation.setInstructedAmount(instructedAmount);

		OBDomestic1 initiation1 = new OBDomestic1();
		data1.setInitiation(initiation1);
		initiation1.setInstructionIdentification("9999");
		OBCashAccountDebtor3 debtorAccount1 = new OBCashAccountDebtor3();
		initiation1.setDebtorAccount(debtorAccount1);
		initiation1.setInstructedAmount(instructedAmount);
		instructedAmount.setAmount("20.0");
		debtorAccount1.setName("PSD2");

		comparator.comparePaymentDetails(response, request, paymentSetupPlatformResource);
	}

	@Test
	public void comparetest_authorisationSame() {
		OBWriteDomesticConsentResponse1 response = new OBWriteDomesticConsentResponse1();
		OBWriteDataDomesticConsentResponse1 data = new OBWriteDataDomesticConsentResponse1();
		response.setData(data);
		OBDomestic1 initiation = new OBDomestic1();
		initiation.setLocalInstrument("Localinstrumenttest");
		data.setInitiation(initiation);
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		initiation.setInstructedAmount(instructedAmount);
		instructedAmount.setAmount("20.0");
		OBRisk1 risk = new OBRisk1();
		response.setRisk(risk);
		risk.setMerchantCustomerIdentification("1234");

		OBAuthorisation1 responseAuth = new OBAuthorisation1();
		data.setAuthorisation(responseAuth);
		response.setData(data);

		OBWriteDomesticConsentResponse1 adaptedPaymentConsentsResponse = new OBWriteDomesticConsentResponse1();

		OBWriteDataDomesticConsentResponse1 data1 = new OBWriteDataDomesticConsentResponse1();
		adaptedPaymentConsentsResponse.setData(data1);
		data1.setInitiation(initiation);
		initiation.setInstructedAmount(instructedAmount);
		initiation.setLocalInstrument("Localinstrumenttest");
		OBRisk1 risk1 = new OBRisk1();
		adaptedPaymentConsentsResponse.setRisk(risk);
		risk1.setMerchantCustomerIdentification("1234");

		OBAuthorisation1 adaptedResponseAuth = new OBAuthorisation1();
		data1.setAuthorisation(adaptedResponseAuth);
		adaptedPaymentConsentsResponse.setData(data1);

		int x = comparator.compare(response, adaptedPaymentConsentsResponse);
		assertEquals(0, x);
	}

	@Test
	public void comparetest_authorisationNotSame() {
		OBWriteDomesticConsentResponse1 response = new OBWriteDomesticConsentResponse1();
		OBWriteDataDomesticConsentResponse1 data = new OBWriteDataDomesticConsentResponse1();
		response.setData(data);
		OBDomestic1 initiation = new OBDomestic1();
		initiation.setLocalInstrument("Localinstrumenttest");
		data.setInitiation(initiation);
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		initiation.setInstructedAmount(instructedAmount);
		instructedAmount.setAmount("20.0");
		OBRisk1 risk = new OBRisk1();
		response.setRisk(risk);
		risk.setMerchantCustomerIdentification("1234");

		OBAuthorisation1 responseAuth = new OBAuthorisation1();
		data.setAuthorisation(responseAuth);
		response.setData(data);

		OBWriteDomesticConsentResponse1 adaptedPaymentConsentsResponse = new OBWriteDomesticConsentResponse1();

		OBWriteDataDomesticConsentResponse1 data1 = new OBWriteDataDomesticConsentResponse1();
		adaptedPaymentConsentsResponse.setData(data1);
		data1.setInitiation(initiation);
		initiation.setInstructedAmount(instructedAmount);
		initiation.setLocalInstrument("Localinstrumenttest");
		OBRisk1 risk1 = new OBRisk1();
		adaptedPaymentConsentsResponse.setRisk(risk);
		risk1.setMerchantCustomerIdentification("1234");

		OBAuthorisation1 adaptedResponseAuth = null;
		data1.setAuthorisation(adaptedResponseAuth);
		adaptedPaymentConsentsResponse.setData(data1);

		int x = comparator.compare(response, adaptedPaymentConsentsResponse);
		assertEquals(0, x);
	}


}