package com.capgemini.psd2.aspect;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class PSD2AspectTest {

	@Mock
	private PSD2AspectUtils aspectUtils;
	
	@Mock
	private JoinPoint joinPoint;
	
	@Mock
	private ProceedingJoinPoint proceedingJoinPoint;
	
	@Mock
	private Throwable throwable;
	
	@InjectMocks
	private PSD2Aspect psd2Aspect = new PSD2Aspect();

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void arroundLoggerAdviceServiceTest() {
		when(aspectUtils.methodAdvice(anyObject())).thenReturn(new Object());
		psd2Aspect.arroundLoggerAdviceService(proceedingJoinPoint);
	}
	
	@Test
	public void arroundLoggerAdviceServiceTest1() {
		when(aspectUtils.methodPayloadAdvice(anyObject())).thenReturn(new Object());
		psd2Aspect.arroundLoggerAdviceController(proceedingJoinPoint);
	}
	
	@Test
	public void arroundLoggerAdviceServiceTest2() {
		doNothing().when(aspectUtils).throwExceptionOnJsonBinding(anyObject(), anyObject());
		psd2Aspect.throwExcpetionOnJsonBinding(joinPoint,throwable);
	}
}
