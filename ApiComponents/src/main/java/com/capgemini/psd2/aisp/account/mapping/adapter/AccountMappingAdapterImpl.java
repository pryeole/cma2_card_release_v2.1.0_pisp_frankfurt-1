/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.aisp.account.mapping.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.account.identfication.mapping.adapter.domain.AccountIdentificationMapping;
import com.capgemini.psd2.aisp.account.identfication.mapping.adapter.domain.IdntificationMapping;
import com.capgemini.psd2.aisp.adapter.AccountIdentificationMappingAdapter;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.validator.PSD2Validator;

/**
 * The Class AccountMappingAdapterImpl.
 */
@Component
public class AccountMappingAdapterImpl implements AccountMappingAdapter {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(AccountMappingAdapterImpl.class);

	private static final String LOG_PARAM = "com.capgemini.psd2.aisp.account.mapping.adapter.retrieveAccountMappingDetails()";

	private static final String RETRIEVEACCOUNTMAPPINGDETAILS = "retrieveAccountMappingDetails";
	
	@Autowired
	private RequestHeaderAttributes reqHeaderAttributes;

	@Autowired
	@Qualifier("AccountMappingRoutingAdapter")
	private AccountIdentificationMappingAdapter accountIdentificationMappingAdapter;

	@Value("${app.mapping.storage.policy:#{true}}")
	private Boolean platformStored;

	@Autowired
	// @Qualifier("PSD2Validator")
	private PSD2Validator fieldValidator;

	/** The logger utils. */
	@Autowired
	private LoggerUtils loggerUtils;

	@Override
	public AccountMapping retrieveAccountMappingDetails(String accountId, AispConsent aispConsent) {

		LOG.info("{\"Enter\":\"{}\",\"{}\"}", LOG_PARAM, loggerUtils.populateLoggerData(RETRIEVEACCOUNTMAPPINGDETAILS));

		List<AccountDetails> accountDetailsList = new ArrayList<>();

		if (platformStored) {
			AccountDetails productStoredAccountDetail = aispConsent.getAccountDetails().stream()
					.filter(accDtl -> accDtl.getAccountId().equals(accountId)).findFirst().orElse(null);
			accountDetailsList.add(productStoredAccountDetail);

		} else {
			List<AccountDetails> consentedAccountDetailList = new ArrayList<>();
			consentedAccountDetailList.add(aispConsent.getAccountDetails().get(0));
			accountDetailsList = retrieveAccountDetailList(consentedAccountDetailList);
		}

		AccountMapping accountMapping = populateAccountMappingDetails(aispConsent, accountDetailsList);

		LOG.info("{\"Exit\":\"{}\",\"{}\"}", LOG_PARAM, loggerUtils.populateLoggerData(RETRIEVEACCOUNTMAPPINGDETAILS));

		return accountMapping;
	}

	@Override
	public AccountMapping retrieveAccountMappingDetails(AispConsent aispConsent) {

		LOG.info("{\"Enter\":\"{}\",\"{}\"}", LOG_PARAM, loggerUtils.populateLoggerData(RETRIEVEACCOUNTMAPPINGDETAILS));

		List<AccountDetails> accountDetailsList = new ArrayList<>();
		if (platformStored) {

			accountDetailsList.addAll(aispConsent.getAccountDetails());

		} else {

			accountDetailsList = retrieveAccountDetailList(aispConsent.getAccountDetails());
		}

		AccountMapping accountMapping = populateAccountMappingDetails(aispConsent, accountDetailsList);

		LOG.info("{\"Exit\":\"{}\",\"{}\"}", LOG_PARAM, loggerUtils.populateLoggerData(RETRIEVEACCOUNTMAPPINGDETAILS));

		return accountMapping;
	}

	private List<AccountDetails> retrieveAccountDetailList(List<AccountDetails> consentedAccountDetailList) {

		List<String> identifiersList = consentedAccountDetailList.stream().map(AccountDetails::getIdentifier)
				.collect(Collectors.toList());

		List<AccountDetails> accountDetailsList = new ArrayList<>();

		AccountIdentificationMapping stagedAccountIdentificationMapping = accountIdentificationMappingAdapter
				.retrieveAccountIdentificationMapping(identifiersList);

		if (stagedAccountIdentificationMapping == null)
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_ACCOUNT_MAPPING_RESOURCE_FOUND);

		List<IdntificationMapping> mappingList = stagedAccountIdentificationMapping.getIdentificationMappingList();

		if (mappingList == null || mappingList.isEmpty() || mappingList.get(0) == null)
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_ACCOUNT_MAPPING_RESOURCE_FOUND);

		consentedAccountDetailList.stream().forEach(consentedAccDtl -> {

			IdntificationMapping matchedMapping = mappingList.stream()
					.filter(stagedElement -> stagedElement.getIdentifier().equals(consentedAccDtl.getIdentifier()))
					.findFirst().orElse(null);

			if (matchedMapping == null)
				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.INVALID_ACCOUNT_MAPPING_RESOURCE_FOUND);

			// Validate all mandatory fields in AccountMapping
			fieldValidator.validateWithError(matchedMapping,
					ErrorCodeEnum.INVALID_ACCOUNT_MAPPING_RESOURCE_FOUND.toString());

			consentedAccDtl.setAccountNumber(matchedMapping.getAccountNumber());
			consentedAccDtl.setAccountNSC(matchedMapping.getAccountNSC());

			accountDetailsList.add(consentedAccDtl);
		});
		return accountDetailsList;
	}

	private AccountMapping populateAccountMappingDetails(AispConsent aispConsent,
			List<AccountDetails> accountDetailsList) {
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId(aispConsent.getPsuId());
		accountMapping.setTppCID(aispConsent.getTppCId());
		accountMapping.setCorrelationId(reqHeaderAttributes.getCorrelationId());
		accountMapping.setAccountDetails(accountDetailsList);
		return accountMapping;
	}

	
}
