package com.capgemini.psd2.aisp.account.mapping.adapter;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.account.identfication.mapping.adapter.domain.AccountIdentificationMapping;
import com.capgemini.psd2.aisp.adapter.AccountIdentificationMappingAdapter;

@Component("AccountMappingRoutingAdapter")
public class AccountMappingRoutingAdapter implements AccountIdentificationMappingAdapter, ApplicationContextAware {

	/** The application context. */
	private ApplicationContext applicationContext;

	/** The default adapter. */
	@Value("${app.mapping.storage.adapter:#{null}}")
	private String adapterName;

	@Override
	public AccountIdentificationMapping retrieveAccountIdentificationMapping(List<String> mappingIdentifierList) {
		AccountIdentificationMappingAdapter accountIdentificationMappingAdapter = getAdapterInstance(adapterName);
		return accountIdentificationMappingAdapter.retrieveAccountIdentificationMapping(mappingIdentifierList);
	}

	public AccountIdentificationMappingAdapter getAdapterInstance(String adapterName) {
		return (AccountIdentificationMappingAdapter) applicationContext.getBean(adapterName);

	}

	@Override
	public void setApplicationContext(ApplicationContext context) {
		this.applicationContext = context;

	}

}
