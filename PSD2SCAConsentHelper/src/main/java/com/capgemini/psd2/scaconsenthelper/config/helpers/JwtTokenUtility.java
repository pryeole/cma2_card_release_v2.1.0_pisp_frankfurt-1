package com.capgemini.psd2.scaconsenthelper.config.helpers;

import java.util.UUID;

import org.joda.time.DateTime;

import com.capgemini.psd2.scaconsenthelper.constants.SCAConsentHelperConstants;
import com.capgemini.psd2.scaconsenthelper.models.AccessJwtToken;
import com.capgemini.psd2.security.exceptions.PSD2SecurityException;
import com.capgemini.psd2.security.exceptions.SCAConsentErrorCodeEnum;
import com.nimbusds.jose.EncryptionMethod;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWEAlgorithm;
import com.nimbusds.jose.JWEHeader;
import com.nimbusds.jose.JWEObject;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSSigner;
import com.nimbusds.jose.Payload;
import com.nimbusds.jose.crypto.DirectEncrypter;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;


public final class JwtTokenUtility {

	private JwtTokenUtility() {

	}

	public static AccessJwtToken createSessionJwtToken(String tokenIssuer, Integer tokenExpirationTime,
			String tokenSigningKey,String intentData,String keyForEncryption)  {

		JWEObject jweObject = null;
		String jweString = null;
		try{
			DateTime currentTime = new DateTime();

			JWTClaimsSet jwtClaims = new JWTClaimsSet.Builder().issuer(tokenIssuer).issueTime(currentTime.toDate())
					.notBeforeTime(currentTime.toDate())
					.expirationTime(currentTime.plusMinutes(tokenExpirationTime).toDate()).claim(SCAConsentHelperConstants.INTENT_DATA, intentData).jwtID(UUID.randomUUID().toString()).build();
			
			JWSSigner signer = new MACSigner(tokenSigningKey);
			SignedJWT signedJWT = new SignedJWT(new JWSHeader(JWSAlgorithm.HS512), jwtClaims);
			signedJWT.sign(signer);
			signedJWT.getParsedString();
			
			//Encrpty JWT token
			JWEHeader header = new JWEHeader(JWEAlgorithm.DIR, EncryptionMethod.A128GCM);
			byte[] key = keyForEncryption.getBytes();
			Payload payload = new Payload(signedJWT);

			jweObject = new JWEObject(header, payload);
			jweObject.encrypt(new DirectEncrypter(key));
			jweString = jweObject.serialize();			
		}
		catch(JOSEException je){
			throw PSD2SecurityException.populatePSD2SecurityException("Invalid Cookie ..",
					SCAConsentErrorCodeEnum.INVALID_REQUEST);
		}
		return new AccessJwtToken(jweString);
	}

/*	public static AccessJwtToken createAccessJwtToken(UserContext userContext, String tokenIssuer,
			Integer tokenExpirationTime, String tokenSigningKey) {
		Claims claims = Jwts.claims().setSubject(userContext.getUsername());
		claims.put("scopes", userContext.getAuthorities().stream().map(s -> s.toString()).collect(Collectors.toList()));

		DateTime currentTime = new DateTime();

		String token = Jwts.builder().setClaims(claims).setIssuer(tokenIssuer).setId(UUID.randomUUID().toString())
				.setIssuedAt(currentTime.toDate()).setExpiration(currentTime.plusMinutes(tokenExpirationTime).toDate())
				.signWith(SignatureAlgorithm.HS512, tokenSigningKey).compact();

		return new AccessJwtToken(token, claims);
	}*/
}
