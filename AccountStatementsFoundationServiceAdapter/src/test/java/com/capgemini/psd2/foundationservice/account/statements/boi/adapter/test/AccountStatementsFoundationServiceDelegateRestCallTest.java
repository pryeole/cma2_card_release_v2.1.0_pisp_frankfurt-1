//
//
//package com.capgemini.psd2.foundationservice.account.statements.boi.adapter.test;
//
//
//
//import static org.junit.Assert.assertEquals;
//import static org.junit.Assert.assertNotNull;
//import static org.mockito.Matchers.any;
//import static org.mockito.Matchers.anyObject;
//
//import java.text.DateFormat;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.Mockito;
//import org.mockito.MockitoAnnotations;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpHeaders;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.util.ReflectionTestUtils;
//import org.springframework.util.LinkedMultiValueMap;
//import org.springframework.util.MultiValueMap;
//
//import com.capgemini.psd2.adapter.datetime.utility.StatementDateRange;
//import com.capgemini.psd2.adapter.datetime.utility.TimeZoneDateTimeAdapter;
//import com.capgemini.psd2.adapter.datetime.utility.TransactionDateRange;
//import com.capgemini.psd2.adapter.exceptions.AdapterException;
//import com.capgemini.psd2.adapter.utility.AdapterUtility;
//import com.capgemini.psd2.consent.domain.AccountDetails;
//import com.capgemini.psd2.consent.domain.AccountMapping;
//import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.client.AccountStatementsFoundationServiceClient;
//import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.constants.AccountStatementsFoundationServiceConstants;
//import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.delegate.AccountStatementsFoundationServiceDelegate;
//import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.raml.domain.StatementObject;
//import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.raml.domain.Statementsresponse;
//import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.transformer.AccountStatementsFoundationServiceTransformer;
//import com.capgemini.psd2.rest.client.model.RequestInfo;
//import com.capgemini.psd2.rest.client.sync.RestClientSync;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//public class AccountStatementsFoundationServiceDelegateRestCallTest {
//	/** The account statements file foundation service adapter. */
//
//	@InjectMocks
//	private AccountStatementsFoundationServiceDelegate delegate;
//
//	/** The account statements file foundation service client. */
//
//	@InjectMocks
//	private AccountStatementsFoundationServiceClient accountStatementsFileFoundationServiceClient;
//
//
//
//	/** The rest client. */
//
//	@Mock
//	private RestClientSync restClient;
//
//	/** The account statements file foundation service delegate. */
//
//	@Mock
//	private AccountStatementsFoundationServiceTransformer accountStatementsFileFSTransformer;
//
//	/**
//
//	 * Sets the up.
//
//	 */
//	
//	@Mock
//	private AccountStatementsFoundationServiceDelegate accountStatementsFileFoundationServiceDelegate;
//
//	@Mock
//	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;
//	
//	@Mock
//	private AdapterUtility adapterUtility;
//	
//	@Before
//	public void setUp(){
//MockitoAnnotations.initMocks(this);
//}
//
//	/**
//
//	 * Context loads.
//
//	 */
//
//	@Test
//	public void contextLoads() {
//
//	}
//
//
//
//	/**
//
//	 * Test account statements file FS.
//
//	 */
//	
//	
//	@Test
//	public void testrestTransportForAccountStatementsFile(){
//		RequestInfo reqInfo=new RequestInfo();
//		HttpHeaders headers=new HttpHeaders();
//		headers.add("X-API-TRANSACTION-ID", "correlationIdInReqHeader");
//		headers.add("X-API-SOURCE-SYSTEM", "sourceSystemReqHeader");
//		headers.add("X-API-SOURCE-USER", "sourceUserReqHeader");
//		headers.add("X-API-CHANNEL-CODE","channelInReqHeader" );
//		headers.add("X-MASKED-PAN","xMaskedPan" );
//		reqInfo.setUrl("http://localhost:8081//psd2-abt-service/services/abt/accounts/nsc/number");
//		accountStatementsFileFoundationServiceClient.restTransportForAccountStatementsFile(reqInfo, byte[].class, headers);
//	}
//
//	@Test
//	public void testrestTransportForAccountStatementsStmt(){
//		RequestInfo reqInfo=new RequestInfo();
//		HttpHeaders headers=new HttpHeaders();
//		Statementsresponse statements = new Statementsresponse();
//		StatementObject stmt = new StatementObject();
//		List<StatementObject> statementsList = new ArrayList<StatementObject>();
//		statementsList.add(stmt);
//		statements.setStatementsList(statementsList);
//		stmt.setDocumentIdentificationNumber("abcd");
//		
//		headers.add("X-API-TRANSACTION-ID", "correlationIdInReqHeader");
//		headers.add("X-API-SOURCE-SYSTEM", "sourceSystemReqHeader");
//		headers.add("X-API-SOURCE-USER", "sourceUserReqHeader");
//		headers.add("X-API-CHANNEL-CODE","channelInReqHeader" );
//		headers.add("X-MASKED-PAN","xMaskedPan" );
//		reqInfo.setUrl("http://localhost:8081//psd2-abt-service/services/abt/accounts/nsc/number");
//		
//	}
//	
//	@Test
//	public void testrestTransportForAccountStatementsList(){
//		RequestInfo reqInfo=new RequestInfo();
//		HttpHeaders headers=new HttpHeaders();
//		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
//		headers.add("X-API-TRANSACTION-ID", "correlationIdInReqHeader");
//		headers.add("X-API-SOURCE-SYSTEM", "sourceSystemReqHeader");
//		headers.add("X-API-SOURCE-USER", "sourceUserReqHeader");
//		headers.add("X-API-CHANNEL-CODE","channelInReqHeader" );
//		headers.add("X-MASKED-PAN","xMaskedPan" );
//		reqInfo.setUrl("http://localhost:8081//psd2-abt-service/services/abt/accounts/nsc/number");
//		accountStatementsFileFoundationServiceClient.restTransportForAccountStatementsStmt(reqInfo, Statementsresponse.class, params, headers);
//	}
//	
//	@Test
//	public void testCreateTransactionDateRangeIfCases(){
//		Map<String, String> params = new HashMap<>();
//		params.put(AccountStatementsFoundationServiceConstants.CONSENT_EXPIRATION_DATETIME, "2015-01-01T05:22:11");
//		params.put(AccountStatementsFoundationServiceConstants.FROM_DATE, "2015-01-01T00:00:00");
//		params.put(AccountStatementsFoundationServiceConstants.TO_DATE, "2015-01-01T00:00:00");
//		params.put(AccountStatementsFoundationServiceConstants.REQUESTED_FROM_CONSENT_DATETIME, "2015-01-01T00:00:00");
//		params.put(AccountStatementsFoundationServiceConstants.REQUESTED_FROM_DATETIME, "2015-01-01T00:00:00");
//		params.put(AccountStatementsFoundationServiceConstants.REQUESTED_TO_CONSENT_DATETIME, "2017-01-01T09:33:22");
//		params.put(AccountStatementsFoundationServiceConstants.REQUESTED_TO_DATETIME, "2017-01-01T09:33:22");
//		Mockito.when(timeZoneDateTimeAdapter.parseDateTimeFS(anyObject())).thenReturn(new Date());
//		Mockito.when(timeZoneDateTimeAdapter.parseDateFS(anyObject())).thenReturn(new Date());
//		ReflectionTestUtils.setField(delegate, "defaultStatementDateRange", 1);
//		StatementDateRange res = delegate.createTransactionDateRange(params);
//		assertNotNull(res);
//	}	
//	
//	@Test
//	public void testCreateTransactionDateRangeIfCases1(){
//		Map<String, String> params = new HashMap<>();
//		params.put(AccountStatementsFoundationServiceConstants.CONSENT_EXPIRATION_DATETIME, null);
//		params.put(AccountStatementsFoundationServiceConstants.FROM_DATE, null);
//		params.put(AccountStatementsFoundationServiceConstants.TO_DATE, null);
//		params.put(AccountStatementsFoundationServiceConstants.REQUESTED_FROM_CONSENT_DATETIME, null);
//		params.put(AccountStatementsFoundationServiceConstants.REQUESTED_FROM_DATETIME, null);
//		params.put(AccountStatementsFoundationServiceConstants.REQUESTED_TO_CONSENT_DATETIME, null);
//		params.put(AccountStatementsFoundationServiceConstants.REQUESTED_TO_DATETIME, null);
//		Mockito.when(timeZoneDateTimeAdapter.parseDateTimeFS(anyObject())).thenReturn(new Date());
//		Mockito.when(timeZoneDateTimeAdapter.parseDateFS(anyObject())).thenReturn(new Date());
//		ReflectionTestUtils.setField(delegate, "defaultStatementDateRange", 1);
//		StatementDateRange res = delegate.createTransactionDateRange(params);
//		assertNotNull(res);
//	}	
//	
//	@Test
//	public void testFsCallFilterException(){
//		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
//		StatementDateRange statementDateRange = new StatementDateRange();
//		try {
//			statementDateRange.setRequestDateTime(dateFormat.parse("2018-01-01T00:00:00"));
//			statementDateRange.setConsentExpiryDateTime(dateFormat.parse("2018-01-01T00:00:00"));
//			statementDateRange.setTransactionFromDateTime(dateFormat.parse("2018-01-01T00:00:00"));
//			statementDateRange.setFilterFromDate(dateFormat.parse("2018-01-01T00:00:00"));
//			statementDateRange.setFilterToDate(dateFormat.parse("2018-01-01T00:00:00"));
//			statementDateRange.setTransactionToDateTime(dateFormat.parse("2018-01-01T00:00:00"));
//		} catch (ParseException e) {
//			e.printStackTrace();
//		}
//		delegate.fsCallFilter(statementDateRange);
//	}
//	
//	@Test
//	public void testFsCallFilterException1(){
//		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
//		StatementDateRange statementDateRange = new StatementDateRange();
//		try {
//			statementDateRange.setRequestDateTime(dateFormat.parse("2018-01-01T00:00:00"));
//			statementDateRange.setConsentExpiryDateTime(dateFormat.parse("2018-01-01T00:00:00"));
//			statementDateRange.setFilterFromDate(dateFormat.parse("2018-01-01T00:00:00"));
//			statementDateRange.setFilterToDate(dateFormat.parse("2018-01-01T00:00:00"));
//			statementDateRange.setTransactionFromDateTime(dateFormat.parse("2018-01-01T00:00:00"));
//			statementDateRange.setTransactionToDateTime(dateFormat.parse("2018-01-01T00:00:00"));
//
//			
//          } catch (ParseException e) {
//			e.printStackTrace();
//		}
//		delegate.fsCallFilter(statementDateRange);
//	}
//	
//	@Test
//	public void testFsCallFilter1(){
//		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
//		StatementDateRange statementDateRange = new StatementDateRange();
//		try {
//			statementDateRange.setRequestDateTime(dateFormat.parse("2014-01-01T00:00:00"));
//			statementDateRange.setConsentExpiryDateTime(dateFormat.parse("2015-01-01T00:00:00"));
//			statementDateRange.setFilterFromDate(dateFormat.parse("2015-01-01T00:00:00"));
//			statementDateRange.setFilterToDate(dateFormat.parse("2017-01-01T00:00:00"));
//			statementDateRange.setTransactionFromDateTime(dateFormat.parse("2015-01-02T00:00:00"));
//			statementDateRange.setTransactionToDateTime(dateFormat.parse("2017-01-01T00:00:00"));
//		} catch (ParseException e) {
//			e.printStackTrace();
//		}
//		StatementDateRange res = delegate.fsCallFilter(statementDateRange);
//		assertEquals(true, res.isEmptyResponse());
//	}
//	
//	@Test
//	public void testFsCallFilter2(){
//		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
//		StatementDateRange statementDateRange = new StatementDateRange();
//		try {
//			statementDateRange.setRequestDateTime(dateFormat.parse("2014-01-01T00:00:00"));
//			statementDateRange.setConsentExpiryDateTime(dateFormat.parse("2015-01-01T00:00:00"));
//			statementDateRange.setFilterFromDate(dateFormat.parse("2015-01-01T00:00:00"));
//			statementDateRange.setFilterToDate(dateFormat.parse("2017-01-01T00:00:00"));
//			statementDateRange.setTransactionFromDateTime(dateFormat.parse("2014-01-01T00:00:00"));
//			statementDateRange.setTransactionToDateTime(dateFormat.parse("2017-01-01T00:00:00"));
//			statementDateRange.setFilterToDate(dateFormat.parse("2015-01-01T00:00:00"));
//			statementDateRange.setFilterFromDate(dateFormat.parse("2015-01-01T00:00:00"));
//		} catch (ParseException e) {
//			e.printStackTrace();
//		}
//		delegate.fsCallFilter(statementDateRange);
//	}
//	
//	@Test
//	public void testFsCallFilter3(){
//		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
//		StatementDateRange statementDateRange = new StatementDateRange();
//		try {
//			statementDateRange.setRequestDateTime(dateFormat.parse("2014-01-01T00:00:00"));
//			statementDateRange.setConsentExpiryDateTime(dateFormat.parse("2015-01-01T00:00:00"));
//			statementDateRange.setFilterFromDate(dateFormat.parse("2015-01-01T00:00:00"));
//			statementDateRange.setFilterToDate(dateFormat.parse("2017-01-02T00:00:00"));
//			statementDateRange.setTransactionFromDateTime(dateFormat.parse("2014-01-01T00:00:00"));
//			statementDateRange.setTransactionToDateTime(dateFormat.parse("2017-01-01T00:00:00"));
//		} catch (ParseException e) {
//			e.printStackTrace();
//		}
//		delegate.fsCallFilter(statementDateRange);
//	}
//	
//	@Test
//	public void testFsCallFilter4(){
//		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
//		StatementDateRange statementDateRange = new StatementDateRange();
//		try {
//			statementDateRange.setRequestDateTime(dateFormat.parse("2015-01-01T00:00:00"));
//			statementDateRange.setConsentExpiryDateTime(dateFormat.parse("2015-01-02T00:00:00"));
//			statementDateRange.setFilterFromDate(dateFormat.parse("2015-01-01T00:00:00"));
//			statementDateRange.setFilterToDate(dateFormat.parse("2015-01-02T00:00:00"));
//			statementDateRange.setTransactionFromDateTime(dateFormat.parse("2015-01-01T02:00:00"));
//			statementDateRange.setTransactionToDateTime(dateFormat.parse("2015-01-02T01:00:00"));
//		} catch (ParseException e) {
//			e.printStackTrace();
//		}
//		delegate.fsCallFilter(statementDateRange);
//		
//		try {
//			statementDateRange.setFilterToDate(dateFormat.parse("2015-01-02T02:00:00"));
//			statementDateRange.setTransactionToDateTime(dateFormat.parse("2015-01-02T01:00:00"));
//		} catch (ParseException e) {
//			e.printStackTrace();
//		}
//		delegate.fsCallFilter(statementDateRange);
//	}
//}



package com.capgemini.psd2.foundationservice.account.statements.boi.adapter.test;



import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.capgemini.psd2.adapter.datetime.utility.StatementDateRange;
import com.capgemini.psd2.adapter.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.adapter.datetime.utility.TransactionDateRange;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.adapter.utility.AdapterUtility;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.client.AccountStatementsFoundationServiceClient;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.constants.AccountStatementsFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.delegate.AccountStatementsFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.raml.domain.StatementObject;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.raml.domain.Statementsresponse;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.transformer.AccountStatementsFoundationServiceTransformer;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountStatementsFoundationServiceDelegateRestCallTest {
	/** The account statements file foundation service adapter. */

	@InjectMocks
	private AccountStatementsFoundationServiceDelegate delegate;

	/** The account statements file foundation service client. */

	@InjectMocks
	private AccountStatementsFoundationServiceClient accountStatementsFileFoundationServiceClient;



	/** The rest client. */

	@Mock
	private RestClientSync restClient;

	/** The account statements file foundation service delegate. */

	@Mock
	private AccountStatementsFoundationServiceTransformer accountStatementsFileFSTransformer;

	/**

	 * Sets the up.

	 */
	
	@Mock
	private AccountStatementsFoundationServiceDelegate accountStatementsFileFoundationServiceDelegate;

	@Mock
	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;
	
	@Mock
	private AdapterUtility adapterUtility;
	
	@Before
	public void setUp(){
MockitoAnnotations.initMocks(this);
}

	/**

	 * Context loads.

	 */

	@Test
	public void contextLoads() {

	}



	/**

	 * Test account statements file FS.

	 */
	
	
	@Test
	public void testrestTransportForAccountStatementsFile(){
		RequestInfo reqInfo=new RequestInfo();
		HttpHeaders headers=new HttpHeaders();
		headers.add("X-API-TRANSACTION-ID", "correlationIdInReqHeader");
		headers.add("X-API-SOURCE-SYSTEM", "sourceSystemReqHeader");
		headers.add("X-API-SOURCE-USER", "sourceUserReqHeader");
		headers.add("X-API-CHANNEL-CODE","channelInReqHeader" );
		headers.add("X-MASKED-PAN","xMaskedPan" );
		reqInfo.setUrl("http://localhost:8081//psd2-abt-service/services/abt/accounts/nsc/number");
		accountStatementsFileFoundationServiceClient.restTransportForAccountStatementsFile(reqInfo, byte[].class, headers);
	}

	@Test
	public void testrestTransportForAccountStatementsStmt(){
		RequestInfo reqInfo=new RequestInfo();
		HttpHeaders headers=new HttpHeaders();
		Statementsresponse statements = new Statementsresponse();
		StatementObject stmt = new StatementObject();
		List<StatementObject> statementsList = new ArrayList<StatementObject>();
		statementsList.add(stmt);
		statements.setStatementsList(statementsList);
		stmt.setDocumentIdentificationNumber("abcd");
		
		headers.add("X-API-TRANSACTION-ID", "correlationIdInReqHeader");
		headers.add("X-API-SOURCE-SYSTEM", "sourceSystemReqHeader");
		headers.add("X-API-SOURCE-USER", "sourceUserReqHeader");
		headers.add("X-API-CHANNEL-CODE","channelInReqHeader" );
		headers.add("X-MASKED-PAN","xMaskedPan" );
		reqInfo.setUrl("http://localhost:8081//psd2-abt-service/services/abt/accounts/nsc/number");
		
	}
	
	@Test
	public void testrestTransportForAccountStatementsList(){
		RequestInfo reqInfo=new RequestInfo();
		HttpHeaders headers=new HttpHeaders();
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		headers.add("X-API-TRANSACTION-ID", "correlationIdInReqHeader");
		headers.add("X-API-SOURCE-SYSTEM", "sourceSystemReqHeader");
		headers.add("X-API-SOURCE-USER", "sourceUserReqHeader");
		headers.add("X-API-CHANNEL-CODE","channelInReqHeader" );
		headers.add("X-MASKED-PAN","xMaskedPan" );
		reqInfo.setUrl("http://localhost:8081//psd2-abt-service/services/abt/accounts/nsc/number");
		accountStatementsFileFoundationServiceClient.restTransportForAccountStatementsStmt(reqInfo, Statementsresponse.class, params, headers);
	}
	
	@Test
	public void testCreateTransactionDateRangeIfCases(){
		Map<String, String> params = new HashMap<>();
		params.put(AccountStatementsFoundationServiceConstants.CONSENT_EXPIRATION_DATETIME, "2015-01-01T05:22:11");
		params.put(AccountStatementsFoundationServiceConstants.FROM_DATE, "2015-01-01T00:00:00");
		params.put(AccountStatementsFoundationServiceConstants.TO_DATE, "2015-01-01T00:00:00");
		params.put(AccountStatementsFoundationServiceConstants.REQUESTED_FROM_CONSENT_DATETIME, "2015-01-01T00:00:00");
		params.put(AccountStatementsFoundationServiceConstants.REQUESTED_FROM_DATETIME, "2015-01-01T00:00:00");
		params.put(AccountStatementsFoundationServiceConstants.REQUESTED_TO_CONSENT_DATETIME, "2017-01-01T09:33:22");
		params.put(AccountStatementsFoundationServiceConstants.REQUESTED_TO_DATETIME, "2017-01-01T09:33:22");
		Mockito.when(timeZoneDateTimeAdapter.parseDateTimeFS(anyObject())).thenReturn(new Date());
		Mockito.when(timeZoneDateTimeAdapter.parseDateFS(anyObject())).thenReturn(new Date());
		ReflectionTestUtils.setField(delegate, "defaultStatementDateRange", 1);
		StatementDateRange res = delegate.createTransactionDateRange(params);
		assertNotNull(res);
	}	
	
	@Test
	public void testCreateTransactionDateRangeIfCases1(){
		Map<String, String> params = new HashMap<>();
		params.put(AccountStatementsFoundationServiceConstants.CONSENT_EXPIRATION_DATETIME, null);
		params.put(AccountStatementsFoundationServiceConstants.FROM_DATE, null);
		params.put(AccountStatementsFoundationServiceConstants.TO_DATE, null);
		params.put(AccountStatementsFoundationServiceConstants.REQUESTED_FROM_CONSENT_DATETIME, null);
		params.put(AccountStatementsFoundationServiceConstants.REQUESTED_FROM_DATETIME, null);
		params.put(AccountStatementsFoundationServiceConstants.REQUESTED_TO_CONSENT_DATETIME, null);
		params.put(AccountStatementsFoundationServiceConstants.REQUESTED_TO_DATETIME, null);
		Mockito.when(timeZoneDateTimeAdapter.parseDateTimeFS(anyObject())).thenReturn(new Date());
		Mockito.when(timeZoneDateTimeAdapter.parseDateFS(anyObject())).thenReturn(new Date());
		ReflectionTestUtils.setField(delegate, "defaultStatementDateRange", 1);
		StatementDateRange res = delegate.createTransactionDateRange(params);
		assertNotNull(res);
	}	
	
	@Test
	public void testFsCallFilterException(){
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		StatementDateRange statementDateRange = new StatementDateRange();
		try {
			statementDateRange.setRequestDateTime(dateFormat.parse("2018-01-01T00:00:00"));
			statementDateRange.setConsentExpiryDateTime(dateFormat.parse("2018-01-01T00:00:00"));
			statementDateRange.setTransactionFromDateTime(dateFormat.parse("2018-01-01T00:00:00"));
			statementDateRange.setFilterFromDate(dateFormat.parse("2018-01-01T00:00:00"));
			statementDateRange.setFilterToDate(dateFormat.parse("2018-01-01T00:00:00"));
			statementDateRange.setTransactionToDateTime(dateFormat.parse("2018-01-01T00:00:00"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		delegate.fsCallFilter(statementDateRange);
	}
	
	@Test
	public void testFsCallFilterException1(){
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		StatementDateRange statementDateRange = new StatementDateRange();
		try {
			statementDateRange.setRequestDateTime(dateFormat.parse("2018-01-01T00:00:00"));
			statementDateRange.setConsentExpiryDateTime(dateFormat.parse("2018-01-01T00:00:00"));
			statementDateRange.setFilterFromDate(dateFormat.parse("2018-01-01T00:00:00"));
			statementDateRange.setFilterToDate(dateFormat.parse("2018-01-01T00:00:00"));
			statementDateRange.setTransactionFromDateTime(dateFormat.parse("2018-01-01T00:00:00"));
			statementDateRange.setTransactionToDateTime(dateFormat.parse("2018-01-01T00:00:00"));

			
          } catch (ParseException e) {
			e.printStackTrace();
		}
		delegate.fsCallFilter(statementDateRange);
	}
	
	@Test
	public void testFsCallFilter1(){
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		StatementDateRange statementDateRange = new StatementDateRange();
		try {
			statementDateRange.setRequestDateTime(dateFormat.parse("2014-01-01T00:00:00"));
			statementDateRange.setConsentExpiryDateTime(dateFormat.parse("2015-01-01T00:00:00"));
			statementDateRange.setFilterFromDate(dateFormat.parse("2015-01-01T00:00:00"));
			statementDateRange.setFilterToDate(dateFormat.parse("2017-01-01T00:00:00"));
			statementDateRange.setTransactionFromDateTime(dateFormat.parse("2015-01-02T00:00:00"));
			statementDateRange.setTransactionToDateTime(dateFormat.parse("2017-01-01T00:00:00"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		StatementDateRange res = delegate.fsCallFilter(statementDateRange);
		assertEquals(true, res.isEmptyResponse());
	}
	
	@Test
	public void testFsCallFilter2(){
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		StatementDateRange statementDateRange = new StatementDateRange();
		try {
			statementDateRange.setRequestDateTime(dateFormat.parse("2014-01-01T00:00:00"));
			statementDateRange.setConsentExpiryDateTime(dateFormat.parse("2015-01-01T00:00:00"));
			statementDateRange.setFilterFromDate(dateFormat.parse("2015-01-01T00:00:00"));
			statementDateRange.setFilterToDate(dateFormat.parse("2017-01-01T00:00:00"));
			statementDateRange.setTransactionFromDateTime(dateFormat.parse("2014-01-01T00:00:00"));
			statementDateRange.setTransactionToDateTime(dateFormat.parse("2017-01-01T00:00:00"));
			statementDateRange.setFilterToDate(dateFormat.parse("2015-01-01T00:00:00"));
			statementDateRange.setFilterFromDate(dateFormat.parse("2015-01-01T00:00:00"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		delegate.fsCallFilter(statementDateRange);
	}
	
	@Test
	public void testFsCallFilter3(){
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		StatementDateRange statementDateRange = new StatementDateRange();
		try {
			statementDateRange.setRequestDateTime(dateFormat.parse("2014-01-01T00:00:00"));
			statementDateRange.setConsentExpiryDateTime(dateFormat.parse("2015-01-01T00:00:00"));
			statementDateRange.setFilterFromDate(dateFormat.parse("2015-01-01T00:00:00"));
			statementDateRange.setFilterToDate(dateFormat.parse("2017-01-02T00:00:00"));
			statementDateRange.setTransactionFromDateTime(dateFormat.parse("2014-01-01T00:00:00"));
			statementDateRange.setTransactionToDateTime(dateFormat.parse("2017-01-01T00:00:00"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		delegate.fsCallFilter(statementDateRange);
	}
	
	@Test
	public void testFsCallFilter4(){
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		StatementDateRange statementDateRange = new StatementDateRange();
		try {
			statementDateRange.setRequestDateTime(dateFormat.parse("2015-01-01T00:00:00"));
			statementDateRange.setConsentExpiryDateTime(dateFormat.parse("2015-01-02T00:00:00"));
			statementDateRange.setFilterFromDate(dateFormat.parse("2015-01-01T00:00:00"));
			statementDateRange.setFilterToDate(dateFormat.parse("2015-01-02T00:00:00"));
			statementDateRange.setTransactionFromDateTime(dateFormat.parse("2015-01-01T02:00:00"));
			statementDateRange.setTransactionToDateTime(dateFormat.parse("2015-01-02T01:00:00"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		delegate.fsCallFilter(statementDateRange);
		
		try {
			statementDateRange.setFilterToDate(dateFormat.parse("2015-01-02T02:00:00"));
			statementDateRange.setTransactionToDateTime(dateFormat.parse("2015-01-02T01:00:00"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		delegate.fsCallFilter(statementDateRange);
	}
}