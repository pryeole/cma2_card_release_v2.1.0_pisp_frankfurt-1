package com.capgemini.psd2.exceptions;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

import com.capgemini.psd2.pisp.domain.OBErrorResponse1;

public class ExceptionAttributes {

	private OBErrorResponse1 obErrorResponse1;
	private ErrorInfo errorInfo;
	private HttpHeaders headers;
	private HttpStatus status;
	
	public ErrorInfo getErrorInfo() {
		return errorInfo;
	}
	public void setErrorInfo(ErrorInfo errorInfo) {
		this.errorInfo = errorInfo;
	}
	public HttpHeaders getHeaders() {
		return headers;
	}
	public void setHeaders(HttpHeaders headers) {
		this.headers = headers;
	}
	public HttpStatus getStatus() {
		return status;
	}
	public void setStatus(HttpStatus status) {
		this.status = status;
	}

	public OBErrorResponse1 getObErrorResponse1() {
		return obErrorResponse1;
	}
	public void setObErrorResponse1(OBErrorResponse1 obErrorResponse1) {
		this.obErrorResponse1 = obErrorResponse1;
	}

}
