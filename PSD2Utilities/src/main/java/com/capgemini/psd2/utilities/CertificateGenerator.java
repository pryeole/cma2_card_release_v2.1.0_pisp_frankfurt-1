package com.capgemini.psd2.utilities;

import java.security.cert.X509Certificate;

public interface CertificateGenerator {

	public X509Certificate getSigningCertificate(String kid,String clientId);
}
