package com.capgemini.psd2.account.request.mongo.db.adapter.transformer.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.domain.OBExternalPermissions1Code;
import com.capgemini.psd2.aisp.domain.OBExternalRequestStatus1Code;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data;
import com.capgemini.psd2.aisp.domain.OBReadDataResponse1;
import com.capgemini.psd2.aisp.domain.OBReadDataResponse1.PermissionsEnum;
import com.capgemini.psd2.aisp.transformer.AccountRequestTransformer;

@Component
public class AccountRequestTransformerImpl implements AccountRequestTransformer{

	@Override
	public <T> OBReadConsentResponse1Data transformAccountInformation(T source, OBReadConsentResponse1Data destination,
			Map<String, String> params) {
		OBReadDataResponse1 accCma2Response = (OBReadDataResponse1) source;
		destination.setCmaVersion(accCma2Response.getCmaVersion());
		destination.setConsentId(accCma2Response.getAccountRequestId());
		destination.setCreationDateTime(accCma2Response.getCreationDateTime());
		destination.setExpirationDateTime(accCma2Response.getExpirationDateTime());
		destination.setStatusUpdateDateTime(accCma2Response.getStatusUpdateDateTime());
		destination.setTransactionFromDateTime(accCma2Response.getTransactionFromDateTime());
		destination.setTransactionToDateTime(accCma2Response.getTransactionToDateTime());
		List<OBExternalPermissions1Code> persmissions= new ArrayList<>();
		for (PermissionsEnum permission : accCma2Response.getPermissions()){
			persmissions.add(OBExternalPermissions1Code.fromValue(permission.toString()));
		}
		destination.setPermissions(persmissions);
		destination.setStatus(OBExternalRequestStatus1Code.fromValue(accCma2Response.getStatus().toString()));
		return destination;
	}
}
