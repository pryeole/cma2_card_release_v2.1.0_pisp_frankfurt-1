package com.capgemini.psd2.customer.account.profile.mock.foundationservice.exception.handler;

public class MissingAuthenticationHeaderException extends Exception{

	public MissingAuthenticationHeaderException(String m){
		super(m);
	}
	
}
