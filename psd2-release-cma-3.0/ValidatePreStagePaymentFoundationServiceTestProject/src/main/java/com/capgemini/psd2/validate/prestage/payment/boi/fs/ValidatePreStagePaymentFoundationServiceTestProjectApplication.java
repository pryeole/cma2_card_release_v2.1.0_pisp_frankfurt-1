package com.capgemini.psd2.validate.prestage.payment.boi.fs;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.foundationservice.validate.prestage.payment.boi.adapter.ValidatePreStagePaymentFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.validate.prestage.payment.boi.adapter.constants.ValidatePreStagePaymentFoundationServiceConstants;
import com.capgemini.psd2.pisp.domain.CreditorAccount;
import com.capgemini.psd2.pisp.domain.CreditorAgent;
import com.capgemini.psd2.pisp.domain.DebtorAccount;
import com.capgemini.psd2.pisp.domain.DebtorAgent;
import com.capgemini.psd2.pisp.domain.DebtorAgent.SchemeNameEnum;
import com.capgemini.psd2.pisp.domain.PaymentSetup;
import com.capgemini.psd2.pisp.domain.PaymentSetupInitiation;
import com.capgemini.psd2.pisp.domain.PaymentSetupInitiationInstructedAmount;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.PaymentSetupValidationResponse;
import com.capgemini.psd2.pisp.domain.RemittanceInformation;
import com.capgemini.psd2.pisp.domain.Risk;
import com.capgemini.psd2.pisp.domain.Risk.PaymentContextCodeEnum;
import com.capgemini.psd2.pisp.domain.RiskDeliveryAddress;

@SpringBootApplication
@ComponentScan("com.capgemini.psd2")
public class ValidatePreStagePaymentFoundationServiceTestProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(ValidatePreStagePaymentFoundationServiceTestProjectApplication.class, args);
	}
}

/*@RestController
@ResponseBody	
class TestValidatePreStagePaymentFSController{

@Autowired
private ValidatePreStagePaymentFoundationServiceAdapter adapter;
	
@RequestMapping("/testValidatePreStagePayment")
public PaymentSetupValidationResponse getResponse(){
	
	PaymentSetupPOSTRequest paymentSetupPOSTRequest = new PaymentSetupPOSTRequest();
	
    PaymentSetup data = new PaymentSetup();
    
    PaymentSetupInitiation initiation = new PaymentSetupInitiation();
    initiation.setInstructionIdentification("ANSM023");
    //initiation.setEndToEndIdentification("FRESCO.21302.GFX.37");
    initiation.setEndToEndIdentification("FS_PMPSV_008");
    
    DebtorAccount debtorAccount = new DebtorAccount();
    debtorAccount.setIdentification("01234567");
    debtorAccount.setName("Andrea Smith");
    debtorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.BBAN);
    
    DebtorAgent debtorAgent = new DebtorAgent();
    debtorAgent.setIdentification("SC112800");
    debtorAgent.setSchemeName(SchemeNameEnum.UKSORTCODE);         
    
    CreditorAccount creditorAccount = new CreditorAccount();
    creditorAccount.setIdentification("21325698");
    creditorAccount.setName("Bob Clements");
    creditorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.CreditorAccount.SchemeNameEnum.BBAN);
    
    CreditorAgent creditorAgent = new CreditorAgent();
    creditorAgent.setIdentification("080800");// In FS side it is expected to be Integer 
    creditorAgent.setSchemeName(com.capgemini.psd2.pisp.domain.CreditorAgent.SchemeNameEnum.UKSORTCODE);
    
    PaymentSetupInitiationInstructedAmount instructedAmount = new PaymentSetupInitiationInstructedAmount();
    instructedAmount.setAmount("20.00");
    instructedAmount.setCurrency("GBP");
    
    RemittanceInformation remittanceInformation = new RemittanceInformation();
    remittanceInformation.setReference("FRESCO-037");
    remittanceInformation.setUnstructured("Internal ops code 5120103");
    
    RiskDeliveryAddress riskDeliveryAddress = new RiskDeliveryAddress();
    
    
    initiation.setCreditorAccount(creditorAccount);
    initiation.setCreditorAgent(creditorAgent);
    initiation.setDebtorAccount(debtorAccount);
    initiation.setDebtorAgent(debtorAgent);
    initiation.setInstructedAmount(instructedAmount);
    initiation.setRemittanceInformation(remittanceInformation);
    

    Risk risk = new Risk();
    risk.setPaymentContextCode(PaymentContextCodeEnum.PERSONTOPERSON);
    risk.setDeliveryAddress(riskDeliveryAddress);
    
    data.setInitiation(initiation);
    paymentSetupPOSTRequest.setData(data);
    paymentSetupPOSTRequest.setRisk(risk);

    Map<String, String> params = new HashMap<String, String>();

    params.put(ValidatePreStagePaymentFoundationServiceConstants.USER_ID, "testUser");
    params.put(ValidatePreStagePaymentFoundationServiceConstants.CHANNEL_ID, "BOL");
    params.put(ValidatePreStagePaymentFoundationServiceConstants.CORRELATION_ID, "testCorrelation");
    params.put(ValidatePreStagePaymentFoundationServiceConstants.PLATFORM_ID, "testPlatform");
    //params.put(InsertPreStagePaymentFoundationServiceConstants.VALIDATION_STATUS, "Pending");

    PaymentSetupValidationResponse paymentSetupPreValidationResponse = adapter.validatePreStagePaymentSetup(paymentSetupPOSTRequest, params);

    return paymentSetupPreValidationResponse;


}

}
*/