package com.capgemini.psd2.foundationservice.account.statements.boi.adapter.transformer;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.aisp.domain.Meta;
import com.capgemini.psd2.aisp.domain.OBExternalStatementType1Code;
import com.capgemini.psd2.aisp.domain.OBReadStatement1;
import com.capgemini.psd2.aisp.domain.OBReadStatement1Data;
import com.capgemini.psd2.aisp.domain.OBStatement1;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountStatementsResponse;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.constants.AccountStatementsFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.domain.Statement;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.domain.StatementsResponse;

@Component
public class AccountStatementsFoundationServiceTransformer {

	@Autowired
	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;

	public <T> PlatformAccountStatementsResponse transformAccountStatementRes(T inputBalanceObj,
			Map<String, String> params) {
		PlatformAccountStatementsResponse platformAccountStatementsResponse = new PlatformAccountStatementsResponse();
		OBReadStatement1 oBReadStatement1 = new OBReadStatement1();
		if (params.get(AccountStatementsFoundationServiceConstants.ACCOUNT_TYPE)
				.equals(AccountStatementsFoundationServiceConstants.CREDIT_CARD)) {

			StatementsResponse statementsResponse = (StatementsResponse) inputBalanceObj;
			List<OBStatement1> statementList = new ArrayList<>();

			for (Statement statement : statementsResponse.getStatementsList()) {
				
				String creationDateStr = timeZoneDateTimeAdapter.parseDateCMA(statement.getDocumentIssuanceDate());
				
				OBStatement1 obStatement1 = new OBStatement1();
				obStatement1.setAccountId(params.get(AccountStatementsFoundationServiceConstants.ACCOUNT_ID));
				obStatement1.setStatementId(statement.getDocumentIdentificationNumber());
				if (statement.getStatementReasonDescription() != null) {

					String type = statement.getStatementReasonDescription();
					switch (type) {
					case AccountStatementsFoundationServiceConstants.ANNUAL_SUMMARY_STMT:
						obStatement1.setType(OBExternalStatementType1Code.ANNUAL);
						Date date = yearMinus(statement.getDocumentIssuanceDate());
						obStatement1.setStartDateTime(timeZoneDateTimeAdapter.parseDateCMA(date));
						break;
					case AccountStatementsFoundationServiceConstants.REGULAR_MON_STMT:
						obStatement1.setType(OBExternalStatementType1Code.REGULARPERIODIC);
						Date date1 = monthMinus(statement.getDocumentIssuanceDate());
						obStatement1.setStartDateTime(timeZoneDateTimeAdapter.parseDateCMA(date1));
						break;
					case AccountStatementsFoundationServiceConstants.COMM_SUMMARY_STMT:
						obStatement1.setType(OBExternalStatementType1Code.REGULARPERIODIC);
						Date date2 = yearMinus(statement.getDocumentIssuanceDate());
						obStatement1.setStartDateTime(timeZoneDateTimeAdapter.parseDateCMA(date2));
						break;
					case AccountStatementsFoundationServiceConstants.COMM_MONTHLY_STMT:
						obStatement1.setType(OBExternalStatementType1Code.REGULARPERIODIC);
						Date date3 = monthMinus(statement.getDocumentIssuanceDate());
						obStatement1.setStartDateTime(timeZoneDateTimeAdapter.parseDateCMA(date3));
					default:
						obStatement1.setType(OBExternalStatementType1Code.REGULARPERIODIC);
						obStatement1.setStartDateTime(creationDateStr);
					}
				}

				obStatement1.setEndDateTime(creationDateStr);
				obStatement1.setCreationDateTime(creationDateStr);

				statementList.add(obStatement1);
			}
			OBReadStatement1Data obReadStatement1Data = new OBReadStatement1Data();

			obReadStatement1Data.setStatement(statementList);
			oBReadStatement1 = new OBReadStatement1();
			oBReadStatement1.setData(obReadStatement1Data);

			if (null == oBReadStatement1.getMeta()) {
				oBReadStatement1.setMeta(new Meta().totalPages(1));
			}

			platformAccountStatementsResponse.setoBReadStatement1(oBReadStatement1);
		}

		else {
			StatementsResponse statementsResponse = (StatementsResponse) inputBalanceObj;
			List<OBStatement1> statementList = new ArrayList<>();

			for (Statement statement : statementsResponse.getStatementsList()) {
				
				String creationDateStr = timeZoneDateTimeAdapter.parseDateCMA(statement.getDocumentIssuanceDate());

				OBStatement1 obStatement1 = new OBStatement1();
				obStatement1.setAccountId(params.get(AccountStatementsFoundationServiceConstants.ACCOUNT_ID));
				obStatement1.setStatementId(statement.getDocumentIdentificationNumber());

				if (statement.getStatementReasonDescription() != null) {
					String type = statement.getStatementReasonDescription();
					switch (type) {
					case AccountStatementsFoundationServiceConstants.ANNUAL_SUMMARY_STMT:
						obStatement1.setType(OBExternalStatementType1Code.ANNUAL);
						Date date = yearMinus(statement.getDocumentIssuanceDate());
						obStatement1.setStartDateTime(timeZoneDateTimeAdapter.parseDateCMA(date));
						break;
					case AccountStatementsFoundationServiceConstants.REGULAR_MON_STMT:
						obStatement1.setType(OBExternalStatementType1Code.REGULARPERIODIC);
						Date date1 = monthMinus(statement.getDocumentIssuanceDate());
						obStatement1.setStartDateTime(timeZoneDateTimeAdapter.parseDateCMA(date1));
						break;
					case AccountStatementsFoundationServiceConstants.COMM_SUMMARY_STMT:
						obStatement1.setType(OBExternalStatementType1Code.REGULARPERIODIC);
						Date date2 = yearMinus(statement.getDocumentIssuanceDate());
						obStatement1.setStartDateTime(timeZoneDateTimeAdapter.parseDateCMA(date2));
						break;
					case AccountStatementsFoundationServiceConstants.COMM_MONTHLY_STMT:
						obStatement1.setType(OBExternalStatementType1Code.REGULARPERIODIC);
						Date date3 = monthMinus(statement.getDocumentIssuanceDate());
						obStatement1.setStartDateTime(timeZoneDateTimeAdapter.parseDateCMA(date3));
					default:
						obStatement1.setType(OBExternalStatementType1Code.REGULARPERIODIC);
						obStatement1.setStartDateTime(creationDateStr);
					}
				}
				obStatement1.setEndDateTime(creationDateStr);
				obStatement1.setCreationDateTime(creationDateStr);

				statementList.add(obStatement1);
			}
			OBReadStatement1Data obReadStatement1Data = new OBReadStatement1Data();
			obReadStatement1Data.setStatement(statementList);
			oBReadStatement1 = new OBReadStatement1();
			oBReadStatement1.setData(obReadStatement1Data);

			if (null == oBReadStatement1.getMeta()) {
				oBReadStatement1.setMeta(new Meta().totalPages(1));
			}

			platformAccountStatementsResponse.setoBReadStatement1(oBReadStatement1);

		}
		return platformAccountStatementsResponse;

	}

	private Date monthMinus(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH, -1);
		return cal.getTime();
	}

	private Date yearMinus(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.YEAR, -1);
		return cal.getTime();
	}
}
