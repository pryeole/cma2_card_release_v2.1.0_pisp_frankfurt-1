package com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.raml.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Defines the geographic scope within which payments for this Payment Beneficiary can be made
 */
public enum PaymentBeneficiaryLocaleTypeCode {
  
  DOMESTIC("Domestic"),
  
  SEPA("SEPA"),
  
  INTERNATIONAL("International");

  private String value;

  PaymentBeneficiaryLocaleTypeCode(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static PaymentBeneficiaryLocaleTypeCode fromValue(String text) {
    for (PaymentBeneficiaryLocaleTypeCode b : PaymentBeneficiaryLocaleTypeCode.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}

