package com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.domain;

import java.util.Objects;
import io.swagger.annotations.ApiModel;
import com.fasterxml.jackson.annotation.JsonValue;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * Defines the geographic scope within which payments for this Payment Beneficiary can be made
 */
public enum PaymentBeneficiaryLocaleTypeCode {
  
  DOMESTIC("Domestic"),
  
  SEPA("SEPA"),
  
  INTERNATIONAL("International");

  private String value;

  PaymentBeneficiaryLocaleTypeCode(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static PaymentBeneficiaryLocaleTypeCode fromValue(String text) {
    for (PaymentBeneficiaryLocaleTypeCode b : PaymentBeneficiaryLocaleTypeCode.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}

