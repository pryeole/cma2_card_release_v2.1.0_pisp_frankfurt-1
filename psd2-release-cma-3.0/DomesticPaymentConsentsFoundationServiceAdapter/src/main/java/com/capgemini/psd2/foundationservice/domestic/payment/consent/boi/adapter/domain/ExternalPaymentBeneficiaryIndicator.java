package com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.domain;

import java.util.Objects;
import io.swagger.annotations.ApiModel;
import com.fasterxml.jackson.annotation.JsonValue;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * Indicates whether (Y) or not (N) the Payee Account associated with the Payment Beneficiary is located outside of the Bank Group
 */
public enum ExternalPaymentBeneficiaryIndicator {
  
  Y("Y"),
  
  N("N");

  private String value;

  ExternalPaymentBeneficiaryIndicator(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static ExternalPaymentBeneficiaryIndicator fromValue(String text) {
    for (ExternalPaymentBeneficiaryIndicator b : ExternalPaymentBeneficiaryIndicator.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}

