package com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain;

import java.time.OffsetDateTime;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * AssessmentDecision
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-01-16T16:32:31.691+05:30")

public class AssessmentDecision   {
  @JsonProperty("assessmentDecisionIndicator")
  private Boolean assessmentDecisionIndicator = null;

  @JsonProperty("assessmentDecisionScore")
  private Integer assessmentDecisionScore = null;

  @JsonProperty("assessmentDecisionReasonCode")
  private String assessmentDecisionReasonCode = null;

  @JsonProperty("assessmentDecisionText")
  private String assessmentDecisionText = null;

  @JsonProperty("assessmentResultCreationDate")
  private OffsetDateTime assessmentResultCreationDate = null;

  public AssessmentDecision assessmentDecisionIndicator(Boolean assessmentDecisionIndicator) {
    this.assessmentDecisionIndicator = assessmentDecisionIndicator;
    return this;
  }

  /**
   * The outcome of the assessment decision. Positive is true or Negative is false
   * @return assessmentDecisionIndicator
  **/
  @ApiModelProperty(value = "The outcome of the assessment decision. Positive is true or Negative is false")


  public Boolean isAssessmentDecisionIndicator() {
    return assessmentDecisionIndicator;
  }

  public void setAssessmentDecisionIndicator(Boolean assessmentDecisionIndicator) {
    this.assessmentDecisionIndicator = assessmentDecisionIndicator;
  }

  public AssessmentDecision assessmentDecisionScore(Integer assessmentDecisionScore) {
    this.assessmentDecisionScore = assessmentDecisionScore;
    return this;
  }

  /**
   * Get assessmentDecisionScore
   * @return assessmentDecisionScore
  **/
  @ApiModelProperty(value = "")


  public Integer getAssessmentDecisionScore() {
    return assessmentDecisionScore;
  }

  public void setAssessmentDecisionScore(Integer assessmentDecisionScore) {
    this.assessmentDecisionScore = assessmentDecisionScore;
  }

  public AssessmentDecision assessmentDecisionReasonCode(String assessmentDecisionReasonCode) {
    this.assessmentDecisionReasonCode = assessmentDecisionReasonCode;
    return this;
  }

  /**
   * Get assessmentDecisionReasonCode
   * @return assessmentDecisionReasonCode
  **/
  @ApiModelProperty(value = "")


  public String getAssessmentDecisionReasonCode() {
    return assessmentDecisionReasonCode;
  }

  public void setAssessmentDecisionReasonCode(String assessmentDecisionReasonCode) {
    this.assessmentDecisionReasonCode = assessmentDecisionReasonCode;
  }

  public AssessmentDecision assessmentDecisionText(String assessmentDecisionText) {
    this.assessmentDecisionText = assessmentDecisionText;
    return this;
  }

  /**
   * Get assessmentDecisionText
   * @return assessmentDecisionText
  **/
  @ApiModelProperty(value = "")


  public String getAssessmentDecisionText() {
    return assessmentDecisionText;
  }

  public void setAssessmentDecisionText(String assessmentDecisionText) {
    this.assessmentDecisionText = assessmentDecisionText;
  }

  public AssessmentDecision assessmentResultCreationDate(OffsetDateTime assessmentResultCreationDate) {
    this.assessmentResultCreationDate = assessmentResultCreationDate;
    return this;
  }

  /**
   * The date-time when the assessment result was produced
   * @return assessmentResultCreationDate
  **/
  @ApiModelProperty(required = true, value = "The date-time when the assessment result was produced")
  @NotNull

  @Valid

  public OffsetDateTime getAssessmentResultCreationDate() {
    return assessmentResultCreationDate;
  }

  public void setAssessmentResultCreationDate(OffsetDateTime assessmentResultCreationDate) {
    this.assessmentResultCreationDate = assessmentResultCreationDate;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AssessmentDecision assessmentDecision = (AssessmentDecision) o;
    return Objects.equals(this.assessmentDecisionIndicator, assessmentDecision.assessmentDecisionIndicator) &&
        Objects.equals(this.assessmentDecisionScore, assessmentDecision.assessmentDecisionScore) &&
        Objects.equals(this.assessmentDecisionReasonCode, assessmentDecision.assessmentDecisionReasonCode) &&
        Objects.equals(this.assessmentDecisionText, assessmentDecision.assessmentDecisionText) &&
        Objects.equals(this.assessmentResultCreationDate, assessmentDecision.assessmentResultCreationDate);
  }

  @Override
  public int hashCode() {
    return Objects.hash(assessmentDecisionIndicator, assessmentDecisionScore, assessmentDecisionReasonCode, assessmentDecisionText, assessmentResultCreationDate);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AssessmentDecision {\n");
    
    sb.append("    assessmentDecisionIndicator: ").append(toIndentedString(assessmentDecisionIndicator)).append("\n");
    sb.append("    assessmentDecisionScore: ").append(toIndentedString(assessmentDecisionScore)).append("\n");
    sb.append("    assessmentDecisionReasonCode: ").append(toIndentedString(assessmentDecisionReasonCode)).append("\n");
    sb.append("    assessmentDecisionText: ").append(toIndentedString(assessmentDecisionText)).append("\n");
    sb.append("    assessmentResultCreationDate: ").append(toIndentedString(assessmentResultCreationDate)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

