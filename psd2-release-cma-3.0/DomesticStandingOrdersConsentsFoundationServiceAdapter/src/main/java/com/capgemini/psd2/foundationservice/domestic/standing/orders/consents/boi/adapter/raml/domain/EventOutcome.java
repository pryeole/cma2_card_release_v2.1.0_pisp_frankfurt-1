package com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * The outcome of the user event whether it was a success or failure
 */
public enum EventOutcome {
  
  SUCCESS("Success"),
  
  FAILED("Failed");

  private String value;

  EventOutcome(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static EventOutcome fromValue(String text) {
    for (EventOutcome b : EventOutcome.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}

