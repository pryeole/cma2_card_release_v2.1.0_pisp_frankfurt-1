/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.constants.InsertPreStagePaymentFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.domain.PaymentInstruction;
import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.domain.ValidationPassed;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class InsertPreStagePaymentFoundationClientImpl implements InsertPreStagePaymentFoundationClient{
	
	@Autowired
	@Qualifier("restClientFoundation")
	private RestClientSync restClient;

	@Override
	public ValidationPassed restTransportForInsertPreStagePayment(RequestInfo reqInfo, PaymentInstruction paymentInstruction, Class <ValidationPassed> responseType, HttpHeaders headers) {
		ValidationPassed validationPassed = null;
		try {
			validationPassed = getValidateInsertPreStagePayment(reqInfo, paymentInstruction, responseType, headers);
		} catch (AdapterException e) {
			AdapterException ae = (AdapterException) e;
			/*The below block would retry when Foundation service is down(i.e CONNECTION_REFUSED_CONNECT)*/
			if ("503".equals(ae.getErrorInfo().getActualErrorCode()) && containsMessage(ae.getErrorInfo().getActualDetailErrorMessage(),
						InsertPreStagePaymentFoundationServiceConstants.CONNECTION_REFUSED_CONNECT)) {
					throw new RuntimeException("Retry_Error_Code");
			}else{
				throw e;
			}
		}catch (Exception e) {
			throw e;

		}
		return validationPassed;
	}
	
	@Override
	public ValidationPassed restTransportForUpdatePreStagePayment(RequestInfo reqInfo, PaymentInstruction paymentInstruction, Class <ValidationPassed> responseType, HttpHeaders headers) {
		ValidationPassed validationPassed = null;
		try {
			validationPassed = getValidateUpdatePreStagePayment(reqInfo, paymentInstruction, responseType, headers);
		} catch (AdapterException e) {
			AdapterException ae = (AdapterException) e;
			/*The below block would retry when Foundation service is down(i.e CONNECTION_REFUSED_CONNECT)*/
			if("503".equals(ae.getErrorInfo().getActualErrorCode()) && containsMessage(ae.getErrorInfo().getActualDetailErrorMessage(),
						InsertPreStagePaymentFoundationServiceConstants.CONNECTION_REFUSED_CONNECT)) {
					throw new RuntimeException("Retry_Error_Code");
			}else{
				throw e;
			}
		}catch (Exception e) {
			throw e;
		}
		return validationPassed;
	}
	
	@Override
	public PaymentInstruction restTransportForPaymentRetrieval(RequestInfo reqInfo, Class <PaymentInstruction> responseType, HttpHeaders headers) {

		PaymentInstruction paymentInstruction = null;
		try {
			paymentInstruction = getValidateRetrievePreStagePayment(reqInfo, responseType, headers);
		} catch (AdapterException e) {
			AdapterException ae = (AdapterException) e;
			/*The below block would retry when Foundation service is down(i.e CONNECTION_REFUSED_CONNECT) and for following error codes from Foundation Service(FS_PMR_005, FS_PMR_004, FS_PMR_002)*/
			if(("503".equals(ae.getErrorInfo().getActualErrorCode()) && (containsMessage(ae.getErrorInfo().getActualDetailErrorMessage(),
					InsertPreStagePaymentFoundationServiceConstants.CONNECTION_REFUSED_CONNECT) || containsMessage(ae.getErrorInfo().getActualDetailErrorMessage(),
							InsertPreStagePaymentFoundationServiceConstants.PLEASE_TRY_AGAIN_LATER))) 
					||("544".equals(ae.getErrorInfo().getActualErrorCode()) && (containsMessage(ae.getErrorInfo().getActualDetailErrorMessage(),
							InsertPreStagePaymentFoundationServiceConstants.CONNECTION_REFUSED_CONNECT) || containsMessage(ae.getErrorInfo().getActualDetailErrorMessage(),
									InsertPreStagePaymentFoundationServiceConstants.PLEASE_TRY_AGAIN_LATER)))
					|| ("515".equals(ae.getErrorInfo().getActualErrorCode()) && containsMessage(
						ae.getErrorInfo().getActualDetailErrorMessage(), InsertPreStagePaymentFoundationServiceConstants.GENERAL_ERROR)) 
					|| ("500".equals(ae.getErrorInfo().getActualErrorCode()) 
								&& containsMessage(ae.getErrorInfo().getActualDetailErrorMessage(), InsertPreStagePaymentFoundationServiceConstants.YOU_ARE_NOT_AUTHORISED))
					|| ("541".equals(ae.getErrorInfo().getActualErrorCode()) 
							&& containsMessage(ae.getErrorInfo().getActualDetailErrorMessage(), InsertPreStagePaymentFoundationServiceConstants.YOU_ARE_NOT_AUTHORISED))) {
					throw new RuntimeException("Retry_Error_Code");
			}else{
				throw e;
			}
		} catch (Exception e) {
			throw e;

		}
		return paymentInstruction;
	}
	
	private PaymentInstruction getValidateRetrievePreStagePayment(RequestInfo reqInfo, Class <PaymentInstruction> responseType, HttpHeaders headers){
		return restClient.callForGet(reqInfo, responseType, headers);
	
	}
	
	private ValidationPassed getValidateInsertPreStagePayment(RequestInfo reqInfo, PaymentInstruction paymentInstruction, Class <ValidationPassed> responseType, HttpHeaders headers){
		return restClient.callForPost(reqInfo, paymentInstruction, responseType, headers);
	
	}
	
	private ValidationPassed getValidateUpdatePreStagePayment(RequestInfo reqInfo, PaymentInstruction paymentInstruction, Class <ValidationPassed> responseType, HttpHeaders headers){
		return restClient.callForPost(reqInfo, paymentInstruction, responseType, headers);
	
	}
	
	private boolean containsMessage(String message, String key){

		if(!NullCheckUtils.isNullOrEmpty(message)){		
				return message.toLowerCase().contains(key.toLowerCase());
		}
		return false;
	}
}
