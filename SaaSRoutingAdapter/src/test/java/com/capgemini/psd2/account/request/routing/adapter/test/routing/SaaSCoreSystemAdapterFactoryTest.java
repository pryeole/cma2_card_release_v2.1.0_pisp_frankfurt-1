package com.capgemini.psd2.account.request.routing.adapter.test.routing;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.context.ApplicationContext;

import com.capgemini.psd2.account.request.routing.adapter.impl.SaaSRoutingAdapter;
import com.capgemini.psd2.account.request.routing.adapter.routing.SaaSCoreSystemAdapterFactory;
import com.capgemini.psd2.authentication.adapter.AuthenticationAdapter;

public class SaaSCoreSystemAdapterFactoryTest {

	@Mock
	private ApplicationContext applicationContext;

	@InjectMocks
	private SaaSCoreSystemAdapterFactory factory;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testGetAdapterInstance() {
		AuthenticationAdapter authenticationAdapter = new SaaSRoutingAdapter();
		when(applicationContext.getBean(anyString())).thenReturn(authenticationAdapter);
		AuthenticationAdapter adapter = factory.getAdapterInstance("test");
		assertNotNull(adapter);
	}

	@Test
	public void testSetApplicationContext() {
		factory.setApplicationContext(new ApplicationContextMock());
	}
}
