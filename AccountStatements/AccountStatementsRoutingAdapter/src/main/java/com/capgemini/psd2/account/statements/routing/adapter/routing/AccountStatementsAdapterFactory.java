package com.capgemini.psd2.account.statements.routing.adapter.routing;


import com.capgemini.psd2.aisp.adapter.AccountStatementsAdapter;

@FunctionalInterface
public interface AccountStatementsAdapterFactory 
{
	/**
	 * Gets the adapter instance.
	 *
	 * @param coreSystemName the core system name
	 * @return the adapter instance
	 */
	public AccountStatementsAdapter getAdapterInstance(String adapterName);
}
