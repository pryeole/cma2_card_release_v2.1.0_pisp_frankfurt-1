package com.capgemini.psd2.integration.service.impl.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyObject;

import javax.naming.directory.BasicAttributes;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.integration.adapter.TPPInformationAdaptor;
import com.capgemini.psd2.integration.dtos.TPPInformationDTO;
import com.capgemini.psd2.integration.service.impl.TPPInformationServiceImpl;

 
@RunWith(SpringJUnit4ClassRunner.class)
public class TPPInformationServiceImplTest

{
@Mock
private TPPInformationAdaptor tppInformationAdaptor;

@InjectMocks
private TPPInformationServiceImpl service;

 @Before
 public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
   }

 @Test
 public void findTPPInformationTest()
 {
	 TPPInformationDTO tppInformationDTO = new TPPInformationDTO();
	 BasicAttributes tppInformationObj = new BasicAttributes();
	 BasicAttributes attr = new BasicAttributes(); 
	 tppInformationObj.put("x-roles", attr );
	 Mockito.when(tppInformationAdaptor.fetchTPPInformation(anyObject())).thenReturn(tppInformationObj);	 
	 service.findTPPInformation("123");	 
	 assertNotNull(tppInformationDTO);
 }  
 @Test
 public void findTPPInformationNullTest()
 {
	 TPPInformationDTO tppInformationDTO = new TPPInformationDTO();
	 Object tppInformationObj = new Object();
	
	 Mockito.when(tppInformationAdaptor.fetchTPPInformation(anyObject())).thenReturn(tppInformationObj);	 
	 service.findTPPInformation("123");	 
	 assertNotNull(tppInformationDTO);
 }  
  }