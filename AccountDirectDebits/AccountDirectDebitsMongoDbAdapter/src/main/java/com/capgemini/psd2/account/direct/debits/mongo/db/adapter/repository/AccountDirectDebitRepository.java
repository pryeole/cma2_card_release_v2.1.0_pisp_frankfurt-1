package com.capgemini.psd2.account.direct.debits.mongo.db.adapter.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.aisp.mongo.db.adapter.domain.AccountDirectDebitCMA2;

public interface AccountDirectDebitRepository extends MongoRepository<AccountDirectDebitCMA2, String> {
	
	/**
	 * Find by account id.
	 *
	 * @param accountId the account id
	 * @return the balances GET response data
	 */
	public AccountDirectDebitCMA2 findByAccountNumberAndAccountNSC (String accountNumber, String accountNSC);

}
