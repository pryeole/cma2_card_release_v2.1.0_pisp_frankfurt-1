/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.direct.debits.routing.adapter.routing;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.adapter.AccountDirectDebitsAdapter;

/**
 * A factory for creating AccountDirectDebitsCoreSystemAdapter objects.
 */
@Component
public class AccountDirectDebitsCoreSystemAdapterFactory implements ApplicationContextAware, AccountDirectDebitsAdapterFactory {

	/** The application context. */
	private ApplicationContext applicationContext;
	
	/* (non-Javadoc)
	 * @see com.capgemini.psd2.account.direct.debits.routing.adapter.routing.AccountDirectDebitsAdapterFactory#getAdapterInstance(java.lang.String)
	 */
	@Override
	public AccountDirectDebitsAdapter getAdapterInstance(String adapterName){
		return (AccountDirectDebitsAdapter) applicationContext.getBean(adapterName);		
	}
		
	/* (non-Javadoc)
	 * @see org.springframework.context.ApplicationContextAware#setApplicationContext(org.springframework.context.ApplicationContext)
	 */
	@Override
	public void setApplicationContext(ApplicationContext context) {
		  this.applicationContext = context;
	}
}
