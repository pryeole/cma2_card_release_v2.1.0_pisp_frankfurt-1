/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.direct.debits.routing.adapter.test.routing;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.context.ApplicationContext;

import com.capgemini.psd2.account.direct.debits.routing.adapter.impl.AccountDirectDebitsRoutingAdapter;
import com.capgemini.psd2.account.direct.debits.routing.adapter.routing.AccountDirectDebitsCoreSystemAdapterFactory;
import com.capgemini.psd2.aisp.adapter.AccountDirectDebitsAdapter;

/**
 * The Class AccountDirectDebitsAdapterFactoryTest.
 */
public class AccountDirectDebitsAdapterFactoryTest {
	
	/** The application context. */
	@Mock
	private ApplicationContext applicationContext;
	
	/** The account direct debits core system adapter factory. */
	@InjectMocks
	private AccountDirectDebitsCoreSystemAdapterFactory accountDirectDebitsCoreSystemAdapterFactory;
	
	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Test account direct debits adapter.
	 */
	@Test
	public void testAccountDirectDebitsAdapter() {
		AccountDirectDebitsAdapter accountDirectDebitsAdapter = new AccountDirectDebitsRoutingAdapter();
		when(applicationContext.getBean(anyString())).thenReturn(accountDirectDebitsAdapter);
		AccountDirectDebitsAdapter accountDirectDebitsAdapterResult = (AccountDirectDebitsRoutingAdapter) accountDirectDebitsCoreSystemAdapterFactory.getAdapterInstance("accountBalanceAdapter");
		assertEquals(accountDirectDebitsAdapter, accountDirectDebitsAdapterResult);
	}
	
	

}
