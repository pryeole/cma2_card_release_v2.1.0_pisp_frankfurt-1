package com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.client.InternationalPaymentConsentsFoundationServiceClient;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.delegate.InternationalPaymentConsentsFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.domain.PaymentInstructionProposalInternational;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.transformer.InternationalPaymentConsentsFoundationServiceTransformer;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.operations.adapter.InternationalPaymentStagingAdapter;
import com.capgemini.psd2.rest.client.model.RequestInfo;

@Component
public class InternationalPaymentConsentsFoundationServiceAdapter implements InternationalPaymentStagingAdapter{
	
	@Autowired
	private InternationalPaymentConsentsFoundationServiceDelegate intPayConsDelegate;

	@Autowired
	private InternationalPaymentConsentsFoundationServiceClient intPayConsClient;

	@Autowired
	private InternationalPaymentConsentsFoundationServiceTransformer intPayConsTransformer;
	
	public CustomIPaymentConsentsPOSTResponse retrieveStagedInternationalPaymentConsents(CustomPaymentStageIdentifiers c,
			Map<String, String> params) {
		
		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders httpHeaders = intPayConsDelegate.createPaymentRequestHeadersPost(c,params);
		String domesticPaymentURL = intPayConsDelegate.getPaymentFoundationServiceURL(c.getPaymentConsentId());
		requestInfo.setUrl(domesticPaymentURL);
		PaymentInstructionProposalInternational  paymentInstructionProposal = intPayConsClient.restTransportForInternationalPaymentFoundationService(requestInfo, PaymentInstructionProposalInternational .class, httpHeaders);
		return intPayConsTransformer.transformInternationalPaymentResponse(paymentInstructionProposal);
	}
	
	public CustomIPaymentConsentsPOSTResponse createStagingInternationalPaymentConsents(
			CustomIPaymentConsentsPOSTRequest paymentConsentsRequest,
			CustomPaymentStageIdentifiers customStageIdentifiers, Map<String, String> params) {
		
		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders httpHeaders = intPayConsDelegate.createPaymentRequestHeadersPost(customStageIdentifiers,params);
		String internationalPaymentPostURL = intPayConsDelegate.postPaymentFoundationServiceURL();
		requestInfo.setUrl(internationalPaymentPostURL);
		PaymentInstructionProposalInternational paymentInstructionProposalRequest = intPayConsDelegate.transformInternationalConsentResponseFromAPIToFDForInsert(paymentConsentsRequest,params);
		PaymentInstructionProposalInternational paymentInstructionProposalResponse = intPayConsClient.restTransportForInternationalPaymentFoundationServicePost(requestInfo, paymentInstructionProposalRequest, PaymentInstructionProposalInternational.class, httpHeaders);
		
		return intPayConsDelegate.transformInternationalConsentResponseFromFDToAPIForInsert(paymentInstructionProposalResponse);		
	}
	
	@Override
	public CustomIPaymentConsentsPOSTResponse processInternationalPaymentConsents(
			CustomIPaymentConsentsPOSTRequest paymentConsentsRequest,
			CustomPaymentStageIdentifiers customStageIdentifiers, Map<String, String> params,
			OBExternalConsentStatus1Code successStatus, OBExternalConsentStatus1Code failureStatus) {
		
		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders httpHeaders = intPayConsDelegate.createPaymentRequestHeadersPost(customStageIdentifiers, params);
		String internationalPaymentPostURL = intPayConsDelegate.postPaymentFoundationServiceURL();
		requestInfo.setUrl(internationalPaymentPostURL);
		PaymentInstructionProposalInternational paymentInstructionProposalRequest = intPayConsDelegate.transformInternationalConsentResponseFromAPIToFDForInsert(paymentConsentsRequest,params);
		PaymentInstructionProposalInternational paymentInstructionProposalResponse = intPayConsClient.restTransportForInternationalPaymentFoundationServicePost(requestInfo, paymentInstructionProposalRequest, PaymentInstructionProposalInternational.class, httpHeaders);
		
		return intPayConsDelegate.transformInternationalConsentResponseFromFDToAPIForInsert(paymentInstructionProposalResponse);
	
	}

}
