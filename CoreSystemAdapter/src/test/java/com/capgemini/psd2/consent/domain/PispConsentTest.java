package com.capgemini.psd2.consent.domain;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.capgemini.psd2.enums.ConsentStatusEnum;

public class PispConsentTest {

	@Test
	public void testConsent() {
		PispConsent consent = new PispConsent();
		consent.setTppCId("tpp123");
		consent.setPsuId("user123");
		consent.setConsentId("a1e590ad-73dc-453a-841e-2a2ef055e878");
		consent.setStartDate("05-07-2017");
		consent.setStatus(ConsentStatusEnum.AUTHORISED);
		consent.setPaymentId("23243534");
		AccountDetails accountDetail = new AccountDetails();
		accountDetail.setAccountId("22289");
		accountDetail.setAccountNSC("SC802001");
		accountDetail.setAccountNumber("1022675");
		consent.setAccountDetails(accountDetail);

		assertEquals("tpp123", consent.getTppCId());
		assertEquals("user123", consent.getPsuId());
		assertEquals("a1e590ad-73dc-453a-841e-2a2ef055e878", consent.getConsentId());
		assertEquals(accountDetail, consent.getAccountDetails());
		assertEquals("05-07-2017", consent.getStartDate());
		assertEquals(ConsentStatusEnum.AUTHORISED, consent.getStatus());
		assertEquals("23243534", consent.getPaymentId());
	}
}
