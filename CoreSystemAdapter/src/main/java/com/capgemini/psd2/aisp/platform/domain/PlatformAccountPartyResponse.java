package com.capgemini.psd2.aisp.platform.domain;

import java.util.Map;

import com.capgemini.psd2.aisp.domain.OBReadParty1;


public class PlatformAccountPartyResponse {
	
	// CMA2 response
		private OBReadParty1 obReadParty1;
		
		// Additional Information
		private Map<String, String> additionalInformation;
		
		public OBReadParty1 getobReadParty1() {
			return obReadParty1;
		}

		public void setobReadParty1(OBReadParty1 obReadParty1) {
			this.obReadParty1 = obReadParty1;
		}

		public Map<String, String> getAdditionalInformation() {
			return additionalInformation;
		}

		public void setAdditionalInformation(Map<String, String> additionalInformation) {
			this.additionalInformation = additionalInformation;
		}

}
