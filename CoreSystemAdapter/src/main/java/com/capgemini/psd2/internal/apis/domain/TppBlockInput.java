package com.capgemini.psd2.internal.apis.domain;

public class TppBlockInput {
	
	private String description;
	private ActionEnum action;
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public ActionEnum getAction() {
		return action;
	}
	public void setAction(ActionEnum action) {
		this.action = action;
	}
	
}
