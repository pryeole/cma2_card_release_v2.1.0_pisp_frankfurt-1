package com.capgemini.psd2.pisp.domain;

import java.util.Objects;

import javax.validation.Valid;

import com.capgemini.psd2.pisp.enums.ProcessExecutionStatusEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

public class DStandingOrderPOST201Response extends OBWriteDomesticStandingOrderResponse1 {

	@JsonProperty("Links")
	private PaymentSetupPOSTResponseLinks links = null;

	@JsonProperty("Meta")
	private PaymentSetupPOSTResponseMeta meta = null;


	// Added manually for cma3 design
	private ProcessExecutionStatusEnum processExecutionStatus = null;

	@JsonIgnore
	public ProcessExecutionStatusEnum getProcessExecutionStatus() {
		return processExecutionStatus;
	}

	public void setProcessExecutionStatus(ProcessExecutionStatusEnum processExecutionStatus) {
		this.processExecutionStatus = processExecutionStatus;
	}

	
	@ApiModelProperty(value = "")

	@Valid

	public PaymentSetupPOSTResponseLinks getLinks() {
		return links;
	}

	public void setLinks(PaymentSetupPOSTResponseLinks links) {
		this.links = links;
	}

	/**
	 * Get meta
	 * 
	 * @return meta
	 **/
	@ApiModelProperty(value = "")

	@Valid

	public PaymentSetupPOSTResponseMeta getMeta() {
		return meta;
	}

	public void setMeta(PaymentSetupPOSTResponseMeta meta) {
		this.meta = meta;
	}

	

	@Override
	public int hashCode() {
		return Objects.hash(this.getData(), links, meta);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class PaymentSubmitPOST201Response {\n");

		sb.append("    data: ").append(toIndentedString(this.getData())).append("\n");
		sb.append("    links: ").append(toIndentedString(links)).append("\n");
		sb.append("    meta: ").append(toIndentedString(meta)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
