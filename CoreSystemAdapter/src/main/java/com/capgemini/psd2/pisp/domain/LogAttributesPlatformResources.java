package com.capgemini.psd2.pisp.domain;

import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;

public class LogAttributesPlatformResources {

	private String status;
	private String paymentConsentId;
	private PaymentTypeEnum paymentType;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getPaymentConsentId() {
		return paymentConsentId;
	}
	public void setPaymentConsentId(String paymentConsentId) {
		this.paymentConsentId = paymentConsentId;
	}
	public PaymentTypeEnum getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(PaymentTypeEnum paymentType) {
		this.paymentType = paymentType;
	}
	@Override
	public String toString() {
		return "UpdatedPlatformResources [status=" + status + ", paymentConsentId=" + paymentConsentId
				+ ", paymentType=" + paymentType + "]";
	}
	
	
	
}
