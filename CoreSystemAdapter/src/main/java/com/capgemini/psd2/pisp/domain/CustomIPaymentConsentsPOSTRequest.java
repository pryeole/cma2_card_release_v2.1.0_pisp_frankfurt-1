package com.capgemini.psd2.pisp.domain;

public class CustomIPaymentConsentsPOSTRequest extends OBWriteInternationalConsent1 {

	private String createdOn;

	@Override
	public String toString() {
		return "CustomIPaymentConsentsPOSTRequest [createdOn=" + createdOn + "]";
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

}
