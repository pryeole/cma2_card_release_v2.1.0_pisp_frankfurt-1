package com.capgemini.psd2.pisp.domain;

import java.util.Objects;

import javax.validation.Valid;

import com.capgemini.psd2.pisp.enums.ProcessExecutionStatusEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * PaymentSubmitPOST201Response
 */
public class PaymentDomesticSubmitPOST201Response extends OBWriteDomesticResponse1 {

	@JsonProperty("Links")
	private PaymentSetupPOSTResponseLinks links = null;

	@JsonProperty("Meta")
	private PaymentSetupPOSTResponseMeta meta = null;

	public PaymentDomesticSubmitPOST201Response data(OBWriteDataDomesticConsentResponse1 data) {
		return this;
	}

	// Added manually for cma3 design
	private ProcessExecutionStatusEnum processExecutionStatus = null;

	@JsonIgnore
	public ProcessExecutionStatusEnum getProcessExecutionStatus() {
		return processExecutionStatus;
	}

	public void setProcessExecutionStatus(ProcessExecutionStatusEnum processExecutionStatus) {
		this.processExecutionStatus = processExecutionStatus;
	}

	/**
	 * Get data
	 * 
	 * @return data
	 **/
	@ApiModelProperty(value = "")

	public PaymentDomesticSubmitPOST201Response links(PaymentSetupPOSTResponseLinks links) {
		this.links = links;
		return this;
	}

	/**
	 * Get links
	 * 
	 * @return links
	 **/
	@ApiModelProperty(value = "")

	@Valid

	public PaymentSetupPOSTResponseLinks getLinks() {
		return links;
	}

	public void setLinks(PaymentSetupPOSTResponseLinks links) {
		this.links = links;
	}

	public PaymentDomesticSubmitPOST201Response meta(PaymentSetupPOSTResponseMeta meta) {
		this.meta = meta;
		return this;
	}

	/**
	 * Get meta
	 * 
	 * @return meta
	 **/
	@ApiModelProperty(value = "")

	@Valid

	public PaymentSetupPOSTResponseMeta getMeta() {
		return meta;
	}

	public void setMeta(PaymentSetupPOSTResponseMeta meta) {
		this.meta = meta;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		PaymentDomesticSubmitPOST201Response paymentSubmitPOST201Response = (PaymentDomesticSubmitPOST201Response) o;
		return Objects.equals(this.getData(), paymentSubmitPOST201Response.getData())
				&& Objects.equals(this.links, paymentSubmitPOST201Response.links)
				&& Objects.equals(this.meta, paymentSubmitPOST201Response.meta);
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.getData(), links, meta);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class PaymentSubmitPOST201Response {\n");

		sb.append("    data: ").append(toIndentedString(this.getData())).append("\n");
		sb.append("    links: ").append(toIndentedString(links)).append("\n");
		sb.append("    meta: ").append(toIndentedString(meta)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
