package com.capgemini.psd2.pisp.domain;

public class CustomFilePaymentSetupPOSTRequest extends OBWriteFileConsent1 {

	private String createdOn;
	private OBExternalConsentStatus2Code filePaymentSetupstatus;
	private String filePaymentSetupStatusUpdateDateTime;

	public OBExternalConsentStatus2Code getFilePaymentSetupstatus() {
		return filePaymentSetupstatus;
	}

	public void setFilePaymentSetupstatus(OBExternalConsentStatus2Code filePaymentSetupstatus) {
		this.filePaymentSetupstatus = filePaymentSetupstatus;
	}

	public String getFilePaymentSetupStatusUpdateDateTime() {
		return filePaymentSetupStatusUpdateDateTime;
	}

	public void setFilePaymentSetupStatusUpdateDateTime(String filePaymentSetupStatusUpdateDateTime) {
		this.filePaymentSetupStatusUpdateDateTime = filePaymentSetupStatusUpdateDateTime;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	@Override
	public String toString() {
		return "CustomFilePaymentSetupPOSTRequest [createdOn=" + createdOn + ", filePaymentSetupstatus="
				+ filePaymentSetupstatus + ", filePaymentSetupStatusUpdateDateTime="
				+ filePaymentSetupStatusUpdateDateTime + "]";
	}

}
