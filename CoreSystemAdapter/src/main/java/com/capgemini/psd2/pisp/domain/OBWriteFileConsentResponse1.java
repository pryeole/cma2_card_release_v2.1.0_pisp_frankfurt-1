package com.capgemini.psd2.pisp.domain;

import java.util.Objects;
import com.capgemini.psd2.pisp.domain.OBWriteDataFileConsentResponse1;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * OBWriteFileConsentResponse1
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-09-05T13:53:27.174+05:30")

public class OBWriteFileConsentResponse1 {
	@JsonProperty("Data")
	private OBWriteDataFileConsentResponse1 data = null;

	//Added manually
	@JsonProperty("Links")
	private PaymentSetupPOSTResponseLinks links = null;

	// Added manually
	@JsonProperty("Meta")
	private PaymentSetupPOSTResponseMeta meta = null;

	public OBWriteFileConsentResponse1 data(OBWriteDataFileConsentResponse1 data) {
		this.data = data;
		return this;
	}

	/**
	 * Get data
	 * 
	 * @return data
	 **/
	@ApiModelProperty(required = true, value = "")
	@NotNull

	@Valid

	public OBWriteDataFileConsentResponse1 getData() {
		return data;
	}

	public void setData(OBWriteDataFileConsentResponse1 data) {
		this.data = data;
	}



	  public OBWriteFileConsentResponse1 links(PaymentSetupPOSTResponseLinks links) {
		    this.links = links;
		    return this;
		  }

		   /**
		   * Get links
		   * @return links
		  **/
		  @ApiModelProperty(value = "")

		  @Valid

		  public PaymentSetupPOSTResponseLinks getLinks() {
		    return links;
		  }

		  public void setLinks(PaymentSetupPOSTResponseLinks links) {
		    this.links = links;
		  }

		  public OBWriteFileConsentResponse1 meta(PaymentSetupPOSTResponseMeta meta) {
		    this.meta = meta;
		    return this;
		  }

		   /**
		   * Get meta
		   * @return meta
		  **/
		  @ApiModelProperty(value = "")

		  @Valid

		  public PaymentSetupPOSTResponseMeta getMeta() {
		    return meta;
		  }

		  public void setMeta(PaymentSetupPOSTResponseMeta meta) {
		    this.meta = meta;
		  }
	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		OBWriteFileConsentResponse1 obWriteFileConsentResponse1 = (OBWriteFileConsentResponse1) o;
	    return Objects.equals(this.data, obWriteFileConsentResponse1.data) &&
	        Objects.equals(this.links, obWriteFileConsentResponse1.links) &&
	        Objects.equals(this.meta, obWriteFileConsentResponse1.meta);
	}

	@Override
	public int hashCode() {
		return Objects.hash(data);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class OBWriteFileConsentResponse1 {\n");
		sb.append("    data: ").append(toIndentedString(data)).append("\n");
	    sb.append("    links: ").append(toIndentedString(links)).append("\n");
	    sb.append("    meta: ").append(toIndentedString(meta)).append("\n");
	    sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
