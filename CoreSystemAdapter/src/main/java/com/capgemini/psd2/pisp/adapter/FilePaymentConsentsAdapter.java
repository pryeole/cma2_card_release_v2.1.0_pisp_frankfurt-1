package com.capgemini.psd2.pisp.adapter;

import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import com.capgemini.psd2.pisp.domain.CustomFilePaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.PlatformFilePaymentsFileResponse;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;

public interface FilePaymentConsentsAdapter {

		public CustomFilePaymentConsentsPOSTResponse createStagingFilePaymentConsents(
				CustomFilePaymentSetupPOSTRequest paymentSetupRequest, CustomPaymentStageIdentifiers stageIdentifiers,
				Map<String, String> params);	
		
		public CustomFilePaymentConsentsPOSTResponse fetchFilePaymentMetadata(CustomPaymentStageIdentifiers stageIdentifiers, Map<String, String> params);

		public String uploadFilePaymentConsents(MultipartFile paymentFile, String consentId); 
		
		public CustomFilePaymentConsentsPOSTResponse updateStagingFilePaymentConsents(CustomFilePaymentConsentsPOSTResponse response);

		public PlatformFilePaymentsFileResponse downloadFilePaymentConsents(
				CustomPaymentStageIdentifiers paymentStageIdentifiers);

}
