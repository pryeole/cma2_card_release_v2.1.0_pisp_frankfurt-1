package com.capgemini.psd2.pisp.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.capgemini.psd2.pisp.enums.ProcessConsentStatusEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Document(collection = "paymentSetupFoundationResources")
public class CustomDPaymentConsentsPOSTResponse extends OBWriteDomesticConsentResponse1 {
	@Id
	private String id;
	private Object fraudScore;
	private ProcessConsentStatusEnum consentPorcessStatus = null;

	@JsonIgnore
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the fraudScore
	 */
	@JsonIgnore
	public Object getFraudScore() {
		return fraudScore;
	}

	/**
	 * @param fraudScore
	 *            the fraudScore to set
	 */
	public void setFraudScore(Object fraudScore) {
		this.fraudScore = fraudScore;

	}

	@JsonIgnore
	public ProcessConsentStatusEnum getConsentPorcessStatus() {
		return consentPorcessStatus;
	}

	public void setConsentPorcessStatus(ProcessConsentStatusEnum consentPorcessStatus) {
		this.consentPorcessStatus = consentPorcessStatus;
	}

	@Override
	public String toString() {
		return "CustomDPaymentConsentsPOSTResponse [id=" + id + ", fraudScore=" + fraudScore + ", consentPorcessStatus="
				+ consentPorcessStatus + "]";
	}

}
