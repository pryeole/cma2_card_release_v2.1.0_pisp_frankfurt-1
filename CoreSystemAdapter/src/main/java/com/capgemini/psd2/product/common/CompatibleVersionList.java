package com.capgemini.psd2.product.common;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "app")
public class CompatibleVersionList {

	final static String CMA1 = "1.0";

	List<String> compatibleVersionList = new ArrayList<>();

	private boolean modified;

	public List<String> getCompatibleVersionList() {
		return compatibleVersionList;
	}

	public void setCompatibleVersionList(List<String> compatibleVersionList) {
		this.compatibleVersionList = compatibleVersionList;
	}

	public List<String> fetchVersionList() {
		if (!modified) {
			if (compatibleVersionList.contains(CMA1)) {
				compatibleVersionList.add(null);
			}
			modified = true;
		}
		return compatibleVersionList;
	}
}
