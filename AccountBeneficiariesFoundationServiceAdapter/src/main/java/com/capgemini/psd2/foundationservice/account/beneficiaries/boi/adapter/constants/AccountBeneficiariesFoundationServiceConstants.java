
package com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.constants;

public class AccountBeneficiariesFoundationServiceConstants {
	public static final String ACCOUNT_ID = "accountId";
	public static final String NSC = "nsc";
	public static final String ACCOUNT_NUMBER = "accountNumber";
	public static final String BICFI = "BICFI";
	public static final String IBAN = "IBAN";
	public static final String SORTCODEACCOUNTNUMBER = "SORTCODEACCOUNTNUMBER";
	public static final String UK_OBIE_BICFI = "UK.OBIE.BICFI";
	public static final String UK_OBIE_IBAN = "UK.OBIE.IBAN";
	public static final String UK_OBIE_SortCodeAccountNumber = "UK.OBIE.SortCodeAccountNumber";
	public static final String UK_OBIE_ABACodeAccountNumber = "UK.OBIE.ABACodeAccountNumber";
	public static final String UK_BOI_AccountNumber = "UK.BOI.AccountNumber";
	public static final String PARENTASORTCODENSCNUMBERHEADER = "parent-national-sort-code-nsc-number";
	public static final String ACCOUNTNUMBERHEADER = "account-number";
	public static final String CHANNEL_ID = "channelId";
}
