
package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.client;

import org.springframework.http.HttpHeaders;

import com.capgemini.psd2.adapter.fraudnet.domain.FraudServiceResponse;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain.FraudServiceRequest;
import com.capgemini.psd2.rest.client.model.RequestInfo;

@FunctionalInterface
public interface FraudnetViaMulesoftProxyClient {

	public FraudServiceResponse fraudnetViaMulesoftCall(RequestInfo requestInfo, FraudServiceRequest fraudServiceRequest, Class<FraudServiceResponse> response, HttpHeaders httpHeaders);

}
