package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * The entity or resource which acts as the subject of the event from the source
 * / source sub system
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public enum Type {

	PROFILE("Profile"),

	PARTY("Party"),

	ACCOUNT("Account"),

	BENEFICIARY("Beneficiary"),

	PAYMENT("Payment"),

	STANDING_ORDER("Standing Order"),

	DEVICE("Device"),

	CARD("Card");

	private String value;

	Type(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	@Override
	@JsonValue
	public String toString() {
		return String.valueOf(value);
	}

	@JsonCreator
	public static Type fromValue(String text) {
		for (Type b : Type.values()) {
			if (String.valueOf(b.value).equals(text)) {
				return b;
			}
		}
		return null;
	}
}
