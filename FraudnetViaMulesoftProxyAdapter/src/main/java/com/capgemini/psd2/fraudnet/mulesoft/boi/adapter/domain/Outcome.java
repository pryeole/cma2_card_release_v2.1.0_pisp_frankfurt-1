package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * The outcome of the user event whether it was a success or failure
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public enum Outcome {

	SUCCESS("Success"),

	FAILED("Failed");

	private String value;

	Outcome(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	@Override
	@JsonValue
	public String toString() {
		return String.valueOf(value);
	}

	@JsonCreator
	public static Outcome fromValue(String text) {
		for (Outcome b : Outcome.values()) {
			if (String.valueOf(b.value).equals(text)) {
				return b;
			}
		}
		return null;
	}
}
