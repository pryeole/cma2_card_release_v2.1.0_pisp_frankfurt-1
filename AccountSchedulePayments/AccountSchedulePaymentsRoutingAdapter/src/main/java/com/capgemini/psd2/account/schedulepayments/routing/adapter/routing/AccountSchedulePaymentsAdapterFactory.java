package com.capgemini.psd2.account.schedulepayments.routing.adapter.routing;

import com.capgemini.psd2.aisp.adapter.AccountSchedulePaymentsAdapter;

@FunctionalInterface
public interface AccountSchedulePaymentsAdapterFactory {

	/**
	 * Gets the adapter instance.
	 *
	 * @param coreSystemName the core system name
	 * @return the adapter instance
	 */
	public AccountSchedulePaymentsAdapter getAdapterInstance(String coreSystemName);
}
