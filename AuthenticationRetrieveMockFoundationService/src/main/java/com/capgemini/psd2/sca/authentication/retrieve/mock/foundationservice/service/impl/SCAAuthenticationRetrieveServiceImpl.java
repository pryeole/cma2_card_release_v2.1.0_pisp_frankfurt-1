package com.capgemini.psd2.sca.authentication.retrieve.mock.foundationservice.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceException;
import com.capgemini.psd2.foundationservice.validator.ValidationUtility;
import com.capgemini.psd2.sca.authentication.retrieve.mock.foundationservice.domain.AuthenticationParameters;
import com.capgemini.psd2.sca.authentication.retrieve.mock.foundationservice.repository.SCAAuthenticationRetrieveServiceRepository;
import com.capgemini.psd2.sca.authentication.retrieve.mock.foundationservice.service.SCAAuthenticationRetrieveService;


@Service
public class SCAAuthenticationRetrieveServiceImpl implements SCAAuthenticationRetrieveService {

	@Autowired
	private SCAAuthenticationRetrieveServiceRepository authenticationParametersRetrieveRepository;

	@Autowired
	private ValidationUtility validationUtility;

	@Override
	public AuthenticationParameters retrieveAuthentication( String digitalUserId) {
	System.out.println("digitalUserId----------"+digitalUserId);
		AuthenticationParameters authParameters = authenticationParametersRetrieveRepository.findAuthenticationParametersByDigitalUserDigitalUserIdentifier(digitalUserId);
		System.out.println("authParameters----------"+authParameters);
		if(authParameters == null)
			throw MockFoundationServiceException.populateMockFoundationServiceException(ErrorCodeEnum.AUTHENTICATION_FAILURE_LOGIN_AUTH);
		return authParameters;
	}

	


	

}
