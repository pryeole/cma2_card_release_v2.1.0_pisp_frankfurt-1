package com.capgemini.psd2.pisp.payment.setup.routing.adapter.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.payment.setup.routing.adapter.routing.IPaymentConsentsAdapterFactory;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.operations.adapter.InternationalPaymentStagingAdapter;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component("iPaymentConsentsStagingRoutingAdapter")
public class IPaymentConsentsStagingRoutingAdapterImpl implements InternationalPaymentStagingAdapter {

	private InternationalPaymentStagingAdapter beanInstance;

	/** The factory. */
	@Autowired
	private IPaymentConsentsAdapterFactory factory;

	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	/** The default adapter. */
	@Value("${app.paymentSetupStagingAdapter}")
	private String paymentSetupStagingAdapter;

	@Override
	public CustomIPaymentConsentsPOSTResponse processInternationalPaymentConsents(
			CustomIPaymentConsentsPOSTRequest paymentSetupRequest, CustomPaymentStageIdentifiers stageIdentifiers,
			Map<String, String> params, OBExternalConsentStatus1Code successStatus,
			OBExternalConsentStatus1Code failureStatus) {
		return getInternationalRoutingAdapter().processInternationalPaymentConsents(paymentSetupRequest,
				stageIdentifiers, addHeaderParams(params), successStatus, failureStatus);
	}

	@Override
	public CustomIPaymentConsentsPOSTResponse retrieveStagedInternationalPaymentConsents(
			CustomPaymentStageIdentifiers customPaymentSetupInfo, Map<String, String> params) {
		return getInternationalRoutingAdapter().retrieveStagedInternationalPaymentConsents(customPaymentSetupInfo,
				addHeaderParams(params));
	}

	private InternationalPaymentStagingAdapter getInternationalRoutingAdapter() {
		if (beanInstance == null)
			beanInstance = factory.getInternationalPaymentSetupStagingInstance(paymentSetupStagingAdapter);
		return beanInstance;
	}

	private Map<String, String> addHeaderParams(Map<String, String> mapParam) {
		if (NullCheckUtils.isNullOrEmpty(mapParam))
			mapParam = new HashMap<>();

		if (null != reqHeaderAtrributes.getToken() && null != reqHeaderAtrributes.getToken().getSeviceParams()) {
			mapParam.put(PSD2Constants.CHANNEL_IN_REQ_HEADER,
					reqHeaderAtrributes.getToken().getSeviceParams().get(PSD2Constants.CHANNEL_NAME));
		}
		mapParam.put(PSD2Constants.USER_IN_REQ_HEADER, reqHeaderAtrributes.getPsuId());
		mapParam.put(PSD2Constants.CORRELATION_REQ_HEADER, reqHeaderAtrributes.getCorrelationId());
		mapParam.put(PSD2Constants.TENANT_ID, reqHeaderAtrributes.getTenantId());
		return mapParam;
	}

}
