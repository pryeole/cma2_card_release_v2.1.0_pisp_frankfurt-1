package com.capgemini.psd2.security.consent.rest.controllers;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ConsentApplicationRestController {
	
	@RequestMapping("/{intentType}/checkSession")
	public void checkSession(@PathVariable("intentType") String intentType) {
		//Dummy Method to check session.		
	}

}
