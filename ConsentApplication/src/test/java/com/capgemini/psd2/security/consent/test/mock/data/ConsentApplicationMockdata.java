package com.capgemini.psd2.security.consent.test.mock.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data;
import com.capgemini.psd2.aisp.domain.OBExternalPermissions1Code;
import com.capgemini.psd2.aisp.domain.OBExternalRequestStatus1Code;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1;
import com.capgemini.psd2.consent.domain.CustomerAccountInfo;

public class ConsentApplicationMockdata {

	public static OBReadConsentResponse1 mockAccountRequestPOSTResponse;
	public static List<CustomerAccountInfo> mockCustomerAccountInfoList;
/*	public static CustomClientDetails mockCustomClientDetails;
	
	public static CustomClientDetails getCustomClientDetails(){
		mockCustomClientDetails = new CustomClientDetails();
		mockCustomClientDetails.setLegalEntityName("MoneyWise Ltd.");
		return mockCustomClientDetails;
	}*/
	
	public static OBReadConsentResponse1 getAccountRequestPOSTResponse() {
		mockAccountRequestPOSTResponse = new OBReadConsentResponse1();

		OBReadConsentResponse1Data data = new OBReadConsentResponse1Data();
		List<OBExternalPermissions1Code> permissions = new ArrayList<>();
		permissions.add(OBExternalPermissions1Code.READACCOUNTSBASIC);
		permissions.add(OBExternalPermissions1Code.READACCOUNTSDETAIL);
		permissions.add(OBExternalPermissions1Code.READBALANCES);
		data.setPermissions(permissions);
		data.setCreationDateTime("2017-12-25T00:00:00-00:00");
		data.setExpirationDateTime("2017-05-02T00:00:00-00:00");
		data.setTransactionFromDateTime("2017-05-03T00:00:00-00:00");
		data.setTransactionToDateTime("2017-12-03T00:00:00-00:00");
		data.setConsentId("af5b90c1-64b5-4a52-ba55-5eed68b2a269");
		data.setStatus(OBExternalRequestStatus1Code.AWAITINGAUTHORISATION);
		mockAccountRequestPOSTResponse.setData(data);
		mockAccountRequestPOSTResponse.setRisk(null);
		return mockAccountRequestPOSTResponse;
	}
	
	public static OBReadConsentResponse1 getAccountRequestPOSTResponseInvalidStatus() {
		OBReadConsentResponse1 mockAccountRequestPOSTResponse = new OBReadConsentResponse1();

		OBReadConsentResponse1Data data = new OBReadConsentResponse1Data();
		List<OBExternalPermissions1Code> permissions = new ArrayList<>();
		permissions.add(OBExternalPermissions1Code.READACCOUNTSBASIC);
		permissions.add(OBExternalPermissions1Code.READACCOUNTSDETAIL);
		permissions.add(OBExternalPermissions1Code.READBALANCES);
		data.setPermissions(permissions);
		data.setCreationDateTime("2017-12-25T00:00:00-00:00");
		data.setExpirationDateTime("2017-05-02T00:00:00-00:00");
		data.setTransactionFromDateTime("2017-05-03T00:00:00-00:00");
		data.setTransactionToDateTime("2017-12-03T00:00:00-00:00");
		data.setConsentId("af5b90c1-64b5-4a52-ba55-5eed68b2a269");
		data.setStatus(OBExternalRequestStatus1Code.AUTHORISED);
		mockAccountRequestPOSTResponse.setData(data);
		mockAccountRequestPOSTResponse.setRisk(null);
		return mockAccountRequestPOSTResponse;
	}
	
	public static List<CustomerAccountInfo> getCustomerAccountInfoList(){
		mockCustomerAccountInfoList = new ArrayList<>();
		CustomerAccountInfo customerAccountInfo1 = new CustomerAccountInfo();
		customerAccountInfo1.setAccountName("John Doe");
		customerAccountInfo1.setUserId("1234");
		customerAccountInfo1.setAccountNumber("10203345");
		customerAccountInfo1.setAccountNSC("SC802001");
		customerAccountInfo1.setCurrency("EUR");
		customerAccountInfo1.setNickname("John");
		customerAccountInfo1.setAccountType("checking");

		CustomerAccountInfo customerAccountInfo2 = new CustomerAccountInfo();
		customerAccountInfo2.setAccountName("Tiffany Doe");
		customerAccountInfo2.setUserId("1234");
		customerAccountInfo2.setAccountNumber("10203346");
		customerAccountInfo2.setAccountNSC("SC802002");
		customerAccountInfo2.setCurrency("GRP");
		customerAccountInfo2.setNickname("Tiffany");
		customerAccountInfo2.setAccountType("savings");

		mockCustomerAccountInfoList.add(customerAccountInfo1);
		mockCustomerAccountInfoList.add(customerAccountInfo2);

		return mockCustomerAccountInfoList;
	}

}
