/*package com.capgemini.psd2.security.consent.auth.test.jwt;

import static org.junit.Assert.assertNotEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.security.consent.filters.JwtTokenAuthenticationProcessingFilter;
import com.capgemini.psd2.security.exceptions.PSD2AuthenticationException;
import com.capgemini.psd2.security.exceptions.SecurityErrorCodeEnum;
import com.capgemini.psd2.security.models.JwtAuthenticationToken;
import com.capgemini.psd2.security.models.RawAccessJwtToken;
import com.capgemini.psd2.security.models.SecurityRequestAttributes;
import com.capgemini.psd2.security.repository.IdTokenMongoRepository;

@RunWith(SpringJUnit4ClassRunner.class)
public class JwtTokenAuthenticationProcessingFilterTest {

	private RequestHeaderAttributes requestHeaderAttributes;

	private SecurityRequestAttributes securityRequestAttributes;

	@Mock
	private AuthenticationFailureHandler failureHandler;

	@Mock
	private IdTokenMongoRepository idTokenMongoRepository;

	private HttpServletRequest request;

	private HttpServletResponse response;

	@Mock
	private RequestMatcher matcher;

	@Mock
	private AuthenticationManager authManager;
	@Mock
	private AbstractAuthenticationProcessingFilter authFilter ;

	@InjectMocks
	private JwtTokenAuthenticationProcessingFilter jwtTokenAuthenticationProcessingFilter ;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		requestHeaderAttributes = new RequestHeaderAttributes();
		requestHeaderAttributes.setCorrelationId("ba4f73f8-9a60-425b-aed8-2a7ef2509fea");
		securityRequestAttributes = new SecurityRequestAttributes();
		jwtTokenAuthenticationProcessingFilter = new JwtTokenAuthenticationProcessingFilter(
				failureHandler, matcher, "0123456789abcdef");
	}

	@Test
	public void testAttemptAuthenticationSuccessFlow() throws AuthenticationException, IOException, ServletException {
		Map<String, String> paramMap = new HashMap<>();
		String idToken = "vntwKKvsq6nBd1YkYTVVqYpvekNqycUPypZkI2wJq5v2eGPD0TDVst8p5Zf5kJIJciladIpQLdsEjGdXGUxntjS9aOaUvBplol17muUFT7cYqe5CrmBIzQQXelw8XJ31cZw++VMBJN6rE+1VOoUtdLDB1RmiCV4RyYhBYJcQ9Ei98/pKRN29HkAANJJCr+SiWRUMmkiNhlq2It/enThFPETM8wqB/mAI1Lte5lKr0XtVF5Khz724Vq/LMZ4571kybcYK2N0b3iydo2KB4KrqBLlTZSg0Gn67IRE+MPM3+7/olqwK/l3+vhAVpyMZ2DKsSVLWAJXUSRmKLdOYTLmjJh3XYLUXcJr3THO6ILKUEtC0Yl9MvIiff2+q5hwlKvVPrY0SHANRhjJ3ZunDS78sHe5OQqlzBP4WTDO/bLQvoOxtiFfFy48To7Jz2CMXDN8IgOj7WmZLPQmbuSaDjLOJig==";
		String oAuthUrl = "Qxl7JY2bCzj/1O3i7r8U7Gn/DJ10ntnMa7hhGzE8i8wBqfkWb98h3QmIRyskQrF1QDJDTg2KGUmbWrA05mx6xVIkbLdaScZnRNv8m6jBsvi/YMbtYnRkY4Y8B7SZhuXvJIpmg1InzhuLLWHj/hYO41fXKZfsIQTLwkmQbYtbLsXMy50yhfPoHWVUGXBlHOdnExGSjpj4U Ll4CYDP6I9V5DWUdChH2sJ4W8wozK1QWj1RsdVVbkdOZLHjlPI/tlsbj1oBmZzqQi3Twq4Dv80zQ==";
		paramMap.put("i"
				+ "dToken", idToken);
		securityRequestAttributes.setParamMap(paramMap);
		securityRequestAttributes.setRequestIdParamName("AccountRequestId");
		securityRequestAttributes.setCallBackEncUrl(oAuthUrl);
		ReflectionTestUtils.setField(jwtTokenAuthenticationProcessingFilter, "securityRequestAttributes",
				securityRequestAttributes);
		ReflectionTestUtils.setField(jwtTokenAuthenticationProcessingFilter, "requestHeaderAttributes",
				requestHeaderAttributes);
		Authentication authentication= new JwtAuthenticationToken();
		request = mock(HttpServletRequest.class);
		HttpSession httpSession = mock(HttpSession.class);
		when(request.getSession()).thenReturn(httpSession);
		when(idTokenMongoRepository.findByToken(anyString())).thenReturn(null);
		when(request.getServletPath()).thenReturn("xyz/");
		ReflectionTestUtils.setField(jwtTokenAuthenticationProcessingFilter, "idTokenMongoRepository",
				idTokenMongoRepository);
		ReflectionTestUtils.setField(jwtTokenAuthenticationProcessingFilter, "authenticationManager",
				authManager);
		assertNotEquals(authentication,jwtTokenAuthenticationProcessingFilter.attemptAuthentication(request, response));
	}

	@SuppressWarnings("unchecked")
	@Test(expected = PSD2AuthenticationException.class)
	public void testAttemptAuthenticationTokenExpired() throws AuthenticationException, IOException, ServletException {
		Map<String, String> paramMap = new HashMap<>();
		String idToken = "vntwKKvsq6nBd1YkYTVVqYpvekNqycUPypZkI2wJq5v2eGPD0TDVst8p5Zf5kJIJciladIpQLdsEjGdXGUxntjS9aOaUvBplol17muUFT7cYqe5CrmBIzQQXelw8XJ31cZw++VMBJN6rE+1VOoUtdLDB1RmiCV4RyYhBYJcQ9Ei98/pKRN29HkAANJJCr+SiWRUMmkiNhlq2It/enThFPETM8wqB/mAI1Lte5lKr0XtVF5Khz724Vq/LMZ4571kybcYK2N0b3iydo2KB4KrqBLlTZSg0Gn67IRE+MPM3+7/olqwK/l3+vhAVpyMZ2DKsSVLWAJXUSRmKLdOYTLmjJh3XYLUXcJr3THO6ILKUEtC0Yl9MvIiff2+q5hwlKvVPrY0SHANRhjJ3ZunDS78sHe5OQqlzBP4WTDO/bLQvoOxtiFfFy48To7Jz2CMXDN8IgOj7WmZLPQmbuSaDjLOJig==";
		String oAuthUrl = "Qxl7JY2bCzj/1O3i7r8U7Gn/DJ10ntnMa7hhGzE8i8wBqfkWb98h3QmIRyskQrF1QDJDTg2KGUmbWrA05mx6xVIkbLdaScZnRNv8m6jBsvi/YMbtYnRkY4Y8B7SZhuXvJIpmg1InzhuLLWHj/hYO41fXKZfsIQTLwkmQbYtbLsXMy50yhfPoHWVUGXBlHOdnExGSjpj4U Ll4CYDP6I9V5DWUdChH2sJ4W8wozK1QWj1RsdVVbkdOZLHjlPI/tlsbj1oBmZzqQi3Twq4Dv80zQ==";
		paramMap.put("idToken", idToken);
		securityRequestAttributes.setParamMap(paramMap);
		securityRequestAttributes.setRequestIdParamName("AccountRequestId");
		securityRequestAttributes.setCallBackEncUrl(oAuthUrl);
		ReflectionTestUtils.setField(jwtTokenAuthenticationProcessingFilter, "securityRequestAttributes",
				securityRequestAttributes);
		ReflectionTestUtils.setField(jwtTokenAuthenticationProcessingFilter, "requestHeaderAttributes",
				requestHeaderAttributes);
		request = mock(HttpServletRequest.class);
		HttpSession httpSession = mock(HttpSession.class);
		when(request.getSession()).thenReturn(httpSession);
		when(request.getServletPath()).thenReturn("/");
		RawAccessJwtToken token = new RawAccessJwtToken("token", requestHeaderAttributes);
		when(idTokenMongoRepository.findByToken(anyString())).thenReturn(token);
		ReflectionTestUtils.setField(jwtTokenAuthenticationProcessingFilter, "idTokenMongoRepository",
				idTokenMongoRepository);
		ReflectionTestUtils.setField(jwtTokenAuthenticationProcessingFilter, "authenticationManager",
				authManager);
		
		when(request.getServletPath().lastIndexOf("/")).thenThrow(PSD2AuthenticationException.class);
		jwtTokenAuthenticationProcessingFilter.attemptAuthentication(request, response);
	}
	@Test(expected = PSD2AuthenticationException.class)
	public void testAttemptAuthenticationOAuthUrlExceptionFlow() throws AuthenticationException, IOException, ServletException {
		Map<String, String> paramMap = new HashMap<>();
		String idToken = "vntwKKvsq6nBd1YkYTVVqYpvekNqycUPypZkI2wJq5v2eGPD0TDVst8p5Zf5kJIJciladIpQLdsEjGdXGUxntjS9aOaUvBplol17muUFT7cYqe5CrmBIzQQXelw8XJ31cZw++VMBJN6rE+1VOoUtdLDB1RmiCV4RyYhBYJcQ9Ei98/pKRN29HkAANJJCr+SiWRUMmkiNhlq2It/enThFPETM8wqB/mAI1Lte5lKr0XtVF5Khz724Vq/LMZ4571kybcYK2N0b3iydo2KB4KrqBLlTZSg0Gn67IRE+MPM3+7/olqwK/l3+vhAVpyMZ2DKsSVLWAJXUSRmKLdOYTLmjJh3XYLUXcJr3THO6ILKUEtC0Yl9MvIiff2+q5hwlKvVPrY0SHANRhjJ3ZunDS78sHe5OQqlzBP4WTDO/bLQvoOxtiFfFy48To7Jz2CMXDN8IgOj7WmZLPQmbuSaDjLOJig==";
		paramMap.put("idToken", idToken);
		securityRequestAttributes.setParamMap(paramMap);
		ReflectionTestUtils.setField(jwtTokenAuthenticationProcessingFilter, "securityRequestAttributes",
				securityRequestAttributes);
		ReflectionTestUtils.setField(jwtTokenAuthenticationProcessingFilter, "requestHeaderAttributes",
				requestHeaderAttributes);
		request = mock(HttpServletRequest.class);
		HttpSession httpSession = mock(HttpSession.class);
		when(request.getSession()).thenReturn(httpSession);
	jwtTokenAuthenticationProcessingFilter.attemptAuthentication(request, response);
	}

	@Test(expected = PSD2AuthenticationException.class)
	public void testAttemptAuthenticationidTokenExceptionFlow() throws AuthenticationException, IOException, ServletException {
		Map<String, String> paramMap = new HashMap<>();
		String oAuthUrl = "Qxl7JY2bCzj/1O3i7r8U7Gn/DJ10ntnMa7hhGzE8i8wBqfkWb98h3QmIRyskQrF1QDJDTg2KGUmbWrA05mx6xVIkbLdaScZnRNv8m6jBsvi/YMbtYnRkY4Y8B7SZhuXvJIpmg1InzhuLLWHj/hYO41fXKZfsIQTLwkmQbYtbLsXMy50yhfPoHWVUGXBlHOdnExGSjpj4U Ll4CYDP6I9V5DWUdChH2sJ4W8wozK1QWj1RsdVVbkdOZLHjlPI/tlsbj1oBmZzqQi3Twq4Dv80zQ==";
		securityRequestAttributes.setParamMap(paramMap);
		securityRequestAttributes.setCallBackEncUrl(oAuthUrl);
		ReflectionTestUtils.setField(jwtTokenAuthenticationProcessingFilter, "securityRequestAttributes",
				securityRequestAttributes);
		ReflectionTestUtils.setField(jwtTokenAuthenticationProcessingFilter, "requestHeaderAttributes",
				requestHeaderAttributes);
		request = mock(HttpServletRequest.class);
		HttpSession httpSession = mock(HttpSession.class);
		when(request.getSession()).thenReturn(httpSession);
		jwtTokenAuthenticationProcessingFilter.attemptAuthentication(request, response);
	}
	
	@Test
	public void testSuccessfulAuthentication() throws InstantiationException, IllegalAccessException, NoSuchMethodException, SecurityException, IllegalArgumentException, InvocationTargetException, NoSuchFieldException{
		
		AuthenticationFailureHandler failureHandler = mock(AuthenticationFailureHandler.class);
		RequestMatcher requestMatcher = mock(RequestMatcher.class);
		
		Class<JwtTokenAuthenticationProcessingFilter> clazz = JwtTokenAuthenticationProcessingFilter.class;
		JwtTokenAuthenticationProcessingFilter filter = clazz.getConstructor(AuthenticationFailureHandler.class, RequestMatcher.class,String.class).newInstance(failureHandler,requestMatcher,"0123456789abcdef");
		
		Field headerAttributesField = clazz.getDeclaredField("requestHeaderAttributes");
		headerAttributesField.setAccessible(true);
		headerAttributesField.set(filter, requestHeaderAttributes);
		
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		FilterChain filterChain = mock(FilterChain.class);
		Authentication authentication = mock(Authentication.class);
		
		Method method = clazz.getDeclaredMethod("successfulAuthentication", HttpServletRequest.class,HttpServletResponse.class,FilterChain.class,Authentication.class);
		method.setAccessible(true);
		method.invoke(filter, request,response,filterChain,authentication);
		assert(true);
	}
	
	@Test
	public void testunsuccessfulAuthentication() throws InstantiationException, IllegalAccessException, NoSuchMethodException, SecurityException, IllegalArgumentException, InvocationTargetException, NoSuchFieldException{
		
		AuthenticationFailureHandler failureHandler = mock(AuthenticationFailureHandler.class);
		RequestMatcher requestMatcher = mock(RequestMatcher.class);
		
		Class<JwtTokenAuthenticationProcessingFilter> clazz = JwtTokenAuthenticationProcessingFilter.class;
		JwtTokenAuthenticationProcessingFilter filter = clazz.getConstructor(AuthenticationFailureHandler.class, RequestMatcher.class,String.class).newInstance(failureHandler,requestMatcher,"0123456789abcdef");
		
		Field headerAttributesField = clazz.getDeclaredField("requestHeaderAttributes");
		headerAttributesField.setAccessible(true);
		headerAttributesField.set(filter, requestHeaderAttributes);
		
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		AuthenticationException authenticationException = mock(AuthenticationException.class);
		
		Method method = clazz.getDeclaredMethod("unsuccessfulAuthentication", HttpServletRequest.class,HttpServletResponse.class,AuthenticationException.class);
		method.setAccessible(true);
		method.invoke(filter, request,response,authenticationException);
		assert(true);
	}
}
*/