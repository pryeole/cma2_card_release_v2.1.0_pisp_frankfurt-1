
package com.capgemini.tpp.muleapi.schema;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "issuer",
    "audience",
    "public_key",
    "claims_mapping",
    "name"
})
public class Saml implements Serializable
{

    @JsonProperty("issuer")
    private String issuer;
    @JsonProperty("audience")
    private String audience;
    @JsonProperty("public_key")
    private String publicKey;
    @JsonProperty("claims_mapping")
    private ClaimsMapping claimsMapping;
    @JsonProperty("name")
    private String name;
    private static final long serialVersionUID = -1206525110470060477L;

    @JsonProperty("issuer")
    public String getIssuer() {
        return issuer;
    }

    @JsonProperty("issuer")
    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }

    @JsonProperty("audience")
    public String getAudience() {
        return audience;
    }

    @JsonProperty("audience")
    public void setAudience(String audience) {
        this.audience = audience;
    }

    @JsonProperty("public_key")
    public String getPublicKey() {
        return publicKey;
    }

    @JsonProperty("public_key")
    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    @JsonProperty("claims_mapping")
    public ClaimsMapping getClaimsMapping() {
        return claimsMapping;
    }

    @JsonProperty("claims_mapping")
    public void setClaimsMapping(ClaimsMapping claimsMapping) {
        this.claimsMapping = claimsMapping;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

}
