package com.capgemini.tpp.registration.model;

import org.springframework.stereotype.Component;

import com.capgemini.tpp.ssa.model.SSAModel;

@Component
public class SSAWrapper {

	private SSAModel ssaModel;
	
	private String ssaToken;
	
	private RegistrationRequest registrationRequest;

	public RegistrationRequest getRegistrationRequest() {
		return registrationRequest;
	}

	public void setRegistrationRequest(RegistrationRequest registrationRequest) {
		this.registrationRequest = registrationRequest;
	}

	public SSAModel getSsaModel() {
		return ssaModel;
	}

	public void setSsaModel(SSAModel ssaModel) {
		this.ssaModel = ssaModel;
	}

	public String getSsaToken() {
		return ssaToken;
	}

	public void setSsaToken(String ssaToken) {
		this.ssaToken = ssaToken;
	}
}
