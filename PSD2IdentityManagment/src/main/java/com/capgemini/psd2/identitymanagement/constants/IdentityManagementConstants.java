package com.capgemini.psd2.identitymanagement.constants;

public final class IdentityManagementConstants {
	
	public static final String TECHNICAL_ERROR = "server_error";
	public static final String CLIENT_ID_LDAP = "x-clientid";
    public static final String SECRET_LDAP = "x-secret";
    public static final String AUTHORITIES_LDAP = "x-scopes";
    public static final String TPP_BLOCK_LDAP = "x-block";
    public static final String LEGAL_ENTITY_NAME_LDAP = "o";
    public static final String REGISTERED_ID_LDAP = "cn";
    public static final String REDIRECT_URL_LDAP = "x-redirecturl";
    public static final String CORRELATION_URL_PARAM = "correlationId"; 
    public static final String IS_MEMBER_OF_LDAP = "isMemberOf";
    public static final String AUTHORIZATION_HEADER = "Authorization";
    public static final String BEARER = "Bearer";
    public static final String RES_MULE_CLIENT_ID = "client_id";
    public static final String RES_MULE_CLIENT_SECRET = "client_secret";
    public static final String RES_MULE_REDIRECT_URI = "redirect_uris";
    public static final String RES_MULE_CLIENT_NAME = "name";
    public static final String RES_MULE_ACCESS_TOKEN = "access_token";
    

   /* public static final String CID_PARAM = "client_id";
    public static final String CLIENT_CREDENTIAL_GRANT_TYPE = "client_credentials";
    public static final String AUTHORIZATION_CODE_GRANT_TYPE = "authorization_code";
    public static final String CONSENT_ID = "consentId";
    public static final String CONSENT_EXPIRY = "consentExpiry";
    public static final String CLAIMS = "claims";
    public static final String BRAND_ID="brandId";
    public static final String CHANNEL_ID="channelId";
    public static final String ID_TOKEN_PARAM = "idToken";
    public static final String CONSENT_SETUP_DATA ="consentSetupdata";
    public static final String CUSTOMER_ACCOUNT_LIST ="customerAccountList";
    public static final String CONDITION_URL_NAME ="url";
    public static final String CONDITION_KEY_NAME ="key";
    public static final String DECRYPTED_OAUTH_URL ="decryptedOauthUrl";
    public static final String CLIENT_ID_LDAP = "x-clientid";
    public static final String SECRET_LDAP = "x-secret";
    public static final String AUTHORITIES_LDAP = "x-scopes";
    public static final String TPP_BLOCK_LDAP = "x-block";
    public static final String LEGAL_ENTITY_NAME_LDAP = "o";
    public static final String REGISTERED_ID_LDAP = "cn";
    public static final String REDIRECT_URL_LDAP = "x-redirecturl";
    public static final String CORRELATION_URL_PARAM = "correlationId"; 
    public static final String IS_MEMBER_OF_LDAP = "isMemberOf";
	public static final String EXCEPTION = "exception";
	public static final String REDIRECT_URI ="redirect_uri";
	public static final String ERROR_PARAM ="error";
	public static final String REDIRECT_URI_MODEL_ATTRIBUTE="redirectUri"; 
	public static final String TPP_NAME = "tppName";
	public static final String OAUTH_URL_PARAM = "oAuthUrl";
	public static final String OAUTH_ENC_URL = "oAuthEncUrl";
	public static final String OAUTH_ACTION_URL="oAuthActionURL";
	public static final String AUTHORIZE= "authorize";
	public static final String TOKEN= "token";
	public static final String GRANT_TYPE="grant_type";
	public static final String SCOPE="scope";
	public static final String POST="post";
	public static final String REFRESH_TOKEN ="refresh_token";
	public static final String OPENBANKING_OAUTH_CONTEXT_PATH="openbanking-nOAuth";*/
}
