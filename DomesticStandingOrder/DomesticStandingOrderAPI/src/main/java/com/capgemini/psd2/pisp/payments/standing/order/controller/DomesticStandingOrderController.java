package com.capgemini.psd2.pisp.payments.standing.order.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderPOSTRequest;
import com.capgemini.psd2.pisp.domain.DStandingOrderPOST201Response;
import com.capgemini.psd2.pisp.domain.OBWriteDomesticStandingOrder1;
import com.capgemini.psd2.pisp.payments.standing.order.service.DomesticStandingOrderService;

@RestController
public class DomesticStandingOrderController {
	
	@Autowired
	private DomesticStandingOrderService service;
	
	@Value("${app.responseErrorMessageEnabled}")
	private String responseErrorMessageEnabled;

	@PostMapping(value = "/domestic-standing-orders", consumes = {
			MediaType.APPLICATION_JSON_UTF8_VALUE }, produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<DStandingOrderPOST201Response> createDomesticStandingOrderSubmissionResource(@RequestBody OBWriteDomesticStandingOrder1 request) {
		try {
			CustomDStandingOrderPOSTRequest customRequest = new CustomDStandingOrderPOSTRequest();
			customRequest.setData(request.getData());
			customRequest.setRisk(request.getRisk());
			
			return new ResponseEntity<>(service.createDomesticStandingOrderResource(customRequest), HttpStatus.CREATED);
		}catch (PSD2Exception psd2Exception) {

			if (Boolean.valueOf(responseErrorMessageEnabled))
				throw psd2Exception;

			return new ResponseEntity<>(
					HttpStatus.valueOf(Integer.parseInt(psd2Exception.getErrorInfo().getStatusCode())));

		} 
	}
	
	@GetMapping(value="/domestic-standing-orders/{DomesticStandingOrderId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<DStandingOrderPOST201Response> retrieveDomesticStandingOrderSubmissionResource(@PathVariable("DomesticStandingOrderId") String consentId){
		try {
			return new ResponseEntity<>(service.retrieveDomesticStandingOrderResource(consentId),HttpStatus.OK);
		}catch (PSD2Exception psd2Exception) {

			if (Boolean.valueOf(responseErrorMessageEnabled))
				throw psd2Exception;

			return new ResponseEntity<>(
					HttpStatus.valueOf(Integer.parseInt(psd2Exception.getErrorInfo().getStatusCode())));

		} 
	}
}
