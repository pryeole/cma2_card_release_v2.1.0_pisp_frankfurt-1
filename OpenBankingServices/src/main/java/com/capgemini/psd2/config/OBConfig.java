package com.capgemini.psd2.config;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import com.capgemini.psd2.model.Authorisation;
/**
 * Load open banking configurations from yaml file.
 */
@Configuration
@ConfigurationProperties(prefix="openbanking")
public class OBConfig {

	private String tokenUrl;
	private String apiUrl;
	private String clientAssertionType;
	private String grantType;
	private String scope;
	private String obKeyId;
	private String clientId;
	private long jwtTokenValidity;
	private String keyAlias;
	private String transportkeyAlias;
	private String responseattributes;
	private String filter;
	private Map<String,List<Authorisation>> passporting=new HashMap<>();
	public Map<String, List<Authorisation>> getPassporting() {
		return passporting;
	}
	public String getResponseattributes() {
		return responseattributes;
	}
	public void setResponseattributes(String responseattributes) {
		this.responseattributes = responseattributes;
	}
	public String getFilter() {
		return filter;
	}
	public void setFilter(String filter) {
		this.filter = filter;
	}
	public String getTokenUrl() {
		return tokenUrl;
	}
	public void setTokenUrl(String tokenUrl) {
		this.tokenUrl = tokenUrl;
	}
	public String getApiUrl() {
		return apiUrl;
	}
	public void setApiUrl(String apiUrl) {
		this.apiUrl = apiUrl;
	}
	public String getClientAssertionType() {
		return clientAssertionType;
	}
	public void setClientAssertionType(String clientAssertionType) {
		this.clientAssertionType = clientAssertionType;
	}
	public String getGrantType() {
		return grantType;
	}
	public void setGrantType(String grantType) {
		this.grantType = grantType;
	}
	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}
	public String getObKeyId() {
		return obKeyId;
	}
	public void setObKeyId(String obKeyId) {
		this.obKeyId = obKeyId;
	}
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	public long getJwtTokenValidity() {
		return jwtTokenValidity;
	}
	public void setJwtTokenValidity(long jwtTokenValidity) {
		this.jwtTokenValidity = jwtTokenValidity;
	}
	public String getKeyAlias() {
		return keyAlias;
	}
	public void setKeyAlias(String keyAlias) {
		this.keyAlias = keyAlias;
	}
	public String getTransportkeyAlias() {
		return transportkeyAlias;
	}
	public void setTransportkeyAlias(String transportkeyAlias) {
		this.transportkeyAlias = transportkeyAlias;
	}
	
}
