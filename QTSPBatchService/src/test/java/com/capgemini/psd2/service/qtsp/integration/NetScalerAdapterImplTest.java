package com.capgemini.psd2.service.qtsp.integration;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.model.qtsp.QTSPResource;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.capgemini.psd2.service.qtsp.config.NSConfig;
import com.capgemini.psd2.service.qtsp.stub.QTSPStub;
import com.capgemini.psd2.service.qtsp.utility.QTSPServiceUtility;

@RunWith(SpringJUnit4ClassRunner.class)
public class NetScalerAdapterImplTest {

	@InjectMocks
	private NetScalerAdapterImpl netScalerAdapterImpl;

	@Mock
	private NSConfig nsConfig;

	@Mock
	private RestClientSync restClientSync;

	@Mock
	private QTSPServiceUtility utility;

	private List<Map<String, List<QTSPResource>>> updatedQTSPs;
	private Map<String, String> loginResponse;
	private HttpHeaders requestHeaders;
	private Map<String, Object> unBindErrormap;
	private Map<String, Object> removeCAErrormap;
	private Map<String, Object> removeCertErrormap;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);

		Map<String, String> prefix = new HashMap<>();
		Map<String, String> userName = new HashMap<>();
		Map<String, String> password = new HashMap<>();
		Map<String, String> vserverName = new HashMap<>();
		prefix.put("ns1", "https://entryserver.private.apibank-cma2plus.in/nitro");
		userName.put("ns1", "nsroot");
		password.put("ns1", "P@ssw0rd");
		vserverName.put("BOI", "API");
		loginResponse = new HashMap<>();
		loginResponse.put("sessionid", "xyz");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(prefix.get("ns1") + "/v1/config/login");
		requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(new MediaType("application", "json"));
		when(nsConfig.getPrefix()).thenReturn(prefix);
		when(nsConfig.getUsername()).thenReturn(userName);
		when(nsConfig.getPassword()).thenReturn(password);
		when(nsConfig.getVserverName()).thenReturn(vserverName);
		when(nsConfig.getLogin()).thenReturn("/v1/config/login");
		when(nsConfig.getUploadCert()).thenReturn("/v1/config/systemfile");
		when(nsConfig.getAddCertAsCACert()).thenReturn("/v1/config/sslcertkey");
		when(nsConfig.getBindCACert()).thenReturn("/v1/config/sslvserver_sslcertkey_binding");
		when(nsConfig.getUnbindCACert()).thenReturn("/v1/config/sslvserver_sslcertkey_binding/");
		when(nsConfig.getUnbindCACertParam()).thenReturn("abc");
		when(nsConfig.getRemoveCertAsCACert()).thenReturn("/v1/config/sslcertkey/");
		when(nsConfig.getRemoveCert()).thenReturn("/v1/config/systemfile/");
		when(nsConfig.getLogout()).thenReturn("/v1/config/logout");
		when(nsConfig.getFileLocation()).thenReturn("/nsconfig/ssl/");
		when(nsConfig.getFileEncoding()).thenReturn("BASE64");
		when(nsConfig.getTimeout()).thenReturn("900");

		List<QTSPResource> newQTSPs = QTSPStub.getCerts();
		List<QTSPResource> modifiedQTSPs = QTSPStub.getCerts();
		updatedQTSPs = new ArrayList<>();
		Map<String, List<QTSPResource>> map1 = new HashMap<>();
		Map<String, List<QTSPResource>> map2 = new HashMap<>();
		map1.put("newQTSPs", newQTSPs);
		map1.put("modifiedQTSPs", modifiedQTSPs);
		updatedQTSPs.add(map1);
		updatedQTSPs.add(map2);

		unBindErrormap = new HashMap<>();
		unBindErrormap.put("errorcode", 1540);
		removeCAErrormap = new HashMap<>();
		removeCAErrormap.put("errorcode", 1540);
		removeCertErrormap = new HashMap<>();
		removeCertErrormap.put("errorcode", 3445);

	}

	@Test
	public void processCertTest() {
		when(restClientSync.callForPost(any(RequestInfo.class), any(Map.class), any(), any(HttpHeaders.class)))
				.thenReturn(loginResponse).thenReturn("SUCCESS");
		netScalerAdapterImpl.processCert(updatedQTSPs);
	}

	@Test
	public void processCertTest_loginFailure() {
		PSD2Exception e = new PSD2Exception("could not login", new ErrorInfo());
		try {
			when(restClientSync.callForPost(any(RequestInfo.class), any(Map.class), any(), any(HttpHeaders.class)))
					.thenThrow(e);
		} catch (PSD2Exception e2) {
		}
		netScalerAdapterImpl.processCert(updatedQTSPs);
	}

	@Test
	public void processCertTest_uploadFailure() {
		PSD2Exception e = new PSD2Exception(
				"{ \"errorcode\": 273, \"message\": \"Resource already exists\", \"severity\": \"ERROR\" }\r\n",
				new ErrorInfo());
		try {
			when(restClientSync.callForPost(any(RequestInfo.class), any(Map.class), any(), any(HttpHeaders.class)))
					.thenReturn(loginResponse).thenThrow(e);
		} catch (PSD2Exception e2) {
		}
		netScalerAdapterImpl.processCert(updatedQTSPs);
	}

	@Test
	public void processCertTest_uploadFailure_parseErr() {
		PSD2Exception e = new PSD2Exception("Could not upload certs", new ErrorInfo());
		try {
			when(restClientSync.callForPost(any(RequestInfo.class), any(Map.class), any(), any(HttpHeaders.class)))
					.thenReturn(loginResponse).thenThrow(e);
		} catch (PSD2Exception e2) {
		}
		netScalerAdapterImpl.processCert(updatedQTSPs);
	}

	@Test
	public void processCertTest_addCACertFailure() {
		PSD2Exception e = new PSD2Exception(
				"{ \"errorcode\": 273, \"message\": \"Resource already exists\", \"severity\": \"ERROR\" }\r\n",
				new ErrorInfo());
		when(restClientSync.callForPost(any(RequestInfo.class), any(Map.class), any(), any(HttpHeaders.class)))
				.thenReturn(loginResponse).thenReturn("SUCCESS").thenThrow(e);
		netScalerAdapterImpl.processCert(updatedQTSPs);
	}

	@Test
	public void processCertTest_addCACertFailure_parseErr() {
		PSD2Exception e = new PSD2Exception("Could not add CA certs", new ErrorInfo());
		when(restClientSync.callForPost(any(RequestInfo.class), any(Map.class), any(), any(HttpHeaders.class)))
				.thenReturn(loginResponse).thenReturn("123").thenThrow(e);
		netScalerAdapterImpl.processCert(updatedQTSPs);
	}

	@Test
	public void processCertTest_bindCert() {
		PSD2Exception e = new PSD2Exception(
				"{ \"errorcode\": 273, \"message\": \"Resource already exists\", \"severity\": \"ERROR\" }\r\n",
				new ErrorInfo());
		when(restClientSync.callForPost(any(RequestInfo.class), any(Map.class), any(), any(HttpHeaders.class)))
				.thenReturn(loginResponse).thenReturn("SUCCESS").thenReturn("SUCCESS").thenThrow(e);
		netScalerAdapterImpl.processCert(updatedQTSPs);
	}

	@Test
	public void processCertTest_bindCert_invalidErrorCode() {
		PSD2Exception e = new PSD2Exception(
				"{ \"errorcode\": 1234, \"message\": \"Some error occurred\", \"severity\": \"ERROR\" }\r\n",
				new ErrorInfo());
		when(restClientSync.callForPost(any(RequestInfo.class), any(Map.class), any(), any(HttpHeaders.class)))
				.thenReturn(loginResponse).thenReturn("SUCCESS").thenReturn("SUCCESS").thenThrow(e);
		netScalerAdapterImpl.processCert(updatedQTSPs);
	}

	@Test
	public void processCertTest_bindCert_parseErr() {
		PSD2Exception e = new PSD2Exception("Could not add CA certs", new ErrorInfo());
		when(restClientSync.callForPost(any(RequestInfo.class), any(Map.class), any(), any(HttpHeaders.class)))
				.thenReturn(loginResponse).thenReturn("SUCCESS").thenReturn("SUCCESS").thenThrow(e);
		netScalerAdapterImpl.processCert(updatedQTSPs);
	}

	@Test
	public void processCertTest_unBindCert_failure() {
		PSD2Exception e = new PSD2Exception("Could not add CA certs", new ErrorInfo());
		when(restClientSync.callForPost(any(RequestInfo.class), any(Map.class), any(), any(HttpHeaders.class)))
				.thenReturn(loginResponse);
		when(restClientSync.callForDelete(any(RequestInfo.class), any(), any(HttpHeaders.class))).thenThrow(e);
		netScalerAdapterImpl.processCert(updatedQTSPs);
	}

	@Test
	public void processCertTest_removeCA_failure() {
		when(restClientSync.callForPost(any(RequestInfo.class), any(Map.class), any(), any(HttpHeaders.class)))
				.thenReturn(loginResponse);
		when(restClientSync.callForDelete(any(RequestInfo.class), any(), any(HttpHeaders.class)))
				.thenReturn(unBindErrormap).thenReturn(removeCAErrormap);
		netScalerAdapterImpl.processCert(updatedQTSPs);
	}

	@Test
	public void processCertTest_removeCA_failure_exception() {
		PSD2Exception e = new PSD2Exception(
				"{ \"errorcode\": 1540, \"message\": \"Resource already exists\", \"severity\": \"ERROR\" }\r\n",
				new ErrorInfo());
		when(restClientSync.callForPost(any(RequestInfo.class), any(Map.class), any(), any(HttpHeaders.class)))
				.thenReturn(loginResponse);
		when(restClientSync.callForDelete(any(RequestInfo.class), any(), any(HttpHeaders.class)))
				.thenReturn(unBindErrormap).thenThrow(e);
		netScalerAdapterImpl.processCert(updatedQTSPs);
	}

	@Test
	public void processCertTest_removeCert_failure() {

		when(restClientSync.callForPost(any(RequestInfo.class), any(Map.class), any(), any(HttpHeaders.class)))
				.thenReturn(loginResponse);
		when(restClientSync.callForDelete(any(RequestInfo.class), any(), any(HttpHeaders.class)))
				.thenReturn(unBindErrormap).thenReturn(unBindErrormap).thenReturn(removeCertErrormap);
		netScalerAdapterImpl.processCert(updatedQTSPs);
	}

	@Test
	public void processCertTest_removeCert_failure_null() {

		when(restClientSync.callForPost(any(RequestInfo.class), any(Map.class), any(), any(HttpHeaders.class)))
				.thenReturn(loginResponse);
		when(restClientSync.callForDelete(any(RequestInfo.class), any(), any(HttpHeaders.class)))
				.thenReturn(unBindErrormap).thenReturn(unBindErrormap).thenReturn(null);
		netScalerAdapterImpl.processCert(updatedQTSPs);
	}

	@Test
	public void processCertTest_removeCert_failure_exception() {
		PSD2Exception e = new PSD2Exception(
				"{ \"errorcode\": 3445, \"message\": \"Resource already exists\", \"severity\": \"ERROR\" }\r\n",
				new ErrorInfo());
		when(restClientSync.callForPost(any(RequestInfo.class), any(Map.class), any(), any(HttpHeaders.class)))
				.thenReturn(loginResponse);
		when(restClientSync.callForDelete(any(RequestInfo.class), any(), any(HttpHeaders.class)))
				.thenReturn(unBindErrormap).thenReturn(unBindErrormap).thenThrow(e);
		netScalerAdapterImpl.processCert(updatedQTSPs);
	}
}
