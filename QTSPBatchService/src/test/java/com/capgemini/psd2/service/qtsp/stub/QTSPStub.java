package com.capgemini.psd2.service.qtsp.stub;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.capgemini.psd2.model.qtsp.Meta;
import com.capgemini.psd2.model.qtsp.QTSPResource;
import com.capgemini.psd2.model.qtsp.QTSPServiceExtension;

@Service
public class QTSPStub {

	public static List<QTSPResource> getCerts() {

		List<QTSPResource> qtsps = new ArrayList<>();

		QTSPResource qtsp1 = new QTSPResource();
		qtsp1.setCertificateFingerprint("f18bd1ba06c980a0b598056ecc191fed52ebdc25");
		qtsp1.setServiceCountry("PT");
		qtsp1.setCreateTimestamp("2019-01-22T19:57:29.765Z");
		qtsp1.setModifyTimestamp("2019-01-22T19:57:29.765Z");
		List<QTSPServiceExtension> qtspServiceExtensions1 = new ArrayList<>();
		QTSPServiceExtension qtspServiceExtension1 = new QTSPServiceExtension();
		qtspServiceExtension1.setCritical(true);
		qtspServiceExtension1.setAdditionalInfoLang("en");
		qtspServiceExtension1.setAdditionalInfoURI("http://uri.etsi.org/TrstSvc/TrustedList/SvcInfoExt/ForeSignatures");
		qtspServiceExtensions1.add(qtspServiceExtension1);
		qtsp1.setServiceExtensions(qtspServiceExtensions1);
		qtsp1.setServiceLanguage("en");
		qtsp1.setServiceName("ECCE 001 (CA-Qualified certificates)");
		qtsp1.setServiceStatus("http://uri.etsi.org/TrstSvc/TrustedList/Svcstatus/granted");
		qtsp1.setServiceType("http://uri.etsi.org/TrstSvc/Svctype/CA/QC");
		qtsp1.setStatusStartingTime("2016-06-30T22:00:00Z");
		qtsp1.setX509Certificate("MIIFQDCCAyigAwIBAgIQW+ApHj8MkelVitA9MDf1STANBgkqhkiG9w0BAQsFADAzMQswCQYDVQ\r\n"
				+ "QGEwJQVDENMAsGA1UECgwEU0NFRTEVMBMGA1UEAwwMRUNSYWl6RXN0YWRvMB4XDTE1MDYyNDE1NDM1N1oXDTI3MDYyNDE\r\n"
				+ "1NDM1N1owQjELMAkGA1UEBhMCUFQxDTALBgNVBAoMBFNDRUUxETAPBgNVBAsMCEVDRXN0YWRvMREwDwYDVQQDDAhFQ0NF\r\n"
				+ "IDAwMTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMD1nmAQ6iiApqcE9KCvKGTn/3P3WNbrmGguwoUZgDPlN\r\n"
				+ "Seopd1ifACygPs0hXSCVbjIEVacsJaQ4jUPAPxGs6/XO2SF49K1M+F02n5QVBDOm7RInu4c5uz6uUp/z0z/51OxBvJT0K\r\n"
				+ "Cj2aGnLB2vo2atLcyP4pgqeb6lPT3HcTPNTnTnYytAPM2Kj82mJWfMNMop/3qwlaz2SYjvG5ZJNNewGLqOqC++4LYkmNV\r\n"
				+ "YaU/r5hX/8p4dlQg+zEP1bQeol8rUQbbtmJ7ZRkD5H01pUngL+1rPE+0V+/qz787ib6jVA513jesBbSxAR9QjMuhdab46\r\n"
				+ "q8FBdMqyMPTwqrMYjfcCAwEAAaOCAT8wggE7MA8GA1UdEwEB/wQFMAMBAf8wHQYDVR0OBBYEFOMa2JoMNlrUDiOsDphvg\r\n"
				+ "VKeLl0FMB8GA1UdIwQYMBaAFHF/Nd71d3FtHRKc4ZCkuvCpg4+AMA4GA1UdDwEB/wQEAwIBBjA7BgNVHSAENDAyMDAGBF\r\n"
				+ "UdIAAwKDAmBggrBgEFBQcCARYaaHR0cDovL3d3dy5lY2VlLmdvdi5wdC9kcGMwZAYIKwYBBQUHAQEEWDBWMCMGCCsGAQU\r\n"
				+ "FBzABhhdodHRwOi8vb2NzcC5lY2VlLmdvdi5wdDAvBggrBgEFBQcwAoYjaHR0cDovL3RydXN0LmVjZWUuZ292LnB0L2Vj\r\n"
				+ "cmFpei5jcnQwNQYDVR0fBC4wLDAqoCigJoYkaHR0cDovL2NybHMuZWNlZS5nb3YucHQvY3Jscy9BUkwuY3JsMA0GCSqGS\r\n"
				+ "Ib3DQEBCwUAA4ICAQDRjGHFQLgpVdEaDuyC7VwwzRBPqMhhY5mHs84tZL6d1BAUKW6qzLrEB370xEnBgWdxE5dU9jKSZx\r\n"
				+ "FeZau9Mkt5Yau3fi2jrYwhacFa+jzeBxthAfVGGD/x9zQU8wCfocrbC7Sfd6t9z7+0drAyAg0A/RwdBjMsrAhHpxxMf5m\r\n"
				+ "+5HxlaxY2y5C85ZG0Ru/S4paBDHklK1PNQdPyxrU8QgH/UD6sIz4BgqaSgFijyoaExquCmciX1TSCmxqDHc+c4O6X3iQO\r\n"
				+ "B8ZAx2RYsENKJG3DM+thp1yo8mZxEsan1yJcQY4OpKHNvKAq0g9UNyu93ZL/Fn9okVQuFNXp9L9APZsL8p70hAFt2aoTh\r\n"
				+ "QATaQSRjtb99beGbm5DYvhWh7oli4QnvqmgA/YUrRBz3w0WGCqz+lO0VNXKCKJLPlWwlmeil4YXNbK8Nr+XOvb/XT/yxV\r\n"
				+ "09iZBV+hXK/MwQ6dZLbEVwn84WPKiuM3nyQzuEYJHD1iCLy8RM9GM3lNmTL/Wt6mfX66o4+OsR6ZG2PNZxPaPUetZAgX/\r\n"
				+ "VCM1T1liL+H7z2aQ7OIjZ1BMo2F27x/QpZSBMIYITZIqxRm0kTEyo90wi14VXlp6scGLcTj6fIE9SD4zOaXKkFg8gc5uz\r\n"
				+ "fUtUg2u1fVvqdkywbF1PHgMud9e7scWuX/xLeKCvuTp+Arb78A==");
		qtsp1.setX509SKI("BBYEFOMa2JoMNlrUDiOsDphvgVKeLl0F");
		qtsp1.setX509SubjectName("CN=ECCE 001, \r\n" + " OU=ECEstado, \r\n" + " O=SCEE, \r\n" + " C=PT");
		qtsp1.setId("f18bd1ba06c980a0b598056ecc191fed52ebdc25");

		Meta meta1 = new Meta();
		meta1.setResourceType("OBQualifiedTrustServiceProviders");
		meta1.setLocation(
				"https://matls-api.openbankingtest.org.uk/scim/v2/OBQualifiedTrustServiceProviders/f18bd1ba06c980a0b598056ecc191fed52ebdc25");
		qtsp1.setMeta(meta1);
		List<String> schemas1 = new ArrayList<>();
		schemas1.add("urn:openbanking:qualifiedtrustserviceprovider:1.0");
		qtsp1.setSchemas(schemas1);
		qtsps.add(qtsp1);

		QTSPResource qtsp2 = new QTSPResource();
		qtsp2.setCertificateFingerprint("dd4d93eaf1dd91ed49dc71e81fd4fcb77a3a13b6");
		qtsp2.setServiceCountry("PT");
		qtsp2.setCreateTimestamp("2019-01-22T19:57:31.592Z");
		qtsp2.setModifyTimestamp("2019-01-22T19:57:31.592Z");
		List<QTSPServiceExtension> qtspServiceExtensions2 = new ArrayList<>();
		QTSPServiceExtension qtspServiceExtension2 = new QTSPServiceExtension();
		qtspServiceExtension2.setCritical(true);
		qtspServiceExtension2.setAdditionalInfoLang("en");
		qtspServiceExtension2.setAdditionalInfoURI("http://uri.etsi.org/TrstSvc/TrustedList/SvcInfoExt/ForeSignatures");
		qtspServiceExtensions1.add(qtspServiceExtension2);
		qtsp2.setServiceExtensions(qtspServiceExtensions2);
		qtsp2.setServiceLanguage("en");
		qtsp2.setServiceName("ECM Justica (CA-Qualified Certificates)");
		qtsp2.setServiceStatus("http://uri.etsi.org/TrstSvc/TrustedList/Svcstatus/granted");
		qtsp2.setServiceType("http://uri.etsi.org/TrstSvc/Svctype/CA/QC");
		qtsp2.setStatusStartingTime("2018-07-10T23:00:00Z");
		qtsp2.setX509Certificate("MIIGxTCCBK2gAwIBAgIIQ24pQyJ7ahgwDQYJKoZIhvcNAQELBQAwgYUxCzAJBgNVB\r\n"
				+ "AYTAlBUMUIwQAYDVQQKDDlNVUxUSUNFUlQgLSBTZXJ2acOnb3MgZGUgQ2VydGlmaWNhw6fDo28gRWxlY3Ryw\r\n"
				+ "7NuaWNhIFMuQS4xMjAwBgNVBAMMKU1VTFRJQ0VSVCBSb290IENlcnRpZmljYXRpb24gQXV0aG9yaXR5IDAxM\r\n"
				+ "B4XDTE4MDcxMDEwMzAzOVoXDTE5MDEwNzEwMzAzOVowQTELMAkGA1UEBhMCUFQxDTALBgNVBAoMBFNDRUUxE\r\n"
				+ "TAPBgNVBAsMCEVDRXN0YWRvMRAwDgYDVQQDDAdKdXN0aWNhMIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICC\r\n"
				+ "gKCAgEAr5bP3YChchO77DDEuF72kLUGUU4QUiQnjnjaFadAWtGVDEuZOE5rrO19wgz497tc4DgQX8a9tPR6a\r\n"
				+ "JJm8mgfoZcq4jEhHNofQPrLl3p8wk+3ISizzHZpDop02nC9xFqAEx4bP4bs2DzrwEtgx9im7vMMoE9uxFNRL\r\n"
				+ "3Lbry/QUoKOndZvwIUzy2E43pL1/6DUj9OxQH5oKU7nd/Q6v+2idF+fy+ubhS3GePFv8TKLmrhKUi6Xdrefv\r\n"
				+ "y5Y9NPLueONREgVlHo1GaWICngqsXtBYI+2ycApT2jtV87JqmWbEZf1zIanxGecANdwyqmCScRdLimzUES14\r\n"
				+ "ERfQkaW4M2/sbxroj6gcAwBy/FQuPLh5JYxA/81sOOtmN/ldQe7ZxINq4OXBg32kyXTxix9Vtxyt2ivp9gDt\r\n"
				+ "VNizw3LipbifVyR0qk+REpMzw8D4x2mr56vBw/wgPuty9gHPg3S2KyBGL957Ka+uZ8+JV+S3IkOuWjHZWaI6\r\n"
				+ "CcDQnRPpNsZfVC/s6HFnzjX2/dJx05vfPRxOuGmwP+mtEUKaAh5bCNW1oOu50rc5d+yRtSjeuBLQjCz50Ksh\r\n"
				+ "9gBlSZZnp6kmToutL6Oa/OGEsbnxogOTUUjGVHueOWzoooQ9hSnnJQQil5DxwsALhC7otiQYklBGZi9wzuPb\r\n"
				+ "9po+Esg0B+ovkQdoDUCAwEAAaOCAXowggF2MDoGCCsGAQUFBwEBBC4wLDAqBggrBgEFBQcwAYYeaHR0cDovL\r\n"
				+ "29jc3AubXVsdGljZXJ0LmNvbS9vY3NwMB0GA1UdDgQWBBS9qDINK0PVu6Vz/VjtX6kK3eawCzASBgNVHRMBA\r\n"
				+ "f8ECDAGAQH/AgEAMB8GA1UdIwQYMBaAFNU5HJxbbwSqopVM7yDdKXSkxUVxMIGQBgNVHSAEgYgwgYUwPAYNK\r\n"
				+ "wYBBAGBw24BAQEABzArMCkGCCsGAQUFBwIBFh1odHRwOi8vcGtpcm9vdC5tdWx0aWNlcnQuY29tLzA9Bg4rB\r\n"
				+ "gEEAYHDbgEBAQABAjArMCkGCCsGAQUFBwIBFh1odHRwOi8vcGtpcm9vdC5tdWx0aWNlcnQuY29tLzAGBgRVH\r\n"
				+ "SAAMEEGA1UdHwQ6MDgwNqA0oDKGMGh0dHA6Ly9wa2lyb290Lm11bHRpY2VydC5jb20vY3JsL3Jvb3RfbWNfY\r\n"
				+ "3JsLmNybDAOBgNVHQ8BAf8EBAMCAQYwDQYJKoZIhvcNAQELBQADggIBAASBEZIhVfrayCJ849aoWuAKGfmf8\r\n"
				+ "RBRRYfKT0EqEyDAgHSXpwZojtV+sWLYAvJawj4qsoozQMCFf8TbrxoBRr6VnS8dxbLRc+YuekDNgoZtnnRBX\r\n"
				+ "ewFfgdcSptLy2pkWqId/Vt9UGxGa83TiWJMvyRJmUbUfPxQqgViC3v3Sp43xfX2cLL7MgDsePquEXjtkn6zy\r\n"
				+ "l/llBuVW44F1kTdwEZOmWznwzLe0zGNbdlI+sngOTGbOabJLyshr/p+MJrQn2Y9hc4WeReIofSbbYpDwvGRT\r\n"
				+ "STXpNS6Da/fo4uH089OV9OtwETpULnthvHIB1D3e5OEfSKFyUpOf9XejizWlR3c1nDxMWzPo3Zf9w2v1B5b7\r\n"
				+ "KNJ6cFb9qojOZhoypPUdN6pSFvfM3FUow89ppPQqsSv1iT4smAZsiCwRM4sGqcw7jlp5r28XwfrAQy1b2P36\r\n"
				+ "b6yk0N68GF4pDQRXVJebtghRWAiI2qJbWxN1PvGP+uMZUvAWbW3wJfI0NkGNFCYXCixfENaTtacqt2YmfEJk\r\n"
				+ "MtxdawVcDIWFDWxifNxgRmr6+8WTMAxnmzzuxeEQ4hCms0lI8z4J/G5v48fOlzg0Pdp62Rqbeu09Ux3Q9SC8\r\n"
				+ "RCXd9L/1zC8kVH+4EKXGNTW20EC/izV3tZNJKPRlSzA3ZwuY3xbZfVYt7uUdXXLAzmZ");
		qtsp2.setX509SKI("BBYEFL2oMg0rQ9W7pXP9WO1fqQrd5rAL");
		qtsp2.setX509SubjectName("CN=Justica, \r\n" + " OU=ECEstado, \r\n" + " O=SCEE, \r\n" + " C=PT");
		qtsp2.setId("dd4d93eaf1dd91ed49dc71e81fd4fcb77a3a13b6");

		Meta meta2 = new Meta();
		meta2.setResourceType("OBQualifiedTrustServiceProviders");
		meta2.setLocation(
				"https://matls-api.openbankingtest.org.uk/scim/v2/OBQualifiedTrustServiceProviders/dd4d93eaf1dd91ed49dc71e81fd4fcb77a3a13b6");
		qtsp2.setMeta(meta2);
		List<String> schemas2 = new ArrayList<>();
		schemas2.add("urn:openbanking:qualifiedtrustserviceprovider:1.0");
		qtsp2.setSchemas(schemas2);
		qtsps.add(qtsp2);

		return qtsps;
	}

}