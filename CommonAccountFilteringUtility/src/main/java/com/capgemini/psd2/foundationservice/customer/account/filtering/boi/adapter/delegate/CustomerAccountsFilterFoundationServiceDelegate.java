package com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.delegate;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.adapter.utility.AdapterUtility;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion.CustomerAccountsFilter;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.AccountEntitlements;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PartyEntitlements;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.AccountEntitlements2;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.utilities.NullCheckUtils;

/**
 * The class CustomerAccountProfileFoundationServiceDelegate
 *
 */
@Component
public class CustomerAccountsFilterFoundationServiceDelegate {

	/** To create Base URL basis on Channel Id */
	@Autowired
	private AdapterUtility adapterUtility;

	/** The user in req header. */
	@Value("${foundationService.userInReqHeader:#{X-BOI-USER}}")
	private String userInReqHeader;

	/** The channel in req header. */
	@Value("${foundationService.channelInReqHeader:#{X-BOI-CHANNEL}}")
	private String channelInReqHeader;

	/** The workstation in req header. */
	@Value("${foundationService.platformInReqHeader:#{X-BOI-PLATFORM}}")
	private String platformInReqHeader;

	/** The correlation req header. */
	@Value("${foundationService.correlationReqHeader:#{X-BOI-WORKSTATION}}")
	private String correlationReqHeader;
	
	/** The correlation-ID req header. */
	@Value("${foundationService.correlationMuleReqHeader:#{X-API-CORRELATION-ID}}")
	private String correlationMuleReqHeader;
	
	/** The correlation-ID req header. */
	@Value("${foundationService.sourceSystemReqHeader:#{X-API-SOURCE-SYSTEM}}")
	private String sourceSystemReqHeader;
	
	/** The correlation-ID req header. */
	@Value("${foundationService.sourceUserReqHeader:#{X-API-SOURCE-USER}}")
	private String sourceUserReqHeader;
	
	@Value("${foundationService.transactioReqHeader:#{X-API-TRANSACTION-ID}}")
	private String transactioReqHeader;
	
	@Value("${foundationService.partySourceNumber:#{X-API-PARTY-SOURCE-ID-NUMBER}}")
	private String partySourceNumber;

	/** The platform. */
	@Value("${app.platform}")
	private String platform;
	
	@Value("${foundationService.version}")
	private String version;

	/* Customer Account Profile Foundation Service Transformer */
	@Autowired
	private CustomerAccountsFilter CustAccntProfileSerTrans;

	/* This method is giving URL */
	public String getFoundationServiceURLAISP(Map<String, String> params, String userId) {
		String channelCode = params.get(PSD2Constants.CHANNEL_ID).toUpperCase();
		String baseURL = adapterUtility.retriveCustProfileFoundationServiceURLAISP(channelCode);
		if (NullCheckUtils.isNullOrEmpty(baseURL)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}
		return baseURL + "/" + version + "/channels/" + channelCode + "/digitalusers/" + userId + "/digital-user-profile";
	}

	/* This method is giving URL */
	public String getFoundationServiceURLPISP(Map<String, String> params, String userId) {
		String channelCode = params.get(PSD2Constants.CHANNEL_ID).toUpperCase();
		String baseURL = adapterUtility.retriveCustProfileFoundationServiceURLPISP(channelCode);
		if (NullCheckUtils.isNullOrEmpty(baseURL)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}
		return baseURL + "/" + version + "/party-positions/profile-accounts-balances/channels/" + channelCode;
	}
	
	public String getFoundationServiceURLforPISP(String channelId, String accountNSC, String accountNumber) {
		if (NullCheckUtils.isNullOrEmpty(channelId)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_CHANNEL_ID_IN_REQUEST);
		}
		if (NullCheckUtils.isNullOrEmpty(accountNSC)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_NSC_IN_REQUEST);
		}
		if (NullCheckUtils.isNullOrEmpty(accountNumber)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}
		String baseURL = adapterUtility.retriveFoundationServiceURL(channelId);
		if (NullCheckUtils.isNullOrEmpty(baseURL)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}
		return baseURL + "/" + accountNSC + "/" + accountNumber ;
	}

	/*
	 * This method is calling customer account profile transformer to convert
	 * object
	 */
	public List<AccountEntitlements> getFilteredAccounts(List<AccountEntitlements> accntEntitlments, PartyEntitlements partyEntitlements, Map<String, String> params) {
		return CustAccntProfileSerTrans.getFilteredAccounts(accntEntitlments, partyEntitlements, params);
	}
	
	public List<AccountEntitlements2> getFilteredAccountsPISP(List<AccountEntitlements2> accntEntitlments, com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.PartyEntitlements partyEntitlements, Map<String, String> params) {
		return CustAccntProfileSerTrans.getFilteredAccountsPISP(accntEntitlments, partyEntitlements, params);
	}

	/**
	 * Creates the request headers.
	 *
	 * @param requestInfo
	 *            the request info
	 * @param accountMapping
	 *            the account mapping
	 * @return the http headers
	 */
	public HttpHeaders createRequestHeadersAISP(Map<String, String> params) {
		if (NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.USER_ID))
				|| NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.CORRELATION_ID))) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}
		String channelCode = params.get(PSD2Constants.CHANNEL_ID).toLowerCase();
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add(sourceUserReqHeader, params.get(PSD2Constants.USER_ID));
		httpHeaders.add(sourceSystemReqHeader, platform);
		httpHeaders.add(correlationMuleReqHeader, "");
		httpHeaders.add(transactioReqHeader, params.get(PSD2Constants.CORRELATION_ID));
		if(channelCode.equalsIgnoreCase("bol"))
		httpHeaders.add(partySourceNumber, params.get(PSD2Constants.PARTY_IDENTIFIER));
		return httpHeaders;
	}

	public HttpHeaders createRequestHeadersPISP(Map<String, String> params) {
		if (NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.USER_ID))
				|| NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.CORRELATION_ID))) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}
		String channelCode = params.get(PSD2Constants.CHANNEL_ID).toLowerCase();
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add(sourceUserReqHeader, params.get(PSD2Constants.USER_ID));
		httpHeaders.add(sourceSystemReqHeader, platform);
		httpHeaders.add(correlationMuleReqHeader, "");
		httpHeaders.add(transactioReqHeader, params.get(PSD2Constants.CORRELATION_ID));
		if(channelCode.equalsIgnoreCase("bol"))
		httpHeaders.add(partySourceNumber, params.get(PSD2Constants.PARTY_IDENTIFIER));
		return httpHeaders;
	}
	
	public HttpHeaders createRequestHeadersForPISP(Map<String, String> params) {
		if (NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.USER_IN_REQ_HEADER))
				|| NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER))
				|| NullCheckUtils.isNullOrEmpty(platform)
				|| NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.CORRELATION_REQ_HEADER))) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add(userInReqHeader, params.get(PSD2Constants.USER_IN_REQ_HEADER));
		httpHeaders.add(channelInReqHeader, params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER));
		httpHeaders.add(platformInReqHeader, platform);
		httpHeaders.add(correlationReqHeader, params.get(PSD2Constants.CORRELATION_REQ_HEADER));
		return httpHeaders;
	}
}