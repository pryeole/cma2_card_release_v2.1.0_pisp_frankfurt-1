package com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion;

import java.util.List;
import java.util.Map;

import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.AccountEntitlements;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PartyEntitlements;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.AccountEntitlements2;

public interface FilterationChain {
	public abstract void setNext(FilterationChain nextInChain);
	public abstract List<AccountEntitlements> process(List<AccountEntitlements> accounts, PartyEntitlements partyEntitlements,String consentFlowType, Map<String, Map<String,List<String>>> accountFiltering);	
	public abstract List<AccountEntitlements2> processPISP(List<AccountEntitlements2> accounts, com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.PartyEntitlements partyEntitlements,String consentFlowType, Map<String, Map<String,List<String>>> accountFiltering);

}
