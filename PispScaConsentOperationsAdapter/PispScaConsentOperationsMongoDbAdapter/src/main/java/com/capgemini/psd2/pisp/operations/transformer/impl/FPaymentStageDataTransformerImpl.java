package com.capgemini.psd2.pisp.operations.transformer.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Conditional;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.conditions.MongoDbMockCondition;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus2Code;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.file.payment.setup.mongo.db.adapter.repository.FPaymentConsentsDataRepository;
import com.capgemini.psd2.pisp.operations.transformer.PaymentStageDataTransformer;
import com.capgemini.psd2.pisp.stage.domain.AmountDetails;
import com.capgemini.psd2.pisp.stage.domain.ChargeDetails;
import com.capgemini.psd2.pisp.stage.domain.CustomConsentAppViewData;
import com.capgemini.psd2.pisp.stage.domain.CustomDebtorDetails;
import com.capgemini.psd2.pisp.stage.domain.CustomFileData;
import com.capgemini.psd2.pisp.stage.domain.CustomFraudSystemPaymentData;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageUpdateData;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Conditional(MongoDbMockCondition.class)
@Component("filePaymentStageDataTransformer")
public class FPaymentStageDataTransformerImpl
		implements PaymentStageDataTransformer<CustomFilePaymentConsentsPOSTResponse> {

	@Autowired
	private FPaymentConsentsDataRepository fPaymentConsentsDataRepository;

	@Override
	public CustomConsentAppViewData transformDStageToConsentAppViewData(
			CustomFilePaymentConsentsPOSTResponse fileConsentResponse) {

		CustomConsentAppViewData response = new CustomConsentAppViewData();

		// Required for File Payment Field Population
		CustomFileData customFilePayField = new CustomFileData();

		/* Payment Type needed on consent page */
		response.setPaymentType(PaymentTypeEnum.FILE_PAY.getPaymentType());

		/* Debtor details needed on consent page */
		CustomDebtorDetails debtorDetails = null;
		if (!NullCheckUtils.isNullOrEmpty(fileConsentResponse.getData().getInitiation().getDebtorAccount())) {
			debtorDetails = new CustomDebtorDetails();
			debtorDetails.setIdentification(
					fileConsentResponse.getData().getInitiation().getDebtorAccount().getIdentification());
			debtorDetails.setName(fileConsentResponse.getData().getInitiation().getDebtorAccount().getName());
			String stagedDebtorSchemeName = fileConsentResponse.getData().getInitiation().getDebtorAccount()
					.getSchemeName();

			// Fix Scheme name validation failed. In MongoDB producing
			// exception: can't have. In field names [UK.OBIE.IBAN] while
			// creating test data. Hence, in mock data, we used "_" instead of
			// "." character
			// Replacing "_" to "." to convert valid Scheme Name into mongo db
			// adapter only. there is no impact on CMA api flow. Its specifc to
			// Sandbox functionality
			debtorDetails.setSchemeName(stagedDebtorSchemeName.replace("_", "."));
			debtorDetails.setSecondaryIdentification(
					fileConsentResponse.getData().getInitiation().getDebtorAccount().getSecondaryIdentification());
		}

		/* Charge details needed on consent page */
		ChargeDetails chargeDetails = new ChargeDetails();
		chargeDetails.setChargesList(fileConsentResponse.getData().getCharges());

		/* Amount details needed on consent page */
		AmountDetails amountDetails = new AmountDetails();
		if (!NullCheckUtils.isNullOrEmpty(fileConsentResponse.getData().getInitiation().getControlSum())) {
			// TODO change the amount and Currency as per the value from FS
			amountDetails.setAmount(fileConsentResponse.getData().getInitiation().getControlSum().toString());
			amountDetails.setCurrency("INR");
		}

		customFilePayField
				.setNumberOfTransaction(fileConsentResponse.getData().getInitiation().getNumberOfTransactions());
		customFilePayField.setFileReference(fileConsentResponse.getData().getInitiation().getFileReference());
		customFilePayField.setLocalInstrument(fileConsentResponse.getData().getInitiation().getLocalInstrument());
		// TODO change as per the FS
		customFilePayField.setNumberOfPayees("1");
		customFilePayField.setRequestedExecutionDateTime(
				fileConsentResponse.getData().getInitiation().getRequestedExecutionDateTime());

		response.setAmountDetails(amountDetails);
		response.setChargeDetails(chargeDetails);
		response.setDebtorDetails(debtorDetails);
		response.setCustomeFileData(customFilePayField);
		return response;

	}

	@Override
	public CustomFraudSystemPaymentData transformDStageToFraudData(
			CustomFilePaymentConsentsPOSTResponse stageResponse) {
		CustomFraudSystemPaymentData fraudSystemData = new CustomFraudSystemPaymentData();
		fraudSystemData.setPaymentType(PaymentTypeEnum.FILE_PAY.getPaymentType());
		// TODO change the amount
		fraudSystemData.setTransferAmount(stageResponse.getData().getInitiation().getControlSum().toString());
		fraudSystemData.setTransferCurrency("INR");
		if (!NullCheckUtils.isNullOrEmpty(stageResponse.getData().getInitiation().getRemittanceInformation()))
			fraudSystemData
					.setTransferMemo(stageResponse.getData().getInitiation().getRemittanceInformation().getReference());
		fraudSystemData.setTransferTime(stageResponse.getData().getCreationDateTime());

		return fraudSystemData;
	}

	@Override
	public void updateStagedPaymentConsents(CustomPaymentStageIdentifiers customStageIdentifiers,
			CustomPaymentStageUpdateData stageUpdateData, Map<String, String> params) {

		CustomFilePaymentConsentsPOSTResponse updatedStagedResource = fPaymentConsentsDataRepository
				.findOneByDataConsentId(customStageIdentifiers.getPaymentConsentId());
		if (stageUpdateData.getDebtorDetailsUpdated() == Boolean.TRUE) {
			updatedStagedResource.getData().getInitiation().setDebtorAccount(stageUpdateData.getDebtorDetails());
		}
		if (stageUpdateData.getFraudScoreUpdated() == Boolean.TRUE) {
			updatedStagedResource.setFraudScore(stageUpdateData.getFraudScore());
		}
		if (stageUpdateData.getSetupStatusUpdated() == Boolean.TRUE) {
			updatedStagedResource.getData()
					.setStatus(OBExternalConsentStatus2Code.fromValue(stageUpdateData.getSetupStatus()));
			updatedStagedResource.getData().setStatusUpdateDateTime(stageUpdateData.getSetupStatusUpdateDateTime());
		}
		try {
			fPaymentConsentsDataRepository.save(updatedStagedResource);
		} catch (Exception exp) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.PISP_PAYMENT_SETUP_CREATION_FAILED));
		}

	}

}
