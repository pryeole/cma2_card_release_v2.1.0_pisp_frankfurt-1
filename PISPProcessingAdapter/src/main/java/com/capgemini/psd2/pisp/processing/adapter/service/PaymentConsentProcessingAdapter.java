package com.capgemini.psd2.pisp.processing.adapter.service;

import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPlatformDetails;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentResponseInfo;
import com.capgemini.psd2.pisp.domain.PaymentRetrieveGetRequest;

public interface PaymentConsentProcessingAdapter<T,U> {

	public PaymentConsentsPlatformResource preConsentProcessFlows(T request,CustomPaymentSetupPlatformDetails customPaymentSetupPlatformDetails);
	
	public U postConsentProcessFlows(PaymentResponseInfo stageInfo, U stageResponse, PaymentConsentsPlatformResource paymentSetupPlatformResource, String methodType);
	
	public PaymentConsentsPlatformResource preConsentProcessGETFlow(PaymentRetrieveGetRequest paymentRetrieveRequest);

}
