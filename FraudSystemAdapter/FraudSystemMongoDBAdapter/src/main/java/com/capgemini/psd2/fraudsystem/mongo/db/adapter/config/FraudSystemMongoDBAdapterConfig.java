package com.capgemini.psd2.fraudsystem.mongo.db.adapter.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.capgemini.psd2.fraudsystem.adapter.FraudSystemAdapter;
import com.capgemini.psd2.fraudsystem.mongo.db.adapter.impl.FraudSystemMongoDBAdapterImpl;

@Configuration
public class FraudSystemMongoDBAdapterConfig {

	@Bean(name="fraudSystemMongoDbAdapter")
	public FraudSystemAdapter fraudSystemMongoDBAdapter(){
		return new FraudSystemMongoDBAdapterImpl();
	}
}
