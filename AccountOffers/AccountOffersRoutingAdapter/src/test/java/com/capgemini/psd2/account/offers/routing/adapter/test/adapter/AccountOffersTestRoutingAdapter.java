package com.capgemini.psd2.account.offers.routing.adapter.test.adapter;

import java.util.Map;

import com.capgemini.psd2.account.offers.routing.adapter.test.mock.data.AccountOffersRoutingAdapterTestMockData;
import com.capgemini.psd2.aisp.adapter.AccountOffersAdapter;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountOffersResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;

public class AccountOffersTestRoutingAdapter implements AccountOffersAdapter {

	@Override
	public PlatformAccountOffersResponse retrieveAccountOffers(AccountMapping accountMapping,
			Map<String, String> params) {
		// TODO Auto-generated method stub
		return AccountOffersRoutingAdapterTestMockData.getMockOffersGETResponse();
	}

}
