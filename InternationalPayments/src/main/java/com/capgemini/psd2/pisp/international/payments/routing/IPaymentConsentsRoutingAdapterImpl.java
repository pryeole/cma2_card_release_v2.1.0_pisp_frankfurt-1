package com.capgemini.psd2.pisp.international.payments.routing;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.operations.adapter.InternationalPaymentStagingAdapter;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component("iPaymentConsentsStagingRoutingAdapter")
public class IPaymentConsentsRoutingAdapterImpl implements InternationalPaymentStagingAdapter, ApplicationContextAware {

	private InternationalPaymentStagingAdapter beanInstance;

	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Value("${app.paymentSetupStagingAdapter}")
	private String beanName;

	/** The application context. */
	private ApplicationContext applicationContext;

	public InternationalPaymentStagingAdapter getRoutingInstance(String adapterName) {
		if (beanInstance == null)
			beanInstance = (InternationalPaymentStagingAdapter) applicationContext.getBean(adapterName);
		return beanInstance;
	}

	@Override
	public void setApplicationContext(ApplicationContext context) {
		this.applicationContext = context;
	}

	@Override
	public CustomIPaymentConsentsPOSTResponse processInternationalPaymentConsents(
			CustomIPaymentConsentsPOSTRequest internationalPaymentConsentsRequest,
			CustomPaymentStageIdentifiers customStageIdentifiers, Map<String, String> params,
			OBExternalConsentStatus1Code successStatus, OBExternalConsentStatus1Code failureStatus) {
		return getRoutingInstance(beanName).processInternationalPaymentConsents(internationalPaymentConsentsRequest,
				customStageIdentifiers, getHeaderParams(params), successStatus, failureStatus);
	}

	@Override
	public CustomIPaymentConsentsPOSTResponse retrieveStagedInternationalPaymentConsents(
			CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
		return getRoutingInstance(beanName).retrieveStagedInternationalPaymentConsents(customPaymentStageIdentifiers,
				getHeaderParams(params));

	}

	private Map<String, String> getHeaderParams(Map<String, String> params) {
		if (NullCheckUtils.isNullOrEmpty(params))
			params = new HashMap<>();

		if (null != reqHeaderAtrributes.getToken() && null != reqHeaderAtrributes.getToken().getSeviceParams()) {
			params.put(PSD2Constants.CHANNEL_IN_REQ_HEADER,
					reqHeaderAtrributes.getToken().getSeviceParams().get(PSD2Constants.CHANNEL_NAME));
		}
		params.put(PSD2Constants.USER_IN_REQ_HEADER, reqHeaderAtrributes.getPsuId());
		params.put(PSD2Constants.CORRELATION_REQ_HEADER, reqHeaderAtrributes.getCorrelationId());
		params.put(PSD2Constants.TENANT_ID, reqHeaderAtrributes.getTenantId());
		return params;
	}
}
