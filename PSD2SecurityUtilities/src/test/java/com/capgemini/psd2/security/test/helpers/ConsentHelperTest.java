/*package com.capgemini.psd2.security.test.helpers;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.identitymanagement.models.CustomClientDetails;
import com.capgemini.psd2.identitymanagement.services.TppInfoMgmtService;
import com.capgemini.psd2.security.config.AuthenticationFacade;
import com.capgemini.psd2.security.config.IAuthenticationFacade;
import com.capgemini.psd2.security.constants.PSD2SecurityConstants;
import com.capgemini.psd2.security.exceptions.PSD2SecurityException;
import com.capgemini.psd2.security.helpers.ConsentHelper;
import com.capgemini.psd2.security.models.SecurityRequestAttributes;
import com.capgemini.psd2.utilities.PSD2SecurityUtilitiesConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = PSD2SecurityUtilitiesConfig.class)
public class ConsentHelperTest {

	@InjectMocks
	private ConsentHelper consentHelper = new ConsentHelper();

	private HttpServletRequest request;

	private HttpServletResponse response;

	private SecurityRequestAttributes securityRequestAttributes;

	private TppInfoMgmtService tppInfoMgmtServiceImpl;

	@Mock
	private IAuthenticationFacade authenticationFacade = new AuthenticationFacade();

	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test(expected = PSD2SecurityException.class)
	public void testPopulateUserIdWithException(){
		ReflectionTestUtils.setField(consentHelper, "authenticationFacade", authenticationFacade);	
		request = mock(HttpServletRequest.class);
		response = mock(HttpServletResponse.class);
		consentHelper.populateUserId(request, response);
	}

	@Test
	public void testPopulateTppInfo(){
		CustomClientDetails customerClientDetails = new CustomClientDetails();
		customerClientDetails.setLegalEntityName("Moneywise");
		tppInfoMgmtServiceImpl = mock(TppInfoMgmtService.class);
		when(tppInfoMgmtServiceImpl.findTppInfo(anyString())).thenReturn(customerClientDetails);
		securityRequestAttributes = mock(SecurityRequestAttributes.class);
		Map<String, String> paramMaps = new HashMap<>();
		paramMaps.put(PSD2SecurityConstants.CID_PARAM, "ClientId");
		ReflectionTestUtils.setField(consentHelper, "securityRequestAttributes", securityRequestAttributes);
		ReflectionTestUtils.setField(consentHelper, "tppInfoMgmtService", tppInfoMgmtServiceImpl);
		when(securityRequestAttributes.getParamMap()).thenReturn(paramMaps );
		consentHelper.populateTppInfo();
	}

	@After
	public void tearDown() throws Exception {
		consentHelper = null;
	}
}
*/