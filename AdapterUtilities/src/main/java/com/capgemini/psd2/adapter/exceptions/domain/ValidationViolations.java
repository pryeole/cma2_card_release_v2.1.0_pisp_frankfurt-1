
package com.capgemini.psd2.adapter.exceptions.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "validationViolation"
})
public class ValidationViolations {

    @JsonProperty("validationViolation")
    private ValidationViolation validationViolation;

    /**
     * No args constructor for use in serialization
     * 
     */
    public ValidationViolations() {
    }

    /**
     * 
     * @param validationViolation
     */
    public ValidationViolations(ValidationViolation validationViolation) {
        super();
        this.validationViolation = validationViolation;
    }

    @JsonProperty("validationViolation")
    public ValidationViolation getValidationViolation() {
        return validationViolation;
    }

    @JsonProperty("validationViolation")
    public void setValidationViolation(ValidationViolation validationViolation) {
        this.validationViolation = validationViolation;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("validationViolation", validationViolation).toString();
    }

}
