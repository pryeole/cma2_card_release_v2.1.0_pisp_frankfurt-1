/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.adapter.exceptions.test;

import static org.junit.Assert.assertEquals;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.adapter.exceptions.AdapterExceptionHandlerImpl;

/**
 * The Class AdapterExceptionHandlerImplTest.
 */
public class AdapterExceptionHandlerImplTest {
    
    /** The adapter exception handler impl. */
    @InjectMocks
	private AdapterExceptionHandlerImpl adapterExceptionHandlerImpl;
	
	/** The http server error exception. */
	@Mock
	private HttpServerErrorException httpServerErrorException;
	
	/** The http client error exception. */
	@Mock 
	private HttpClientErrorException httpClientErrorException;
	
	/** The resource access exception. */
	@Mock
	private ResourceAccessException resourceAccessException;
	
	
	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		adapterExceptionHandlerImpl = new AdapterExceptionHandlerImpl();
	}
	
	/**
	 * Test handle exception with server error.
	 */
	@Test(expected=AdapterException.class)
	public void testHandleExceptionWithServerError(){
		httpServerErrorException = new HttpServerErrorException(HttpStatus.BAD_REQUEST);
		adapterExceptionHandlerImpl.handleException(httpServerErrorException);
	}
	
	/**
	 * Test handle exception with clent error.
	 */
	@Test(expected=AdapterException.class)
	public void testHandleExceptionWithClentError(){
		httpClientErrorException=new HttpClientErrorException(HttpStatus.NOT_FOUND);
		adapterExceptionHandlerImpl.handleException(httpClientErrorException);
	}
	
	/**
	 * Test error map.
	 */
	@Test
	public void testErrorMap(){
		Map<String, String> errormap = new HashMap<>();
		errormap.put("errorCode", "400");
		adapterExceptionHandlerImpl.setErrormap(errormap);
		assertEquals("400", adapterExceptionHandlerImpl.getErrormap().get("errorCode"));
	}
	
	/**
	 * Test resource access exception.
	 */
	@Test(expected=AdapterException.class)
	public void testResourceAccessException(){
		resourceAccessException=new ResourceAccessException("Connetion Time Out");
		adapterExceptionHandlerImpl.handleException(resourceAccessException);
	}
	
	/**
	 * Test handle exception with server exception.
	 */
	@Test(expected=AdapterException.class)
	public void testHandleExceptionWithServerException(){
		String errorStr = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><validationViolations xmlns=\"http://bankofireland.com/channels/errors/\"><validationViolation><errorCode>FS_ABTB_004</errorCode><errorText>Technical Error. Please try again later</errorText><errorField>Technical Error. Please try again later</errorField><errorValue>Server Error</errorValue></validationViolation></validationViolations>";
		byte[] responseBody =errorStr.getBytes();
		Map<String, String> errormap = new HashMap<>();
		errormap.put("FS_ABTB_004", "ACCOUNT_DETAILS_NOT_FOUND");
		adapterExceptionHandlerImpl.setErrormap(errormap);
		httpServerErrorException = new HttpServerErrorException(HttpStatus.BAD_REQUEST,"Bad Request",responseBody,Charset.forName("UTF-8"));
        adapterExceptionHandlerImpl.handleException(httpServerErrorException);
	}
	
	/**
	 * Test handle exception with clent exception.
	 */
	@Test(expected=AdapterException.class)
	public void testHandleExceptionWithClentException(){
		String errorStr = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><validationViolations xmlns=\"http://bankofireland.com/channels/errors/\"><validationViolation><errorCode>FS_ABTB_004</errorCode><errorText>Technical Error. Please try again later</errorText><errorField>Technical Error. Please try again later</errorField><errorValue>Server Error</errorValue></validationViolation></validationViolations>";
		byte[] responseBody =errorStr.getBytes();
		Map<String, String> errormap = new HashMap<>();
		errormap.put("FS_ABTB_004", "ACCOUNT_DETAILS_NOT_FOUND");
		adapterExceptionHandlerImpl.setErrormap(errormap);
		httpClientErrorException = new HttpClientErrorException(HttpStatus.BAD_REQUEST,"Bad Request",responseBody,Charset.forName("UTF-8"));
        adapterExceptionHandlerImpl.handleException(httpClientErrorException);
	}
	
	
	
	
}
